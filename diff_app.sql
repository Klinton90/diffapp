-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.26-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for diff_app
CREATE DATABASE IF NOT EXISTS `diff_app` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `diff_app`;

-- Dumping structure for table diff_app.ac_groups
CREATE TABLE IF NOT EXISTS `ac_groups` (
  `acg_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`acg_id`),
  UNIQUE KEY `UK_ac_group` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.ac_groups: ~6 rows (approximately)
/*!40000 ALTER TABLE `ac_groups` DISABLE KEYS */;
INSERT INTO `ac_groups` (`acg_id`, `name`, `version`) VALUES
	(0, 'fake', 39),
	(1, 'acg_1', 58),
	(2, 'acg2', 20),
	(8, 'Languages', 39),
	(9, 'arr1', 82),
	(10, '1', 59);
/*!40000 ALTER TABLE `ac_groups` ENABLE KEYS */;

-- Dumping structure for table diff_app.ac_params
CREATE TABLE IF NOT EXISTS `ac_params` (
  `acp_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `acg_id` int(11) NOT NULL,
  PRIMARY KEY (`acp_id`),
  KEY `FK_focalmekc7g1kocwgi03dg1cy` (`acg_id`),
  CONSTRAINT `FK_focalmekc7g1kocwgi03dg1cy` FOREIGN KEY (`acg_id`) REFERENCES `ac_groups` (`acg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.ac_params: ~11 rows (approximately)
/*!40000 ALTER TABLE `ac_params` DISABLE KEYS */;
INSERT INTO `ac_params` (`acp_id`, `name`, `value`, `version`, `acg_id`) VALUES
	(1, '11', '21', 5, 1),
	(2, 'acp_key2', 'NI, IN', 0, 1),
	(25, '2', '2', 5, 2),
	(28, 'Lng', 'EN,FR,EN,FR,EN,FR,EN,FR,EN,FR,EN,FR,EN,FR,EN,FR,EN,FR,EN,FR,EN,FR', 2, 8),
	(32, '1', '1', 22, 9),
	(39, '2', '2', 0, 9),
	(41, '3', '3', 0, 9),
	(45, '4', '4', 0, 9),
	(46, 'a1', 'a1_1, a1_2', 2, 10),
	(47, 'a2', 'a2_1, a2_2', 3, 10),
	(55, 'a3', 'a3_1, a3_2', 2, 10);
/*!40000 ALTER TABLE `ac_params` ENABLE KEYS */;

-- Dumping structure for table diff_app.db_drivers
CREATE TABLE IF NOT EXISTS `db_drivers` (
  `db_driver_id` int(11) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `driver_class` varchar(256) NOT NULL,
  `name` varchar(50) NOT NULL,
  `driver_url_prefix` varchar(50) NOT NULL,
  PRIMARY KEY (`db_driver_id`),
  UNIQUE KEY `UK_db_drivers` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.db_drivers: ~3 rows (approximately)
/*!40000 ALTER TABLE `db_drivers` DISABLE KEYS */;
INSERT INTO `db_drivers` (`db_driver_id`, `version`, `driver_class`, `name`, `driver_url_prefix`) VALUES
	(0, 0, '', '', ''),
	(1, 0, 'com.mysql.jdbc.Driver', 'mysql', ''),
	(2, 0, 'oracle.jdbc.driver.OracleDriver', 'oracle', '');
/*!40000 ALTER TABLE `db_drivers` ENABLE KEYS */;

-- Dumping structure for table diff_app.db_settings
CREATE TABLE IF NOT EXISTS `db_settings` (
  `db_setting_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `db_driver_id` int(11) NOT NULL,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`db_setting_id`),
  KEY `FK_kq4fh8ovbkomgapf2rk60xeyq` (`db_driver_id`),
  CONSTRAINT `FK_kq4fh8ovbkomgapf2rk60xeyq` FOREIGN KEY (`db_driver_id`) REFERENCES `db_drivers` (`db_driver_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.db_settings: ~4 rows (approximately)
/*!40000 ALTER TABLE `db_settings` DISABLE KEYS */;
INSERT INTO `db_settings` (`db_setting_id`, `login`, `password`, `url`, `db_driver_id`, `version`, `name`) VALUES
	(0, 'fake', 'fake', 'fake', 1, 0, 'fake'),
	(1, 'root', '12345qQ!', 'localhost:3306/nvd_canada', 1, 6, 'home1'),
	(2, 'root', '12345qQ!', 'jdbc:mysql://localhost:3306/diff_app', 1, 10, 'home2'),
	(3, '1', '1', '1', 2, 3, 'db1');
/*!40000 ALTER TABLE `db_settings` ENABLE KEYS */;

-- Dumping structure for table diff_app.ignored_element
CREATE TABLE IF NOT EXISTS `ignored_element` (
  `ie_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `xml2xml_id` int(11) NOT NULL,
  PRIMARY KEY (`ie_id`),
  KEY `FK_fltvt198enghae6tky7hlrefo` (`xml2xml_id`),
  CONSTRAINT `FK_fltvt198enghae6tky7hlrefo` FOREIGN KEY (`xml2xml_id`) REFERENCES `xml2xml_params` (`xml2xml_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.ignored_element: ~2 rows (approximately)
/*!40000 ALTER TABLE `ignored_element` DISABLE KEYS */;
INSERT INTO `ignored_element` (`ie_id`, `name`, `value`, `version`, `xml2xml_id`) VALUES
	(1, '113', '113', 4, 1),
	(2, '21', '21', 1, 1);
/*!40000 ALTER TABLE `ignored_element` ENABLE KEYS */;

-- Dumping structure for table diff_app.included_mapping
CREATE TABLE IF NOT EXISTS `included_mapping` (
  `im_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `query` text NOT NULL,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `xml2db_id` int(11) NOT NULL,
  PRIMARY KEY (`im_id`),
  KEY `FK_9n5rfucpjsvgghercj92qm3ok` (`xml2db_id`),
  CONSTRAINT `FK_9n5rfucpjsvgghercj92qm3ok` FOREIGN KEY (`xml2db_id`) REFERENCES `xml2db_params` (`xml2db_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.included_mapping: ~0 rows (approximately)
/*!40000 ALTER TABLE `included_mapping` DISABLE KEYS */;
INSERT INTO `included_mapping` (`im_id`, `name`, `query`, `version`, `xml2db_id`) VALUES
	(1, '12', '12345', 1, 1),
	(2, 'SpecialProperties', 'SELECT 2;', 0, 2);
/*!40000 ALTER TABLE `included_mapping` ENABLE KEYS */;

-- Dumping structure for table diff_app.iteration_runs
CREATE TABLE IF NOT EXISTS `iteration_runs` (
  `iteration_run_id` int(11) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `passed` bit(1) NOT NULL,
  `test_run_id` int(11) NOT NULL,
  `ws_params` varchar(255) NOT NULL,
  PRIMARY KEY (`iteration_run_id`),
  KEY `FK_bjwkvp0knhrgitqysv582f7nx` (`test_run_id`),
  CONSTRAINT `FK_bjwkvp0knhrgitqysv582f7nx` FOREIGN KEY (`test_run_id`) REFERENCES `test_runs` (`test_run_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1486 DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.iteration_runs: ~132 rows (approximately)
/*!40000 ALTER TABLE `iteration_runs` DISABLE KEYS */;
INSERT INTO `iteration_runs` (`iteration_run_id`, `version`, `passed`, `test_run_id`, `ws_params`) VALUES
	(1354, 0, b'1', 113, '[Lng:EN]'),
	(1355, 0, b'1', 113, '[Lng:FR]'),
	(1356, 0, b'1', 113, '[Lng:EN]'),
	(1357, 0, b'1', 113, '[Lng:FR]'),
	(1358, 0, b'1', 113, '[Lng:EN]'),
	(1359, 0, b'1', 113, '[Lng:FR]'),
	(1360, 0, b'1', 113, '[Lng:EN]'),
	(1361, 0, b'1', 113, '[Lng:FR]'),
	(1362, 0, b'1', 113, '[Lng:EN]'),
	(1363, 0, b'1', 113, '[Lng:FR]'),
	(1364, 0, b'1', 113, '[Lng:EN]'),
	(1365, 0, b'1', 113, '[Lng:FR]'),
	(1366, 0, b'1', 113, '[Lng:EN]'),
	(1367, 0, b'1', 113, '[Lng:FR]'),
	(1368, 0, b'1', 113, '[Lng:EN]'),
	(1369, 0, b'1', 113, '[Lng:FR]'),
	(1370, 0, b'1', 113, '[Lng:EN]'),
	(1371, 0, b'1', 113, '[Lng:FR]'),
	(1372, 0, b'1', 113, '[Lng:EN]'),
	(1373, 0, b'1', 113, '[Lng:FR]'),
	(1374, 0, b'1', 113, '[Lng:EN]'),
	(1375, 0, b'1', 113, '[Lng:FR]'),
	(1376, 0, b'1', 114, '[Lng:EN]'),
	(1377, 0, b'1', 114, '[Lng:FR]'),
	(1378, 0, b'1', 114, '[Lng:EN]'),
	(1379, 0, b'1', 114, '[Lng:FR]'),
	(1380, 0, b'1', 114, '[Lng:EN]'),
	(1381, 0, b'1', 114, '[Lng:FR]'),
	(1382, 0, b'1', 114, '[Lng:EN]'),
	(1383, 0, b'1', 114, '[Lng:FR]'),
	(1384, 0, b'1', 114, '[Lng:EN]'),
	(1385, 0, b'1', 114, '[Lng:FR]'),
	(1386, 0, b'1', 114, '[Lng:EN]'),
	(1387, 0, b'1', 114, '[Lng:FR]'),
	(1388, 0, b'1', 114, '[Lng:EN]'),
	(1389, 0, b'1', 114, '[Lng:FR]'),
	(1390, 0, b'1', 114, '[Lng:EN]'),
	(1391, 0, b'1', 114, '[Lng:FR]'),
	(1392, 0, b'1', 114, '[Lng:EN]'),
	(1393, 0, b'1', 114, '[Lng:FR]'),
	(1394, 0, b'1', 114, '[Lng:EN]'),
	(1395, 0, b'1', 114, '[Lng:FR]'),
	(1396, 0, b'1', 114, '[Lng:EN]'),
	(1397, 0, b'1', 114, '[Lng:FR]'),
	(1398, 0, b'1', 115, '[Lng:EN]'),
	(1399, 0, b'1', 115, '[Lng:FR]'),
	(1400, 0, b'1', 115, '[Lng:EN]'),
	(1401, 0, b'1', 115, '[Lng:FR]'),
	(1402, 0, b'1', 115, '[Lng:EN]'),
	(1403, 0, b'1', 115, '[Lng:FR]'),
	(1404, 0, b'1', 115, '[Lng:EN]'),
	(1405, 0, b'1', 115, '[Lng:FR]'),
	(1406, 0, b'1', 115, '[Lng:EN]'),
	(1407, 0, b'1', 115, '[Lng:FR]'),
	(1408, 0, b'1', 115, '[Lng:EN]'),
	(1409, 0, b'1', 115, '[Lng:FR]'),
	(1410, 0, b'1', 115, '[Lng:EN]'),
	(1411, 0, b'1', 115, '[Lng:FR]'),
	(1412, 0, b'1', 115, '[Lng:EN]'),
	(1413, 0, b'1', 115, '[Lng:FR]'),
	(1414, 0, b'1', 115, '[Lng:EN]'),
	(1415, 0, b'1', 115, '[Lng:FR]'),
	(1416, 0, b'1', 115, '[Lng:EN]'),
	(1417, 0, b'1', 115, '[Lng:FR]'),
	(1418, 0, b'1', 115, '[Lng:EN]'),
	(1419, 0, b'1', 115, '[Lng:FR]'),
	(1420, 0, b'1', 116, '[Lng:EN]'),
	(1421, 0, b'1', 116, '[Lng:FR]'),
	(1422, 0, b'1', 116, '[Lng:EN]'),
	(1423, 0, b'1', 116, '[Lng:FR]'),
	(1424, 0, b'1', 116, '[Lng:EN]'),
	(1425, 0, b'1', 116, '[Lng:FR]'),
	(1426, 0, b'1', 116, '[Lng:EN]'),
	(1427, 0, b'1', 116, '[Lng:FR]'),
	(1428, 0, b'1', 116, '[Lng:EN]'),
	(1429, 0, b'1', 116, '[Lng:FR]'),
	(1430, 0, b'1', 116, '[Lng:EN]'),
	(1431, 0, b'1', 116, '[Lng:FR]'),
	(1432, 0, b'1', 116, '[Lng:EN]'),
	(1433, 0, b'1', 116, '[Lng:FR]'),
	(1434, 0, b'1', 116, '[Lng:EN]'),
	(1435, 0, b'1', 116, '[Lng:FR]'),
	(1436, 0, b'1', 116, '[Lng:EN]'),
	(1437, 0, b'1', 116, '[Lng:FR]'),
	(1438, 0, b'1', 116, '[Lng:EN]'),
	(1439, 0, b'1', 116, '[Lng:FR]'),
	(1440, 0, b'1', 116, '[Lng:EN]'),
	(1441, 0, b'1', 116, '[Lng:FR]'),
	(1442, 0, b'1', 117, '[Lng:EN]'),
	(1443, 0, b'1', 117, '[Lng:FR]'),
	(1444, 0, b'1', 117, '[Lng:EN]'),
	(1445, 0, b'1', 117, '[Lng:FR]'),
	(1446, 0, b'1', 117, '[Lng:EN]'),
	(1447, 0, b'1', 117, '[Lng:FR]'),
	(1448, 0, b'1', 117, '[Lng:EN]'),
	(1449, 0, b'1', 117, '[Lng:FR]'),
	(1450, 0, b'1', 117, '[Lng:EN]'),
	(1451, 0, b'1', 117, '[Lng:FR]'),
	(1452, 0, b'1', 117, '[Lng:EN]'),
	(1453, 0, b'1', 117, '[Lng:FR]'),
	(1454, 0, b'1', 117, '[Lng:EN]'),
	(1455, 0, b'1', 117, '[Lng:FR]'),
	(1456, 0, b'1', 117, '[Lng:EN]'),
	(1457, 0, b'1', 117, '[Lng:FR]'),
	(1458, 0, b'1', 117, '[Lng:EN]'),
	(1459, 0, b'1', 117, '[Lng:FR]'),
	(1460, 0, b'1', 117, '[Lng:EN]'),
	(1461, 0, b'1', 117, '[Lng:FR]'),
	(1462, 0, b'1', 117, '[Lng:EN]'),
	(1463, 0, b'1', 117, '[Lng:FR]'),
	(1464, 0, b'1', 118, '[Lng:EN]'),
	(1465, 0, b'1', 118, '[Lng:FR]'),
	(1466, 0, b'1', 118, '[Lng:EN]'),
	(1467, 0, b'1', 118, '[Lng:FR]'),
	(1468, 0, b'1', 118, '[Lng:EN]'),
	(1469, 0, b'1', 118, '[Lng:FR]'),
	(1470, 0, b'1', 118, '[Lng:EN]'),
	(1471, 0, b'1', 118, '[Lng:FR]'),
	(1472, 0, b'1', 118, '[Lng:EN]'),
	(1473, 0, b'1', 118, '[Lng:FR]'),
	(1474, 0, b'1', 118, '[Lng:EN]'),
	(1475, 0, b'1', 118, '[Lng:FR]'),
	(1476, 0, b'1', 118, '[Lng:EN]'),
	(1477, 0, b'1', 118, '[Lng:FR]'),
	(1478, 0, b'1', 118, '[Lng:EN]'),
	(1479, 0, b'1', 118, '[Lng:FR]'),
	(1480, 0, b'1', 118, '[Lng:EN]'),
	(1481, 0, b'1', 118, '[Lng:FR]'),
	(1482, 0, b'1', 118, '[Lng:EN]'),
	(1483, 0, b'1', 118, '[Lng:FR]'),
	(1484, 0, b'1', 118, '[Lng:EN]'),
	(1485, 0, b'1', 118, '[Lng:FR]');
/*!40000 ALTER TABLE `iteration_runs` ENABLE KEYS */;

-- Dumping structure for table diff_app.other_params
CREATE TABLE IF NOT EXISTS `other_params` (
  `op_id` int(11) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `param_type` varchar(50) NOT NULL,
  `value` varchar(50) NOT NULL,
  PRIMARY KEY (`op_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.other_params: ~0 rows (approximately)
/*!40000 ALTER TABLE `other_params` DISABLE KEYS */;
INSERT INTO `other_params` (`op_id`, `version`, `param_type`, `value`) VALUES
	(1, 0, '0', '0');
/*!40000 ALTER TABLE `other_params` ENABLE KEYS */;

-- Dumping structure for table diff_app.sql_combo_groups
CREATE TABLE IF NOT EXISTS `sql_combo_groups` (
  `scg_id` int(11) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL,
  `query` longtext NOT NULL,
  PRIMARY KEY (`scg_id`),
  UNIQUE KEY `UK_7qc6n4jd5mmib05c3ttsns03` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.sql_combo_groups: ~3 rows (approximately)
/*!40000 ALTER TABLE `sql_combo_groups` DISABLE KEYS */;
INSERT INTO `sql_combo_groups` (`scg_id`, `version`, `name`, `query`) VALUES
	(0, 67, 'fake', 'fake_'),
	(1, 121, 'sql1', 'SELECT * \nFROM tests AS t\nWHERE t.test_id = :test_id\nAND t.comment = :comment;'),
	(2, 13, '2', '22222');
/*!40000 ALTER TABLE `sql_combo_groups` ENABLE KEYS */;

-- Dumping structure for table diff_app.sql_combo_params
CREATE TABLE IF NOT EXISTS `sql_combo_params` (
  `scp_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL DEFAULT '',
  `scg_id` int(11) NOT NULL,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `test_id` int(11) NOT NULL,
  PRIMARY KEY (`scp_id`),
  KEY `FK_9kvo4je5jf2a6x7tph1q27nk0` (`scg_id`),
  CONSTRAINT `FK_9kvo4je5jf2a6x7tph1q27nk0` FOREIGN KEY (`scg_id`) REFERENCES `sql_combo_groups` (`scg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.sql_combo_params: ~2 rows (approximately)
/*!40000 ALTER TABLE `sql_combo_params` DISABLE KEYS */;
INSERT INTO `sql_combo_params` (`scp_id`, `name`, `value`, `scg_id`, `version`, `test_id`) VALUES
	(4, 'test_id', '1', 1, 1, 0),
	(6, 'comment', 'c1', 1, 1, 0);
/*!40000 ALTER TABLE `sql_combo_params` ENABLE KEYS */;

-- Dumping structure for table diff_app.tests
CREATE TABLE IF NOT EXISTS `tests` (
  `test_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `web_test_id` int(11) NOT NULL DEFAULT '0',
  `acg_id` int(11) NOT NULL DEFAULT '0',
  `scg_id` int(11) NOT NULL DEFAULT '0',
  `comment` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `spira_id` int(11) NOT NULL DEFAULT '0',
  `xml2xml_id` int(11) NOT NULL DEFAULT '0',
  `xml2db_id` int(11) NOT NULL DEFAULT '0',
  `version` bigint(20) NOT NULL DEFAULT '0',
  `list_path` varchar(255) NOT NULL,
  `report_folder` varchar(20) NOT NULL,
  `db_setting_id` int(11) NOT NULL DEFAULT '0',
  `spira_prj_id` int(11) NOT NULL,
  `spira_test_id` int(11) NOT NULL,
  `web_test1_id` int(11) NOT NULL,
  `web_test2_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`test_id`),
  UNIQUE KEY `Index 2` (`name`),
  KEY `FK_cajx9t53nuk7ymunh0fy7uh4w` (`web_test_id`),
  KEY `FK_94yqb92uf6ciqxfvt8l0u9wmr` (`acg_id`),
  KEY `FK_p8p0sk4u0ilsfnt43oee4aj4t` (`xml2db_id`),
  KEY `FK_eynf7n7elx2hlk1t0ticxgm7h` (`xml2xml_id`),
  KEY `FK_r2id68h696wxbgcl3g0v6niv0` (`scg_id`),
  KEY `FK_7atak0rx4w5t1k9csc29bsc6k` (`db_setting_id`),
  KEY `FK_edksq3vew46hdsxqct2bya9k1` (`web_test1_id`),
  KEY `FK_a1esrkmdcn6qrlaplcxwfswh2` (`web_test2_id`),
  CONSTRAINT `FK_7atak0rx4w5t1k9csc29bsc6k` FOREIGN KEY (`db_setting_id`) REFERENCES `db_settings` (`db_setting_id`),
  CONSTRAINT `FK_94yqb92uf6ciqxfvt8l0u9wmr` FOREIGN KEY (`acg_id`) REFERENCES `ac_groups` (`acg_id`),
  CONSTRAINT `FK_a1esrkmdcn6qrlaplcxwfswh2` FOREIGN KEY (`web_test2_id`) REFERENCES `web_tests` (`web_test_id`),
  CONSTRAINT `FK_cajx9t53nuk7ymunh0fy7uh4w` FOREIGN KEY (`web_test_id`) REFERENCES `web_tests` (`web_test_id`),
  CONSTRAINT `FK_edksq3vew46hdsxqct2bya9k1` FOREIGN KEY (`web_test1_id`) REFERENCES `web_tests` (`web_test_id`),
  CONSTRAINT `FK_eynf7n7elx2hlk1t0ticxgm7h` FOREIGN KEY (`xml2xml_id`) REFERENCES `xml2xml_params` (`xml2xml_id`),
  CONSTRAINT `FK_p8p0sk4u0ilsfnt43oee4aj4t` FOREIGN KEY (`xml2db_id`) REFERENCES `xml2db_params` (`xml2db_id`),
  CONSTRAINT `FK_r2id68h696wxbgcl3g0v6niv0` FOREIGN KEY (`scg_id`) REFERENCES `sql_combo_groups` (`scg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.tests: ~5 rows (approximately)
/*!40000 ALTER TABLE `tests` DISABLE KEYS */;
INSERT INTO `tests` (`test_id`, `name`, `web_test_id`, `acg_id`, `scg_id`, `comment`, `description`, `spira_id`, `xml2xml_id`, `xml2db_id`, `version`, `list_path`, `report_folder`, `db_setting_id`, `spira_prj_id`, `spira_test_id`, `web_test1_id`, `web_test2_id`) VALUES
	(1, 'test1 long long long long long long long long', 1, 1, 0, 'c1', 'd1', 123, 1, 1, 23, 'result', 'some1', 0, 0, 0, 0, NULL),
	(4, 'getDealers XML', 2, 8, 0, 'getDealers XML', 'getDealers XML', 0, 2, 0, 147, 'dealers', 'some2', 0, 0, 0, 0, NULL),
	(6, 'getDealers DB', 2, 8, 0, 'getDealers DB', 'getDealers DB', 0, 0, 2, 32, 'dealers', 'qwe', 1, 0, 0, 0, NULL),
	(7, '1', 1, 10, 1, '1', '1', 1, 1, 1, 22, '1', '1', 2, 0, 0, 0, NULL),
	(8, '2', 1, 9, 1, '2', '2', 2, 2, 1, 9, '2', '2', 0, 0, 0, 0, NULL);
/*!40000 ALTER TABLE `tests` ENABLE KEYS */;

-- Dumping structure for table diff_app.test_cases
CREATE TABLE IF NOT EXISTS `test_cases` (
  `tc_id` int(11) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `db_setting_id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `web_environment1_id` int(11) NOT NULL,
  `web_environment2_id` int(11) NOT NULL,
  PRIMARY KEY (`tc_id`),
  UNIQUE KEY `UK_e9sstgd6333dvt8rwxam6q633` (`name`),
  KEY `FK_ijrh42johy3xo860670pqoy44` (`db_setting_id`),
  KEY `FK_4krt3pc0oowl7vx6ty994cifn` (`web_environment1_id`),
  KEY `FK_29fk0sswkyxwqi4otgpmib3bq` (`web_environment2_id`),
  CONSTRAINT `FK_29fk0sswkyxwqi4otgpmib3bq` FOREIGN KEY (`web_environment2_id`) REFERENCES `web_environments` (`web_environment_id`),
  CONSTRAINT `FK_4krt3pc0oowl7vx6ty994cifn` FOREIGN KEY (`web_environment1_id`) REFERENCES `web_environments` (`web_environment_id`),
  CONSTRAINT `FK_ijrh42johy3xo860670pqoy44` FOREIGN KEY (`db_setting_id`) REFERENCES `db_settings` (`db_setting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.test_cases: ~2 rows (approximately)
/*!40000 ALTER TABLE `test_cases` DISABLE KEYS */;
INSERT INTO `test_cases` (`tc_id`, `version`, `name`, `db_setting_id`, `description`, `web_environment1_id`, `web_environment2_id`) VALUES
	(1, 24, 'getDealers XML only', 0, '', 0, 0),
	(8, 29, 'getDealers XML and PROD', 0, '', 0, 0);
/*!40000 ALTER TABLE `test_cases` ENABLE KEYS */;

-- Dumping structure for table diff_app.test_cases_tests
CREATE TABLE IF NOT EXISTS `test_cases_tests` (
  `test_case_test_id` int(11) DEFAULT NULL,
  `test_id` int(11) DEFAULT NULL,
  KEY `FK_8o5dq8qqaa0f1jfse13py3bos` (`test_id`),
  KEY `FK_nw4ikiv5g63stww69aum4n5vo` (`test_case_test_id`),
  CONSTRAINT `FK_8o5dq8qqaa0f1jfse13py3bos` FOREIGN KEY (`test_id`) REFERENCES `tests` (`test_id`),
  CONSTRAINT `FK_nw4ikiv5g63stww69aum4n5vo` FOREIGN KEY (`test_case_test_id`) REFERENCES `test_cases` (`tc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.test_cases_tests: ~3 rows (approximately)
/*!40000 ALTER TABLE `test_cases_tests` DISABLE KEYS */;
INSERT INTO `test_cases_tests` (`test_case_test_id`, `test_id`) VALUES
	(1, 4),
	(8, 6),
	(8, 4);
/*!40000 ALTER TABLE `test_cases_tests` ENABLE KEYS */;

-- Dumping structure for table diff_app.test_case_runs
CREATE TABLE IF NOT EXISTS `test_case_runs` (
  `tc_run_id` int(11) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `tc_id` int(11) NOT NULL,
  `messages` text NOT NULL,
  `date_created` datetime NOT NULL,
  `status` varchar(9) NOT NULL,
  PRIMARY KEY (`tc_run_id`),
  KEY `FK_nql55tjmljirsgx86jrvdxjvy` (`tc_id`),
  CONSTRAINT `FK_nql55tjmljirsgx86jrvdxjvy` FOREIGN KEY (`tc_id`) REFERENCES `test_cases` (`tc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=185 DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.test_case_runs: ~6 rows (approximately)
/*!40000 ALTER TABLE `test_case_runs` DISABLE KEYS */;
INSERT INTO `test_case_runs` (`tc_run_id`, `version`, `tc_id`, `messages`, `date_created`, `status`) VALUES
	(179, 1, 1, '', '0000-00-00 00:00:00', ''),
	(180, 1, 1, '', '0000-00-00 00:00:00', ''),
	(181, 1, 1, '', '0000-00-00 00:00:00', ''),
	(182, 1, 1, '', '0000-00-00 00:00:00', ''),
	(183, 1, 1, '', '0000-00-00 00:00:00', ''),
	(184, 1, 1, '', '0000-00-00 00:00:00', '');
/*!40000 ALTER TABLE `test_case_runs` ENABLE KEYS */;

-- Dumping structure for table diff_app.test_runs
CREATE TABLE IF NOT EXISTS `test_runs` (
  `test_run_id` int(11) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `test_id` int(11) NOT NULL,
  `test_case_run_id` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `tc_run_id` int(11) NOT NULL,
  PRIMARY KEY (`test_run_id`),
  KEY `FK_kpbrdmw2scwbecvre9l4sk7j0` (`test_id`),
  KEY `FK_1djtihg4vh8wsk7wogd9828cq` (`test_case_run_id`),
  CONSTRAINT `FK_1djtihg4vh8wsk7wogd9828cq` FOREIGN KEY (`test_case_run_id`) REFERENCES `test_case_runs` (`tc_run_id`),
  CONSTRAINT `FK_kpbrdmw2scwbecvre9l4sk7j0` FOREIGN KEY (`test_id`) REFERENCES `tests` (`test_id`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.test_runs: ~6 rows (approximately)
/*!40000 ALTER TABLE `test_runs` DISABLE KEYS */;
INSERT INTO `test_runs` (`test_run_id`, `version`, `test_id`, `test_case_run_id`, `total`, `tc_run_id`) VALUES
	(113, 0, 4, 179, 22, 0),
	(114, 0, 4, 180, 22, 0),
	(115, 0, 4, 181, 22, 0),
	(116, 0, 4, 182, 22, 0),
	(117, 0, 4, 183, 22, 0),
	(118, 0, 4, 184, 22, 0);
/*!40000 ALTER TABLE `test_runs` ENABLE KEYS */;

-- Dumping structure for table diff_app.vt05_backup
CREATE TABLE IF NOT EXISTS `vt05_backup` (
  `acode` varchar(11) NOT NULL,
  `version` bigint(20) NOT NULL,
  `rev_code` float NOT NULL,
  PRIMARY KEY (`acode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.vt05_backup: ~0 rows (approximately)
/*!40000 ALTER TABLE `vt05_backup` DISABLE KEYS */;
/*!40000 ALTER TABLE `vt05_backup` ENABLE KEYS */;

-- Dumping structure for table diff_app.web_environments
CREATE TABLE IF NOT EXISTS `web_environments` (
  `web_environment_id` int(11) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`web_environment_id`),
  UNIQUE KEY `UK_environments` (`web_environment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.web_environments: ~3 rows (approximately)
/*!40000 ALTER TABLE `web_environments` DISABLE KEYS */;
INSERT INTO `web_environments` (`web_environment_id`, `version`, `name`, `url`) VALUES
	(0, 0, 'fake', 'fake_'),
	(1, 4, 'QA Canada', 'http://127.0.0.1'),
	(2, 2, 'UAT Canada', 'http://uat-nica-ws.autodatadirect.com/nissanca-tools-ws/rest/ws/'),
	(3, 0, 'PROD Canada', 'http://nica-ws.autodatadirect.com/nissanca-tools-ws/rest/ws/');
/*!40000 ALTER TABLE `web_environments` ENABLE KEYS */;

-- Dumping structure for table diff_app.web_tests
CREATE TABLE IF NOT EXISTS `web_tests` (
  `web_test_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `path_pattern` varchar(255) NOT NULL DEFAULT '',
  `version` bigint(20) NOT NULL DEFAULT '0',
  `hmethod` enum('GET','POST') NOT NULL DEFAULT 'GET',
  `ws_name` varchar(50) NOT NULL,
  `format` enum('XML','JSON') NOT NULL DEFAULT 'XML',
  PRIMARY KEY (`web_test_id`),
  UNIQUE KEY `UK_web_services` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.web_tests: ~3 rows (approximately)
/*!40000 ALTER TABLE `web_tests` DISABLE KEYS */;
INSERT INTO `web_tests` (`web_test_id`, `name`, `path_pattern`, `version`, `hmethod`, `ws_name`, `format`) VALUES
	(0, '', '', 0, 'GET', '', 'XML'),
	(1, 'getAllFilters', '', 3, 'GET', 'getAllFilters', 'XML'),
	(2, 'getDealers', '/CA/$Lng.xml', 3, 'GET', 'getDealers', 'XML');
/*!40000 ALTER TABLE `web_tests` ENABLE KEYS */;

-- Dumping structure for table diff_app.xml2db_params
CREATE TABLE IF NOT EXISTS `xml2db_params` (
  `xml2db_id` int(11) NOT NULL AUTO_INCREMENT,
  `ignored_value` varchar(10) NOT NULL DEFAULT '',
  `mods1` text NOT NULL,
  `mods2` text NOT NULL,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `orderly_safe_array` bit(1) NOT NULL,
  `orderly_safe_inc` bit(1) NOT NULL,
  `orderly_safe` bit(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `query` text NOT NULL,
  `web_environment_id` int(11) NOT NULL,
  PRIMARY KEY (`xml2db_id`),
  KEY `FK_bsn0dk2l7ajq0wjx39dvxlr0f` (`web_environment_id`),
  CONSTRAINT `FK_bsn0dk2l7ajq0wjx39dvxlr0f` FOREIGN KEY (`web_environment_id`) REFERENCES `web_environments` (`web_environment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.xml2db_params: ~2 rows (approximately)
/*!40000 ALTER TABLE `xml2db_params` DISABLE KEYS */;
INSERT INTO `xml2db_params` (`xml2db_id`, `ignored_value`, `mods1`, `mods2`, `version`, `orderly_safe_array`, `orderly_safe_inc`, `orderly_safe`, `name`, `query`, `web_environment_id`) VALUES
	(0, 'fake', 'fake', 'fake', 21, b'0', b'0', b'0', 'fake', 'fake_', 0),
	(1, 'ignore', '', '', 77, b'1', b'0', b'1', '1', '12345', 1),
	(2, 'ignore', '', '', 3, b'0', b'1', b'1', 'getDealers DB', 'SELECT 1;', 2);
/*!40000 ALTER TABLE `xml2db_params` ENABLE KEYS */;

-- Dumping structure for table diff_app.xml2xml_params
CREATE TABLE IF NOT EXISTS `xml2xml_params` (
  `xml2xml_id` int(11) NOT NULL AUTO_INCREMENT,
  `attr_command` text NOT NULL,
  `node_command` text NOT NULL,
  `mods1` text NOT NULL,
  `mods2` text NOT NULL,
  `version` bigint(20) NOT NULL DEFAULT '0',
  `orderly_safe_children` bit(1) NOT NULL,
  `orderly_safe` bit(1) NOT NULL,
  `name` varchar(255) NOT NULL,
  `web_environment1_id` int(11) NOT NULL,
  `web_environment2_id` int(11) NOT NULL,
  `needle_helper` varchar(255) NOT NULL,
  PRIMARY KEY (`xml2xml_id`),
  KEY `FK_bgsqms7ve36wnxp030ul9c2c4` (`web_environment1_id`),
  KEY `FK_phap8xqeh8vyk64sdv1v70uac` (`web_environment2_id`),
  CONSTRAINT `FK_bgsqms7ve36wnxp030ul9c2c4` FOREIGN KEY (`web_environment1_id`) REFERENCES `web_environments` (`web_environment_id`),
  CONSTRAINT `FK_phap8xqeh8vyk64sdv1v70uac` FOREIGN KEY (`web_environment2_id`) REFERENCES `web_environments` (`web_environment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table diff_app.xml2xml_params: ~3 rows (approximately)
/*!40000 ALTER TABLE `xml2xml_params` DISABLE KEYS */;
INSERT INTO `xml2xml_params` (`xml2xml_id`, `attr_command`, `node_command`, `mods1`, `mods2`, `version`, `orderly_safe_children`, `orderly_safe`, `name`, `web_environment1_id`, `web_environment2_id`, `needle_helper`) VALUES
	(0, 'fake', 'fake', 'fake', 'fake', 14, b'0', b'0', 'fake', 0, 0, ''),
	(1, '', '', '', '', 75, b'1', b'0', '1', 1, 2, ''),
	(2, '', '', '', '', 13, b'1', b'1', 'getDealers UAT and PROD CA', 2, 3, '');
/*!40000 ALTER TABLE `xml2xml_params` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
