package diffapp.testLib.Helpers

import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import groovy.util.logging.Slf4j

@Slf4j
class SqlHelper{

    public static ConfigObject props;

    protected static Sql _createSqlConnProps(String name = "", Map outProps = null){
        Map _props = outProps != null ? outProps : props;
        if(_props != null && _props.containsKey(name + "DbUrl") && _props.containsKey(name + "DbUser") && _props.containsKey(name + "DbPass") && _props.containsKey(name + "DbDriver")){
            return Sql.newInstance(
                url: _props."${name}DbUrl".toString(),
                user: _props."${name}DbUser".toString(),
                password: _props."${name}DbPass".toString(),
                driver: _props."${name}DbDriver".toString()
            );
        }else{
            throw new Exception("EMException: Can't create SQL instance");
        }
    }
    
    public static Sql createSqlConn(String name){
        _createSqlConnProps(name);
    }
    
    public static Sql createSqlConn(Map outProps){
        _createSqlConnProps("", outProps);
    }

    /**
     * Creates SQL query string from file name.
     * File with specified name will be looked among project Resources in {@code props.sqlDir}.
     * Usually {@code props} are parameters that were read from 'Properties.groovy'
     * @param fileLocation
     * @return
     */
    public static String getQueryFromFile(String fileLocation){
        String query = "";
        InputStream file = SqlHelper.class.classLoader.getResourceAsStream(fileLocation);

        if(file != null){
            file.readLines().each{String line ->
                query += "\r\n" + line;
            };
        }else{
            throw new Exception("EMException: SQL file $fileLocation not found");
        }
        
        if(query.substring(query.size() - 2) == ";"){
            query = query.substring(0, query.size() - 2)
        }

        return query;
    }
    
    public static String getQueryFromMap(Map params){
        if(!params.containsKey("query")){
            throw new Exception("EMException: 'query' param is missing. Check your DbConfigs.");
        }
        return getQueryFromFile(params.query);;
    }
    
    public static String applyInVars(Map<String,Object> vars, String query){
        vars.each{String key, Object val ->
            if(key.substring(0,3) == "in_"){
                query = query.replace(":$key", val.toString());
            }
        }
        return query;
    }
    
    public static Map<String, String> getInParams(Map<String,Object> vars){
        HashMap<String, String> result = new HashMap<String, String>();
        vars.each{String key, Object val->
            if(val instanceof List || val instanceof Object[]){
                result["in_$key"] = "'${val.toList().join("','")}'";
            }
        };
        return result;
    }
    
    public static List<GroovyRowResult> execute(Sql sql, String query, Map<String, Object> structure = null){
        List<GroovyRowResult> result;
        Map<String,String> inParams = getInParams(structure);
        String _query = applyInVars(inParams, query);
        if(_query.indexOf(":") >=0 && structure != null){
            result = sql.rows(structure, _query);
        }else{
            result = sql.rows(_query);
        }
        return result;
    }
    
    public static List<GroovyRowResult> executeFile(Sql sql, String fileName, Map<String, Object> structure = null){
        String query = getQueryFromFile(fileName);
        return execute(sql, query, structure);
    }
}