package diffapp.testLib.Infostructure

import diffapp.testLib.Util.JsonUtil
import groovy.util.logging.Slf4j
import groovy.util.slurpersupport.NodeChild

@Slf4j
public abstract class AbstractDiffHelper{

    public Closure<Boolean> watchDog;

    public Boolean showErrors = true;
    /*public String output1 = "";
    public String output2 = "";*/
    protected Boolean notified = false;

    public List outputList1;
    public List outputList2;
    
    public List source1;
    public List source2;

    public Map<String, String> modifications1 = new HashMap<String, String>();
    public Map<String, String> modifications2 = new HashMap<String, String>();

    abstract public void setupFromConfig(Map params);

    abstract public void calcDiff();
    
    protected Boolean _isSimilar(List source1, List source2, String name1, String name2){
        if(source1 == null){
            throw new Exception("EMException: Call 'calcDiff()' before using this method!");
        }

        Boolean result;
        int passed_cnt1 = outputList1.size();
        int passed_cnt2 = outputList2.size();
        int failed_cnt1 = source1.size() - passed_cnt1;
        int failed_cnt2 = source2.size() - passed_cnt2;

        if(failed_cnt1 == 0 && failed_cnt2 == 0){
            result = true;
        }else{
            if(failed_cnt1 != 0 && failed_cnt2 == 0){
                log.info "Looks like '$name1' contains additional Elements... Count = '$failed_cnt1'";
            }else if(failed_cnt1 == 0 && failed_cnt2 != 0){
                log.info "Looks like '$name2' contains additional Elements... Count = '$failed_cnt2'";
            }else{
                log.info "Total mismatch counts: $name1: '$failed_cnt1', $name2: '$failed_cnt2'";
                log.info "Total matches counts: $name1: '$passed_cnt1', $name2: '$passed_cnt2'";
            }
            result = false;
        }
        return result;
    }
    
    public Boolean isSimilarNew(){
        if(outputList1 == null && outputList2 == null){
            throw new Exception("EMException: Call 'calcDiff()' before using this method!");
        }

        return outputList1.size() == 0 && outputList2.size() == 0;
    }

    protected Closure _getCommandFromParams(Map params, String name){
        if(params.containsKey(name + "_path")){
            String[] path = params.get(name + "_path").toString().split(">");
            try{
                return Class.forName(path[0]).&"${path[1]}";
            }catch(Exception e){
                log.info "EMException: Can't create Closure for '$name' from specified path: '${path}'";
            }
        }
    }
    
    protected String _applyModifications(Map<String,String> mods, String key, String value, Boolean applied = false){
        String result = value;
        if(mods.size() > 0){
            if(mods.containsKey(key)){
                mods.get(key).split("\\.").each{String mod ->
                    result = result."$mod"();
                }
                applied = true;
            }else{
                Closure command = _getCommandFromParams(mods, key);
                if(command != null){
                    result = command(result);
                    applied = true;
                }
            }

            if(!applied){
                _applyModifications(mods, "_all", result, true);
            }
        }
        
        return result;
    }

    public static String getOutputListAsString(String format, List outputList){
        String result = "";
        outputList.each{el->
            result += getOutputElementAsString(format, el);
        }
        return result;
    }
    
    public static String getOutputElementAsString(String format, Object element){
        String result;
        //if(format.toUpperCase() == "XML"){
        if(element instanceof NodeChild){
            result = groovy.xml.XmlUtil.serialize(element).substring(38);
        }else{
            result += JsonUtil.printMap(element);
        }
        return result;
    }

}
