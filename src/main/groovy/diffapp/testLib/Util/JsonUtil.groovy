package diffapp.testLib.Util

import groovy.json.JsonOutput

/**
 * Created by tymofieieva on 12/2/2015.
 */
class JsonUtil{
    public static List walkJsonByPath(String listPath, def structure){
        List result = [];
        if(listPath != ""){
            String[] pathNodes = listPath.trim().split("\\.");
            result = _help(pathNodes, structure);
        }
        return result;
    }
    
    protected static List _help(String[] pathNodes, def structure){
        List result = [];
        if(pathNodes.size() > 0){
            List target;
            if(structure instanceof List){
                target = structure;
            }else if(structure instanceof Map){
                target = [structure];
            }
            if(target.size() > 0){
                String pathNode = pathNodes[0];
                List newTarget = [];
                target.each{curEl ->
                    if(curEl instanceof List && pathNode.substring(0, 1) == ">"){
                        try{
                            int idx = Integer.parseInt(pathNode.substring(1));
                            if(idx < curEl.size()){
                                newTarget.add(curEl.getAt(idx));
                            }
                        }catch(Exception e){
                        }
                    }else if(curEl instanceof Map && curEl.containsKey(pathNode)){
                        newTarget.addAll(_help(pathNodes.drop(1), curEl[pathNode]));
                    }
                }
                if(newTarget.size() > 0){
                    result = newTarget;
                }
            }
        }else{
            result = structure;
        }
        return result;
    }

    public static String printMap(Map source){
        return JsonOutput.prettyPrint(JsonOutput.toJson(source)) + ",\r\n";
    }
}
