class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                controller inList: [
                    "home",
                    "dataPromotion",
                    "comboTest",
                    "offersTester",
                    "simpleComparison",
                    "csvTest",
                    "modificatorCreator",
                    "db2DbComparison"
                ]
            }
        }

        "/tests"(resources:"test")
        "/testCases"(resources:"testCase")
        "/testCaseRuns"(resources:"testCaseRun")

        "/webTests"(resources:"webTest")
        "/webEnvironments"(resources:"webEnvironment")
        "/dbSettings"(resources:"dbSetting")
        "/arrayComboGroups"(resources:"arrayComboGroup")
        "/sqlComboGroups"(resources:"sqlComboGroup")
        "/xml2XmlParams"(resources:"xml2XmlParam")
        "/xml2DbParams"(resources:"xml2DbParam")

        "/arrayComboParams"(resources:"arrayComboParam")
        "/sqlComboParams"(resources:"sqlComboParam")
        "/includedMappings"(resources:"includedMapping")
        "/ignoredElements"(resources:"ignoredElement")

        "/dbDrivers"(resources:"dbDriver")

        "/simpleComparisonSettings"(resources:"simpleComparisonSetting")

        "/csv2DbParams"(resources:"csv2DbParam")
        "/csvColumns"(resources:"csvColumn")
        "/csvSettings"(resources:"csvSetting")
        "/ignoredRows"(resources:"ignoredRow")
        "/modificators"(resources:"modificator")
        "/db2DbParams"(resources:"db2DbParam")

        "/$action?"(controller: "home")

        "500"(view:"/error")
        "404"(view:"/notFound")
    }
}
