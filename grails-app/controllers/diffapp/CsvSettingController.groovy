package diffapp

class CsvSettingController extends BaseRestfulController{

    static responseFormats = ['json'];

    CsvSettingController(){
        super(CsvSetting);
    }
}