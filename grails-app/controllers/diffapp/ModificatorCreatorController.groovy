package diffapp

import grails.converters.JSON
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j

@Slf4j
class ModificatorCreatorController{

    HashMap<String, Object> viewModel = ["title":"Nissan Testing Tool"];
    
    def index(){
        return viewModel;
    }
    
    def tryTest(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();
        
        def params = request.JSON;
        
        try{
            String input = params.testParams.input.toString();
            String key = params.testParams.key.toString();
            Map obj = new JsonSlurper().parseText(params.testParams.obj.toString());
            
            input = input == "_NULL_" ? null : input;
            obj[key] = input;

            Binding binding = new Binding();
            binding.setProperty("value", input);
            binding.setProperty("key", key);
            binding.setProperty("obj", obj);
            binding.setProperty("result", "");
            GroovyShell shell = new GroovyShell(binding);
            shell.evaluate(params.mod.code.toString());
            
            viewModel["result"] = [
                //modified: Eval.me('value', input, params.code.toString())
                modified: binding.getProperty("result").toString()
            ];
        }catch(Exception e){
            log.info("Error: '${e.getMessage()}'");
            viewModel['errors'] = e.getMessage();
            response.status = 404;
        }
        
        render viewModel as JSON;
    }
}
