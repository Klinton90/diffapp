package diffapp

import grails.converters.JSON
import groovy.util.logging.Slf4j

@Slf4j
class DataPromotionController{
    DataPromotionService dataPromotionService
    
    String errorMessage = "";
    
    HashMap<String, Integer> cadaProgress = [
        "cur": 0,
        "max": 0
    ];
    
    List<Map<String, String>> cadaData;
    
    def index(){
        return [
            "title": "Test: Data promotion",
            "params": DataPromotionService.getDefaultParams()
        ];
    }
    
    def checkCada(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();
        String isCadaRunning = OtherParam.findByParamType("isCadaRunning")?.value;
        if(isCadaRunning == "N" || isCadaRunning == null){
            cadaData = null;
            errorMessage = "";
            cadaProgress["cur"] = 0;
            cadaProgress["max"] = 0;
            def jsonParams = request.JSON
            
            if(dataPromotionService.validateYears(jsonParams)){
                OtherParam.createOrUpdate(jsonParams["cadaYearCodes"], "cadaYearCodes");
                OtherParam.createOrUpdate("Y", "isCadaRunning");
                
                Thread.start{
                    log.info("inProgress");
                    try{
                        cadaData = dataPromotionService.checkCada(jsonParams, cadaProgress);
                        log.info("Complete");
                    }catch(Exception e){
                        errorMessage = e.message;
                        log.info("Error: ${e.message}");
                    }finally{
                        OtherParam.withNewSession{
                            OtherParam.createOrUpdate("N", "isCadaRunning");
                        }
                    }
                };
            }else{
                viewModel['errors'] = "Year Codes doesn't exist in DB";
                response.status = 404;
            }
        }

        viewModel["result"] = [
            "isCadaRunning": true,
            "cadaProgress": cadaProgress
        ];
        
        render viewModel as JSON;
    }
    
    def isCadaRunning(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();
        viewModel["result"] = [
            "isCadaRunning": OtherParam.findByParamType("isCadaRunning")?.value == "Y",
            "cadaProgress": cadaProgress
        ];
        render viewModel as JSON;
    }
    
    def getCadaResult(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();
        if(errorMessage != null && errorMessage != ""){
            viewModel["errors"] = errorMessage;
            response.status = 404;
        }else{
            viewModel["result"] = cadaData ?: "";
        }
        
        render viewModel as JSON;
    }
    
    def checkDb(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();
        try{
            OtherParam.createOrUpdate(request.JSON["excludedMfgCodes"], "excludedMfgCodes");
            viewModel["result"] = dataPromotionService.checkDb(request.JSON);
        }catch(Exception e){
            response.status = 404;
            viewModel["errors"] = e.message;
        }
        render viewModel as JSON;
    }
    
    def checkHidden(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();
        try{
            OtherParam.createOrUpdate(request.JSON["excludedAcodes"], "excludedAcodes");
            viewModel["result"] = dataPromotionService.checkHidden(request.JSON);
        }catch(Exception e){
            response.status = 404;
            viewModel["errors"] = e.message;
        }
        render viewModel as JSON;
    }
    
    def enableAcodes(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();
        if(dataPromotionService.validateAcodes(request.JSON)){
            try{
                dataPromotionService.enableAcodes(request.JSON);
                viewModel["result"] = [status: "Ok"];
            }catch(Exception e){
                response.status = 404;
                viewModel["errors"] = e.message;
            }
        }else{
            response.status = 404;
            viewModel["errors"] = "Some selected Acodes are already enabled or not exist in DB. Refresh Disabled Acodes List and try again";
        }
        
        render viewModel as JSON;
    }
    
    def backupRevCodes(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();
        def jsonParams = request.JSON

        if(dataPromotionService.validateYears(jsonParams)){
            OtherParam.createOrUpdate(jsonParams["cadaYearCodes"], "cadaYearCodes");

            try{
                dataPromotionService.backupRevCodes(jsonParams);
                viewModel["result"] = [status: "Ok"];
            }catch(Exception e){
                response.status = 404;
                viewModel["errors"] = e.message;
            }
        }
        
        return  viewModel as JSON;
    }
    
    def checkSpecialPackages(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();

        try{
            viewModel["result"] = dataPromotionService.checkSpecialPackages();
        }catch(Exception e){
            response.status = 404;
            viewModel["errors"] = e.message;
            log.info(e.message);
        }

        render viewModel as JSON;
    }
    
    def enableSpecialPackages(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();
        def params = request.JSON;
        
        ArrayList<Map<String, String>> pairs = dataPromotionService.prepareSpecialPackagesPairs(params.enableSpecialPackagesList);
        if(dataPromotionService.validateSpecialPackages(pairs)){
            try{
                dataPromotionService.enableSpecialPackages(pairs);
                viewModel["result"] = [status: "Ok"];
            }catch(Exception e){
                response.status = 404;
                viewModel["errors"] = e.message;
                log.info(e.message);
            }
        }else{
            response.status = 404;
            viewModel["errors"] = "Some selected Acodes-OptCodes are already enabled or not exist in DB. Refresh Disabled Acodes-OptCodes List and try again";
        }
        
        render viewModel as JSON;
    }

    def disableSpecialPackages(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();
        def params = request.JSON;

        ArrayList<Map<String, String>> pairs = dataPromotionService.prepareSpecialPackagesPairs(params.disableSpecialPackagesList);
        if(dataPromotionService.validateSpecialPackages(pairs, true)){
            try{
                dataPromotionService.disableSpecialPackages(pairs);
                viewModel["result"] = [status: "Ok"];
            }catch(Exception e){
                response.status = 404;
                viewModel["errors"] = e.message;
                log.info(e.message);
            }
        }else{
            response.status = 404;
            viewModel["errors"] = "Some selected Acodes-OptCodes are already enabled or not exist in DB. Refresh Disabled Acodes-OptCodes List and try again";
        }

        render viewModel as JSON;
    }
}
