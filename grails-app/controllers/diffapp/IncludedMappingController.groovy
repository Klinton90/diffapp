package diffapp

class IncludedMappingController extends BaseRestfulController{
    static responseFormats = ['json'];

    IncludedMappingController(){
        super(IncludedMapping);
    }
}
