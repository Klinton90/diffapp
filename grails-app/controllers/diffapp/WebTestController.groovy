package diffapp

class WebTestController extends BaseRestfulController{

    static responseFormats = ['json'];

    WebTestController(){
        super(WebTest);
    }
}
