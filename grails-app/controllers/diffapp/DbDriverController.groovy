package diffapp

class DbDriverController extends BaseRestfulController{
    static responseFormats = ['json'];

    DbDriverController(){
        super(DbDriver);
    }
}
