package diffapp

class IgnoredRowController extends BaseRestfulController{

    static responseFormats = ['json'];

    IgnoredRowController(){
        super(IgnoredRow);
    }
}