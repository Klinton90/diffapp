package diffapp

class CsvColumnController extends BaseRestfulController{

    static responseFormats = ['json'];

    CsvColumnController(){
        super(CsvColumn);
    }
}