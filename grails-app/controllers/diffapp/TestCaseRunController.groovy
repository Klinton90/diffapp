package diffapp

class TestCaseRunController extends BaseRestfulController{
    static responseFormats = ['json'];

    def TestCaseRunController(){
        super(TestCaseRun);
    }
}
