package diffapp

import grails.converters.JSON
import groovy.util.logging.Slf4j
import org.springframework.web.multipart.MultipartFile

@Slf4j
class OffersTesterController{

    HashMap<String, Object> viewModel = ["title":"Nissan Testing Tool"];
    
    OffersTesterService offersTesterService;
    
    OffersFilesService offersFilesService;

    String errorMessage = "";
    
    Boolean isOffersRunning = false;
    
    ArrayList<HashMap<String, Object>> offersData;
    
    HashMap<String, Integer> offersProgress = [
        "cur": 0,
        "max": 0
    ];
    
    def index(){
        viewModel.put("params", OtherParam.findByParamType("offersParams").value);
        return viewModel;
    }
    
    def testOffers(){
        HashMap<String, Object> viewModel = new HashMap<String, Object> ();
        
        if(!isOffersRunning){
            Map<String, Object> jsonParams = request.JSON;
            errorMessage = "";
            offersProgress["cur"] = 0;
            offersProgress["max"] = 0;
            offersData = null; 
            isOffersRunning = true;

            errorMessage = offersTesterService.prepareInputs(jsonParams);
            if(errorMessage.size() == 0){
                OtherParam.createOrUpdate(jsonParams, "offersParams")
                Thread.start{
                    log.info("In progress");
                    try{
                        offersData = offersTesterService.testOffers(jsonParams, offersProgress);
                        log.info("Complete");
                    }catch(Exception e){
                        errorMessage = e.getMessage();
                        log.info("Error: "+errorMessage);
                    }finally{
                        isOffersRunning = false;
                    }
                }
            }else{
                isOffersRunning = false;
                response.status = 404;
            }
        }
        
        viewModel["result"] = [
            isOffersRunning: true,
            "offersProgress": offersProgress
        ];
        viewModel['errors'] = errorMessage;

        render viewModel as JSON;
    }

    def isOffersRunning(){
        HashMap<String, Object> viewModel = new HashMap<String, Object> ();
        viewModel["result"] = [
            "isOffersRunning": isOffersRunning,
            "offersProgress": offersProgress,
        ];
        viewModel["errors"] = errorMessage;

        if(!isOffersRunning){
            errorMessage = "";
        }

        render viewModel as JSON;
    }
    
    def getOffersResult(){
        HashMap<String, Object> viewModel = new HashMap<String, Object> ();
        if(errorMessage != null && errorMessage != ""){
            viewModel["errors"] = errorMessage;
            response.status = 404;
        }else{
            viewModel["result"] = offersData ?: "";
            //offersData = null;
        }

        render viewModel as JSON;
    }
    
    def downloadFiles(){
        HashMap<String, Object> viewModel = new HashMap<String, Object> ();
        
        try{
            List<String> files = new ArrayList<String>();
            offersFilesService.downloadFiles().each{File f->
                files.add(f.name);
            };
            viewModel["result"] = [
                files: files
            ];
        }catch(Exception e){
            log.info(e.getMessage());
            viewModel['errors'] = e.getMessage();
            response.status = 404;
        }
        
        render viewModel as JSON;
    }
    
    def fileAction(){
        String fileName = params.fileName;
        String format = fileName.substring(fileName.size() - 4);

        render(
            file: offersFilesService.getFeedFile(fileName),
            contentType: "application/${format}",
            fileName: fileName
        );
    }
    
    def uploadFile(){
        HashMap<String, Object> viewModel = new HashMap<String, Object> ();
        
        try{
            offersFilesService.moveUploadedFile(params.data);
            viewModel['status'] = 'ok';
        }catch (Exception e){
            log.info(e.getMessage());
            viewModel['errors'] = e.getMessage();
            response.status = 404;
        }
        
        render viewModel as JSON;
    }
}
