package diffapp

class Xml2DbParamController extends BaseRestfulController{

    static responseFormats = ['json'];

    Xml2DbParamController(){
        super(Xml2DbParam);
    }
}
