package diffapp

import grails.converters.JSON

class Db2DbComparisonController{

    HashMap<String, Object> viewModel = ["title":"Nissan Testing Tool"];

    Db2DbComparisonService db2DbComparisonService;

    def index(){
        return viewModel;
    }

    def runTest(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();
        int id = request.JSON.id;
        
        Db2DbParam db2DbParam = Db2DbParam.getFullTree(id);
        if(db2DbParam != null){
            try{
                viewModel['result'] = db2DbComparisonService.runTest(db2DbParam);
            }catch(CustomException e){
                log.info(e.getMessage());
                viewModel['errors'] = e.getMessage();
                response.status = 404;
            }
        }else{
            viewModel['errors'] = "Test not found!";
            response.status = 404;
        }

        render viewModel as JSON;
    }

    def startTest(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();
        int id = request.JSON.id;
        String error = "";
        QService.Q q;

        try{
            q = db2DbComparisonService.startTest(id);
            error = q.error;
        }catch(Exception e){
            error = e.message;
            db2DbComparisonService.queue.removeQ(id);
        }
        if(error.size() > 0){
            log.info("Error: ${error}");
            viewModel["errors"] = error;
            response.status = 404;
        }else{
            viewModel["result"] = q.getStatus();
        }

        render viewModel as JSON;
    }

    def checkTest(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();
        int id = request.JSON.id;
        QService.Q q = db2DbComparisonService.queue.getQ(id);

        if(q.error.size() > 0){
            db2DbComparisonService.queue.removeQ(id);
            log.info("Error: ${q.error}");
            viewModel["errors"] = q.error;
            response.status = 404;
        }else{
            viewModel["result"] = q.getDiffResult();
        }

        render viewModel as JSON;
    }
}
