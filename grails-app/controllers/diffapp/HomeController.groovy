package diffapp

import grails.converters.JSON

class HomeController{

    HashMap<String, Object> viewModel = ["title":"Nissan Testing Tool"];

    def index(){
        return viewModel;
    }
    
    def contact(){
        return viewModel;
    }

    def status(){
        return viewModel;
    }

    def nna(){
        return viewModel;
    }

    def nci(){
        return viewModel;
    }

    def getDbSettings(){
        viewModel["dbDrivers"] = DbDriver.list();
        viewModel['dbSettings'] = DbSetting.list();
        return viewModel as JSON;
    }
}
