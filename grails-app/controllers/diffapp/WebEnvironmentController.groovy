package diffapp

class WebEnvironmentController extends BaseRestfulController{

    static responseFormats = ['json'];

    WebEnvironmentController(){
        super(WebEnvironment);
    }
}
