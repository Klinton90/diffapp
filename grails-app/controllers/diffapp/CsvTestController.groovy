package diffapp

import grails.converters.JSON

class CsvTestController{

    HashMap<String, Object> viewModel = ["title":"Nissan Testing Tool"];
    
    CsvTestService csvTestService;

    def index(){
        return viewModel;
    }
    
    def runTest(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();
        def id = params.id;
        def feed = params.feed;

        Csv2DbParam csv2DbParam = Csv2DbParam.getFullTree(id);
        if(csv2DbParam != null){
            try{
                viewModel['result'] = csvTestService.runTest(csv2DbParam, csvTestService.moveUploadedFile(feed));
            }catch(CustomException e){
                log.info(e.getMessage());
                viewModel['errors'] = e.getMessage();
                response.status = 404;
            }
        }else{
            viewModel['errors'] = "Test not found!";
            response.status = 404;
        }

        render viewModel as JSON;
    }
    
    def startTest(){
        Integer id = Integer.parseInt(params.id);
        def feed = params.feed;
        HashMap<String, Object> viewModel = new HashMap<String, String> ();
        String error = "";
        QService.Q q;
        
        //try{
            q = csvTestService.startTestCsv(id, csvTestService.moveUploadedFile(feed));
            error = q.error;
        /*}catch(Exception e){
            error = e.message;
            csvTestService.queue.removeQ(id);
        }*/
        if(error.size() > 0){
            log.info("Error: ${error}");
            viewModel["errors"] = error;
            response.status = 404;
        }else{
            viewModel["result"] = q.getStatus();
        }

        render viewModel as JSON;
    }
    
    def checkTest(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();
        int id = request.JSON.id;
        QService.Q q = csvTestService.queue.getQ(id);

        if(q.error.size() > 0){
            csvTestService.queue.removeQ(id);
            log.info("Error: ${q.error}");
            viewModel["errors"] = q.error;
            response.status = 404;
        }else{
            viewModel["result"] = q.getDiffResult();
        }

        render viewModel as JSON;
    }
    
    def unlockTest(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();
        int id = request.JSON.id;

        QService.Q q = csvTestService.queue.getQ(id);
        csvTestService.queue.removeQ(id);
        
        render viewModel as JSON;
    }
}
