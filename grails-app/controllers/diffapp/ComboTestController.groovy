package diffapp

import grails.converters.JSON
import groovy.util.logging.Slf4j

@Slf4j
class ComboTestController{

    HashMap<String, Object> viewModel = ["title":"Nissan Testing Tool"];

    ComboTestService comboTestService;

    def index(){
        return viewModel;
    }

    def testCreator(){
        return viewModel;
    }

    def testCaseCreator(){
        return viewModel;
    }
    
    def dbMappingCreator(){
        return viewModel;
    }

    def runTestCase(){
        String error;
        int id = request.JSON.id;
        HashMap<String, Object> viewModel = new HashMap<String, String> ();
        TestCaseRun tcr = ComboTestService.getLatestTCR(id);
        
        if(tcr == null || tcr.status != "RUNNING"){
            TestCaseRun newTcr;
            try{
                newTcr = comboTestService.initTestCaseRun(id);
            }catch(Exception e){
                error = e.message;
            }
            
            if(newTcr != null){
                tcr = newTcr;
                Thread.start{
                    log.info("Start");
                    TestRun.withNewSession{session->
                        try{
                            comboTestService.runTestCase(tcr);
                            log.info("Complete");
                        }catch(Exception e){
                            log.info("Error: '${e.message}");
                            TestCaseRun.withNewTransaction{
                                comboTestService.finalizeTestCaseRun(tcr, e.getMessage(), "ERROR");
                            }
                        }
                    }
                };
            }
        }
        
        if(error != null){
            log.info("Error: ${error}");
            viewModel["errors"] = error;
            response.status = 404;
        }else{
            viewModel["result"] = [
                "testCaseRunResult": tcr
            ];
        }

        render viewModel as JSON;
    }
    
    def isTestCaseRunning(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();

        TestCaseRun tcr;
        if(request.JSON.testCaseRunId != null && request.JSON.testCaseRunId > 0){
            tcr = TestCaseRun.get(request.JSON.testCaseRunId);
        }else{
            tcr = ComboTestService.getLatestTCR(request.JSON.id);
        }
        
        viewModel["result"] = [
            "testCaseRunResult": tcr
        ];
        
        render viewModel as JSON;
    }

    def fileAction(){
        TestRun testRun = TestRun.get(params.testRunId.toInteger());
        String format = testRun.test.webTest1.format;

        if(params.type == "2" && testRun.test.xml2DbParamId > 0){
            format = "json";
        }
        File file = comboTestService.getFileResult(params.testCaseRunId, params.testRunId, params.iterationRunId, params.type, format, Boolean.parseBoolean(params.original));

        render(
            file: file, 
            contentType: "application/${format}",
            fileName: "${testRun.test.name}_${params.type}.${format}".replace(" ", "_")
        );
    }
    
    def fileData(){
        TestRun testRun = TestRun.get(params.testRunId.toInteger());
        String format = testRun.test.webTest1.format;

        if(params.type == "2" && testRun.test.xml2DbParamId > 0){
            format = "json";
        }
        File file = comboTestService.getFileResult(params.testCaseRunId, params.testRunId, params.iterationRunId, params.type, format, Boolean.parseBoolean(params.original));

        render file.text.toString();
    }
    
    def getDbMapping(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();

        if(request.JSON.xmlSource != null && request.JSON.xmlSource.size() > 0){
            try{
                viewModel.put("result", ["dbResult": ComboTestService.getDbMapping(request.JSON.xmlSource)]);
            }catch(Exception e){
                viewModel.put("errors", "'xmlSource' can't be read, it contains non-valid XML. Initial Exception message: \n\r ${e.message}");
                response.status = 404;
            }
        }else{
            viewModel.put("errors", "'xmlSource' can't be null.");
            response.status = 404;
        }

        render viewModel as JSON;
    }
    
    def unlockTestCase(){
        HashMap<String, Object> viewModel = new HashMap<String, String> ();

        TestCaseRun tcr = ComboTestService.getLatestTCR(params.id.toInteger());
        tcr.status = "CANCELING";
        tcr.save();
        
        render viewModel as JSON;
    }
    
    def findTCRbyTC(){
        ArrayList<TestCaseRun> viewModel = TestCaseRun.createCriteria().list{
            eq("testCase.id", request.JSON.testCaseId)
            order("id", "desc")
        };

        JSON.use("simple", {
            render viewModel as JSON;
        });
    }
}
