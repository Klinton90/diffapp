package diffapp

class IgnoredElementController extends BaseRestfulController{
    static responseFormats = ['json'];

    IgnoredElementController(){
        super(IgnoredElement);
    }
}
