//= require app/app.js
//= require_self

angular.module("diffapp").controller("dbMappingCreator", ["$scope", "$http", function($scope, $http){
    var $ctrl = this;
    
    this.init = function(){
        $scope.adErrors = "";
        $scope.htmlError = "";
    };
    
    this.createMapping = function(){
        $http({
            method: 'POST',
            url: '/comboTest/getDbMapping',
            data: {xmlSource: $scope.xmlSource}
        }).then(function(response){
            $scope.adErrors = "";
            $scope.htmlError = "";
            $scope.dbResult = response.data.result.dbResult;
        },
        function(response){
            if(response.data.errors != null){
                $scope.adErrors = response.data.errors;
            }else{
                $scope.htmlError = response;
            }
        });
    };
    
    $ctrl.init();
}]);