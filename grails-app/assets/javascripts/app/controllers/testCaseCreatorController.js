//= require app/app.js
//= require_self

angular.module("diffapp").controller("testCaseCreatorController", ["$scope", "$http", "$controller", "resUtil", function($scope, $http, $controller, resUtil) {
    var $ctrl = this;

    angular.extend(this, $controller("baseFormController", {$scope: $scope}));

    $ctrl.incEntities = {
        testCase: [
            {
                name: "dbSetting",
                type: "single",
                source: "form2"
            },
            {
                name: "webEnvironment1",
                type: "single",
                source: "form2"
            },
            {
                name: "webEnvironment2",
                type: "single",
                source: "form2"
            },
            {
                name: "test",
                type: "multiple"
            }
        ],
        dbSetting:[
            {
                name: "dbDriver",
                type: "single"
            }
        ]
    };
    
    $ctrl.entMapping = {
        webEnvironment1: "webEnvironment",
        webEnvironment2: "webEnvironment"
    };

    this.init = function(){
        $ctrl.getEntList("testCase", false, {new: true, mType: "nameId"});

        $scope.curTestCase = {};
        $scope.adErrors = "";

        $scope.form2Struct = {name: "", model: {}};
    };

    this.initTooltip = function(){
        $('[data-toggle="tooltip"]').tooltip();
    };

    this.loadTestCase = function(testCase){
        if(testCase.id != $scope.curTestCase.id){
            if($ctrl.formSanityCheck(["testCase", $scope.form2Struct.name])){
                if(testCase.id > 0){
                    resUtil["testCase"].get({id: testCase.id, mType: "deep"}, function(response){
                        $scope.curTestCase = response;
                        angular.forEach($ctrl.incEntities.testCase, function(form){
                            //$scope[form.name+'Tmp'] = angular.copy($scope.curTestCase[form.name]);
                            if(form.name == "test"){
                                $scope["_" + form.name + "Tmp"] = [];
                            }else{
                                $scope[form.name + "Tmp"] = angular.copy($scope.curTestCase[form.name]);
                            }
                        });
                        //$scope._testTmp = [];
                    });
                }else{
                    $scope.curTestCase = angular.copy(testCase);
                    angular.forEach($ctrl.incEntities.testCase, function(form){
                        if(form.name == "test"){
                            $scope[form.name] = [];
                        }else{
                            $scope[form.name + "Tmp"] = {};
                        }
                    });
                }

                angular.forEach($ctrl.incEntities.testCase, function(form){
                    $ctrl.getEntList(form.name, false, {new: form.source != null, mType: "nameId"});
                });
            }
        }
    };
    
    this.submitTestCase = function(){
        $ctrl.saveSingleForm(false, {name: "testCase", model: $scope.curTestCase}, null, [$scope.form2Struct.name], function(response){
            $scope.curTestCase = angular.copy(response);
            $scope.curTest = null;
        });
    };

    this.canReloadForm2 = function(model, name, changed){
        return $ctrl.canReloadSingleForm(model, name, changed, $scope.form2Struct, {model: $scope.curTestCase, name: "testCase"});
    };

    this.loadForm2 = function(model, name, changed){
        $ctrl.loadSingleForm(model, name, changed, $scope.form2Struct, {model: $scope.curTestCase, name: "testCase"});
    };

    this.submitForm2 = function(force){
        $ctrl.saveSingleForm(force, $scope.form2Struct, {name: "testCase", model: $scope.curTestCase}, null);
    };

    this.selectTest = function(test){
        $scope.curTest = angular.copy(test);
    };

    this.addTest = function(){
        $scope.curTest.testCase = [{id: $scope.curTestCase.id}];
        $scope.curTestCase.test = $ctrl.replaceInList($scope.curTestCase.test, $scope.curTest);
        $scope.form_testCase.testCase_test.$setDirty();
    };

    this.removeTest = function(){
        angular.forEach($scope._testTmp, function(test){
            $ctrl.removeFromList($scope.curTestCase.test, test);
        });
        $scope.form_testCase.testCase_test.$setDirty();
    };

    this.init();
}]);