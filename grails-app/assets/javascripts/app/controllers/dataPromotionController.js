//= require app/app.js
//= require_self

angular.module('diffapp').controller('dataPromotionController',
    ['$scope', '$http', '$timeout',
        function($scope, $http, $timeout){
            var $ctrl = this;
            $ctrl.cadaPromise = null;
            
            $scope.statuses = {
                OK : "All data is Up To Date.",
                WARN: "Differences has been found. Please review table below.",
                ERROR: "Oops! Something went wrong.",
                INFO: "Press Button to Launch testing.",
                INFO2: "In progress.",
                BACKUP: "RevCodes BackUp has been saved"
            };
            
            this.init = function(){
                /*$scope.excludedMfgCode = "3CDG75,3KCG75,3KEG75,5XCH15,5XEH15";
                $scope.excludedAcode = 'CAC30NIC151A1,CAC30NIS111A1,CAC30NIS112A1,CAC30NIS112B1,CAC50INC272B0,CAC50INC282B0,CAC50INS152A0,CAC50INS161A0,CAC60NIS111B0,CAC60NIS112B0,CAC60NIT161C0,CAC60NIT161E0';
                $scope.cadaYearCodes = 'C60';*/
                $scope.enableAcodesList = [];
                $scope.enableSpecialPackagesList = [];
                $scope.disableSpecialPackagesList = [];
                $scope.isCadaRunning = false;
                $scope.isDbRunning = false;
                $scope.isHiddenRunning = false;
                $scope.isBackuping = false;
                $scope.isSpecialPackagesRunning = false;
                $scope.cadaData = {
                    status: "INFO",
                    model: {}
                };
                $scope.dbData = {
                    status: "INFO",
                    model: {}
                };
                $scope.hiddenData = {
                    status: "INFO",
                    model: {},
                    list: []
                };
                $scope.specialPackagesData = {
                    status: "INFO",
                    error: "",
                    model: {
                        new: {},
                        red: {}
                    },
                    list: {
                        new: [],
                        red: []
                    }
                }
            };
            
            this.initPopovers = function(row){
                $('[data-toggle="popover"]').popover();
            };
            
            this.getCadaRevCodesTemplate = function(els){
                var rows = "";
                angular.forEach(els, function(el){
                    rows += 
                        "<tr>" +
                            "<td>"+el.RevCode+"</td>" +
                            "<td>"+el.RevNote+"</td>" +
                            "<td>"+el.RevLocked+"</td>" +
                            "<td>"+el.isDelayed+"</td>"+
                        "</tr>";
                });
                return "<table class='table table-condensed table-striped'>" +
                        "<thead>" +
                            "<th>RevCode</th>" +
                            "<th>RevNote</th>" +
                            "<th>RevLocked</th>" +
                            "<th>Is Delayed?</th>" +
                        "</thead>" +
                        "<tbody>" +
                            rows +
                        "</tbody>" +
                    "</table>";
            };
            
            this.checkCada = function(){
                $scope.cadaData = {
                    status: "INFO2",
                    model: {}
                };
                $scope.adErrors = "";
                $scope.htmlError = "";
                $scope.isCadaRunning = true;
                
                $http({
                    method: 'post',
                    url: '/dataPromotion/checkCada',
                    data: {cadaYearCodes: $scope.cadaYearCodes.replace(/\s+/g, '').split(",")}
                }).then(function(response){
                    $ctrl.cadaStatusHandler(response);
                }, function(response){
                    $ctrl.errorHandler(response);
                    $scope.cadaData.status = "ERROR";
                    $scope.isCadaRunning = false;
                });
            };
            
            this.checkCadaStatus = function(){
                if($ctrl.cadaPromise != null){
                    $timeout.cancel($ctrl.cadaPromise);
                }
                $http({
                    method: 'get',
                    url: '/dataPromotion/isCadaRunning'
                }).then(function(response){
                    $ctrl.cadaStatusHandler(response);
                });
            };

            this.cadaStatusHandler = function(response){
                var cur = response.data.result.cadaProgress.cur - (response.data.result.cadaProgress.cur > 0 ? 0.5 : 0);
                $scope.cadaProgress = Math.round(cur*100/(response.data.result.cadaProgress.max ? response.data.result.cadaProgress.max : 1));
                $scope.isCadaRunning = response.data.result.isCadaRunning;
                if(!$scope.isCadaRunning){
                    $ctrl.getCadaResult();
                }else{
                    $ctrl.cadaPromise = $timeout(function(){
                        $ctrl.checkCadaStatus();
                    }, 5000);
                }
            };
            
            this.getCadaResult = function(){
                $http({
                    method: 'get',
                    url: '/dataPromotion/getCadaResult'
                }).then(function(response){
                    $scope.cadaData.model = response.data.result;
                    if(response.data.result.length > 0){
                        $scope.cadaData.status = "WARN";
                    }else{
                        $scope.cadaData.status = "OK";
                    }
                },function(response){
                    $scope.cadaData.status = "ERROR";
                    $ctrl.errorHandler(response);
                });
            };

            this.backupRevCodes = function(){
                $scope.adErrors = "";
                $scope.htmlError = "";
                $scope.isBackuping = true;
                $scope.cadaData.status = "INFO2";

                $http({
                    method: 'post',
                    url: '/dataPromotion/backupRevCodes',
                    data: {cadaYearCodes: $scope.cadaYearCodes.replace(/\s+/g, '').split(",")}
                }).then(function(response){
                    $scope.isBackuping = false;
                    $scope.cadaData.status = "BACKUP";
                }, function(response){
                    $ctrl.errorHandler(response);
                    $scope.cadaData.status = "ERROR";
                    $scope.isBackuping = false;
                });
            };
            
            this.checkDb = function(){
                $scope.dbData = {
                    status: "INFO2",
                    model: {}
                };
                $scope.adErrors = "";
                $scope.htmlError = "";
                $scope.isDbRunning = true;
                
                $http({
                    method: 'post',
                    url: '/dataPromotion/checkDb',
                    data: {excludedMfgCodes: $scope.excludedMfgCode.replace(/\s+/g, '').split(",")}
                }).then(function(response){
                    var res = response.data.result;
                    $scope.isDbRunning = false;
                    $scope.dbData.model = res;
                    if(res.revCode.length > 0 || res.ei02.length > 0 || res.vt86.length > 0 || res.incentives.length > 0){
                        $scope.dbData.status = "WARN";
                    }else{
                        $scope.dbData.status = "OK";
                    }
                }, function(response){
                    $scope.isDbRunning = false;
                    $scope.dbData.status = "ERROR";
                    $ctrl.errorHandler(response);
                });
            };
            
            this.checkHidden = function(){
                $scope.hiddenData = {
                    status: "INFO2",
                    model: {},
                    list: []
                };
                $scope.adErrors = "";
                $scope.htmlError = "";
                $scope.isHiddenRunning = true;

                $http({
                    method: 'post',
                    url: '/dataPromotion/checkHidden',
                    data: {excludedAcodes: $scope.excludedAcode.replace(/\s+/g, '').split(",")}
                }).then(function(response){
                    $scope.isHiddenRunning = false;
                    $scope.hiddenData.model = response.data.result;
                    angular.forEach($scope.hiddenData.model, function(row){
                        if($scope.hiddenData.list.indexOf(row.Acode) == -1){
                            $scope.hiddenData.list.push(row.Acode);
                        }
                    });
                    if($scope.hiddenData.model.length > 0){
                        $scope.hiddenData.status = "WARN";
                    }else{
                        $scope.hiddenData.status = "OK";
                    }
                }, function(response){
                    $scope.isHiddenRunning = false;
                    $scope.hiddenData.status = "ERROR";
                    $ctrl.errorHandler(response);
                });
            };
            
            this.enableAcodes = function(){
                $scope.adErrors = "";
                $scope.htmlError = "";
                $scope.enableAcodesSelect = "";
                if($scope.enableAcodesList.length > 0){
                    $scope.hiddenData.status = "INFO2";
                    $scope.isHiddenRunning = true;
                    $http({
                        method: 'post',
                        url: '/dataPromotion/enableAcodes',
                        data: {
                            excludedAcodes: $scope.excludedAcode.replace(/\s+/g, '').split(","),
                            enableAcodesList : $scope.enableAcodesList
                        }
                    }).then(function(response){
                        $ctrl.checkHidden();
                        $scope.isHiddenRunning = false;
                    }, function(response){
                        if(response.data.errors != null){
                            $scope.enableAcodesSelect = response.data.errors;
                        }else{
                            $scope.htmlError = response;
                        }
                        $scope.hiddenData.status = "ERROR";
                        $scope.isHiddenRunning = false;
                    });
                }else{
                    $scope.enableAcodesSelect = "No Acodes selected.";
                }
            };
            
            this.checkSpecialPackages = function(){
                $scope.specialPackagesData = {
                    status: "INFO2",
                    model: {
                        new: {},
                        red: {}
                    },
                    list: {
                        new: [],
                        red: []
                    }
                };
                $scope.adErrors = "";
                $scope.htmlError = "";
                $scope.isSpecialPackagesRunning = true;
                
                $http({
                    method: "get",
                    url: '/dataPromotion/checkSpecialPackages'
                }).then(function(response){
                    $scope.isSpecialPackagesRunning = false;
                    $scope.specialPackagesData.model.new = response.data.result.newSpecialPackages;
                    $scope.specialPackagesData.model.red = response.data.result.redundantSpecialPackages;
                    angular.forEach($scope.specialPackagesData.model.new, function(row){
                        var needle = $.grep($scope.specialPackagesData.list.new, function(e){
                            return e.Acode == row.Acode && e.OptCode == row.OptCode;
                        });
                        if(needle.length == 0){
                            $scope.specialPackagesData.list.new.push({
                                id: '{"Acode" : "'+row.Acode+'", "OptCode" : "'+row.OptCode+'"}',
                                name: row.Acode + " : " + row.OptCode
                            });
                        }
                    });
                    angular.forEach($scope.specialPackagesData.model.red, function(row){
                        var needle = $.grep($scope.specialPackagesData.list.red, function(e){
                            return e.Acode == row.Acode && e.OptCode == row.OptCode;
                        });
                        if(needle.length == 0){
                            $scope.specialPackagesData.list.red.push({
                                id: '{"Acode" : "'+row.acode+'", "OptCode" : "'+row.OptCode+'"}',
                                name: row.acode + " : " + row.OptCode
                            });
                        }
                    });
                    if($scope.specialPackagesData.model.new.length > 0 || $scope.specialPackagesData.model.red.length > 0 ){
                        $scope.specialPackagesData.status = "WARN";
                    }else{
                        $scope.specialPackagesData.status = "OK";
                    }
                }, function(response){
                    $scope.isSpecialPackagesRunning = false;
                    $scope.specialPackagesData.status = "ERROR";
                    $ctrl.errorHandler(response);
                });
            };
            
            this.enableSpecialPackages = function(){
                $scope.adErrors = "";
                $scope.htmlError = "";
                $scope.enableSpecialPackagesSelectErr = "";
                if($scope.enableSpecialPackagesList.length > 0){
                    $scope.specialPackagesData.status = "INFO2";
                    $scope.isSpecialPackagesRunning = true;
                    $http({
                        method: 'post',
                        url: '/dataPromotion/enableSpecialPackages',
                        data: {
                            enableSpecialPackagesList : $scope.enableSpecialPackagesList
                        }
                    }).then(function(response){
                        $ctrl.checkSpecialPackages();
                        $scope.isSpecialPackagesRunning = false;
                    }, function(response){
                        if(response.data.errors != null){
                            $scope.enableSpecialPackagesSelectErr = response.data.errors;
                        }else{
                            $scope.htmlError = response;
                        }
                        $scope.specialPackagesData.status = "ERROR";
                        $scope.isSpecialPackagesRunning = false;
                    });
                }else{
                    $scope.enableSpecialPackagesSelectErr = "No Special Packages selected.";
                }
            };

            this.disableSpecialPackages = function(){
                $scope.adErrors = "";
                $scope.htmlError = "";
                $scope.disableSpecialPackagesSelectErr = "";
                if($scope.disableSpecialPackagesList.length > 0){
                    $scope.specialPackagesData.status = "INFO2";
                    $scope.isSpecialPackagesRunning = true;
                    $http({
                        method: 'post',
                        url: '/dataPromotion/disableSpecialPackages',
                        data: {
                            disableSpecialPackagesList : $scope.disableSpecialPackagesList
                        }
                    }).then(function(response){
                        $ctrl.checkSpecialPackages();
                        $scope.isSpecialPackagesRunning = false;
                    }, function(response){
                        if(response.data.errors != null){
                            $scope.disableSpecialPackagesSelectErr = response.data.errors;
                        }else{
                            $scope.htmlError = response;
                        }
                        $scope.specialPackagesData.status = "ERROR";
                        $scope.isSpecialPackagesRunning = false;
                    });
                }else{
                    $scope.disableSpecialPackagesSelectErr = "No Special Packages selected.";
                }
            };
            
            this.errorHandler = function(response){
                if(response.data.errors != null){
                    $scope.adErrors = response.data.errors;
                }else{
                    $scope.htmlError = response;
                }
            };
            
            this.init();
        }
    ]
);