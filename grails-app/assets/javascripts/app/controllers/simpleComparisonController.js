//= require app/app.js
//= require_self

angular.module('diffapp').controller('simpleComparisonController',
    ['$scope', '$http', "$controller", "resUtil",
        function($scope, $http, $controller, resUtil){
            angular.extend(this, $controller("baseFormController", {$scope: $scope}));
            var $ctrl = this;

            $scope.codemirrorOptions = {
                lineWrapping : true,
                lineNumbers: true,
                theme:'eclipse',
                mode: 'text/javascript',
                autoRefresh: true,
                delay: 250
            };
            
            $ctrl.lastResult = {};
            this.defSettings = {
                id: -1,
                name: '',
                hMethod: 'GET',
                format: 'XML'
            };

            $scope.statuses = {
                OK : "Match!",
                WARN: "Differences has been found. Please review table below.",
                ERROR: "",
                ERROR2: "Oops! Something went wrong.",
                INFO: "Press 'Start' Button to Launch testing.",
                INFO2: "In progress."
            };
            $scope.xmlPathTooltip = 'Path to LIST elements that consists of NodeNames.\r\nFor example, path to INVENTORY element is "data.list.inventory":\r\n' +
                '<result>\r\n' +
                '    <id>getTrimInventory</id>\r\n' +
                '    <data>\r\n' +
                '        <list>\r\n' +
                '           <inventory>\r\n' +
                '               <vin>123456</vin>\r\n' +
                '               <acode>XGC60NIT161A0</acode>\r\n' +
                '           </inventory>\r\n' +
                '           <inventory>\r\n' +
                '               <vin>654321</vin>\r\n' +
                '               <acode>XGC60NIT161B0</acode>\r\n' +
                '           </inventory>\r\n' +
                '        </list>\r\n' +
                '    </data>\r\n' +
                '</result>\r\n';
            $scope.ignoredElementsTooltip = 'Format: "ignored1,ignored2=val1,@ignoredAttr2,ignored3.@ignoredAttr,ignored>ingoredChild.@ignoredAttrChild=val4"\r\n' +
                'ignored1 - all NODES with name "ignored1" will be ignored\r\n' +
                'ignored2=val1 - all NODES with name "ignored2" that has TEXT="val1" will be ignored\r\n' +
                '@ignoredAttr1 - all ATTRIBUTES with name "ignoredAttr1" will be ignored\r\n' +
                'ignored3.@ignoredAttr2 - all ATTRIBUTES with name "ignoredAttr2" that belongs to NODE with name "ignored3" will be ignored\r\n' +
                'ignored4>ingoredChild.@ignoredAttrChild=val4 - all NODES with name "ignored4" that have child NODE with name "ingoredChild" and ATTRIBUTE "ignoredAttrChild" with value "val4" will be ignored';
            
            this.init = function(){
                $('[data-toggle="tooltip"]').tooltip();
                $(document).keypress(function(e) {
                    if(e.which == 13) {
                        e.preventDefault();
                        $ctrl.testXml();
                    }
                });
                
                $scope.statusXml = 'INFO';
                $scope.curSimpleComparisonSetting = angular.copy($ctrl.defSettings);
                var defSettings = $scope.defScId > 0 ? {id: $scope.defScId} : angular.copy($ctrl.defSettings);
                
                $ctrl.getEntList("simpleComparisonSetting", false, {new: true, mType: "nameId"}, function(){
                    $ctrl.loadSettings(defSettings);
                });
            };
            
            this.testXml = function(){
                if($scope.statusXml != 'INFO2'){
                    $scope.statusXml = 'INFO2';
                    $scope.showGetLinks = false;
                    $scope.showPostLinks = false;
                    $("#originaloutput").empty();
                    $("#prepoutput").empty();
                    $("#diffoutput").empty();

                    $http({
                        method: 'post',
                        url: '/simpleComparison/testXml',
                        data: $scope.curSimpleComparisonSetting
                    }).then(function(response){
                        $ctrl.lastResult = response.data.result;
                        $("#originaloutput").append($ctrl.getDiffOutput(response.data.result.original1, response.data.result.original2));
                        $("#prepoutput").append($ctrl.getDiffOutput(response.data.result.prep1, response.data.result.prep2));
                        
                        if(!response.data.result.passed){
                            $scope.statusXml = 'WARN';
                            $("#diffoutput").append($ctrl.getDiffOutput(response.data.result.out1, response.data.result.out2));
                            if($scope.autoScroll){
                                $('html, body').animate({
                                    scrollTop: output.offset().top
                                }, 200);
                            }
                        }else{
                            $scope.statusXml = 'OK';
                        }
                        
                        if($scope.curSimpleComparisonSetting.hMethod == 'GET'){
                            $scope.showGetLinks = true;
                            $scope.showPostLinks = false;
                        }else{
                            $scope.showGetLinks = false;
                            $scope.showPostLinks = true;
                        }
                    }, function(response){
                        $ctrl.errorHandler(response);
                    });
                }
            };
            
            this.loadSettings = function(simpleComparisonSetting){
                if(simpleComparisonSetting.id != $scope.curSimpleComparisonSetting.id){
                    $scope.statusXml = 'INFO';
                    $scope.showGetLinks = false;
                    $scope.showPostLinks = false;
                    $("#diffoutput").empty();
                    $("#originaloutput").empty();

                    if($ctrl.formSanityCheck(["simpleComparisonSetting"])){
                        if(simpleComparisonSetting.id > 0){
                            resUtil["simpleComparisonSetting"].get({id: simpleComparisonSetting.id, mType: "deep"}, function(response){
                                $scope.curSimpleComparisonSetting = response;
                            });
                        }else{
                            $scope.curSimpleComparisonSetting = angular.copy($ctrl.defSettings);
                        }
                    }
                }
            };
            
            this.saveSettings = function(copy){
                if(copy){
                   $scope.curSimpleComparisonSetting.id = -1; 
                }
                $ctrl.saveSingleForm(false, {name: "simpleComparisonSetting", model: $scope.curSimpleComparisonSetting}, null, [], function(response){
                    $scope.curSimpleComparisonSetting = angular.copy(response);
                });
            };
            
            this.openPostLink = function(link){
                var data = link == 2 ? $ctrl.lastResult.original2 : $ctrl.lastResult.original1;
                var newWin = open('','Link');
                newWin.document.write('<xmp>'+data+'</xmp>');
            };

            this.errorHandler = function(response){
                if(response.data.errors != null){
                    $scope.statusXml = 'ERROR';
                    $scope.statuses.ERROR = response.data.errors;
                }else{
                    $scope.statusXml = 'ERROR2';
                    $("#htmlError").html(response.data);
                }
            };
        }
    ]
);
