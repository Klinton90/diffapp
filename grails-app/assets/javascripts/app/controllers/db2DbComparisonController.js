//= require app/app.js
//= require_self

angular.module('diffapp').controller('db2DbComparisonController',
    ['$scope', '$http', "$controller", "$timeout", "resUtil",
        function($scope, $http, $controller, $timeout, resUtil){
            angular.extend(this, $controller("baseFormController", {$scope: $scope}));
            var $ctrl = this;

            $scope.codemirrorOptions = {
                //lineWrapping : true,
                lineNumbers: true,
                theme:'eclipse',
                mode: 'text/x-mysql',
                autoRefresh: true,
                delay: 250,
                onLoad: function(_cm){
                    $ctrl.cm = _cm;
                }
            };

            $ctrl.incEntities = {
                db2DbParam:[
                    {
                        name: "dbSetting1",
                        type: "single",
                        source: "form2"
                    },
                    {
                        name: "dbSetting2",
                        type: "single",
                        source: "form2"
                    }
                ],
                dbSetting1:[
                    {
                        name: "dbDriver",
                        type: "single"
                    }
                ],
                dbSetting2:[
                    {
                        name: "dbDriver",
                        type: "single"
                    }
                ]
            };
            
            $ctrl.entMapping={
                dbSetting1: "dbSetting",
                dbSetting2: "dbSetting"
            };

            $scope.statuses = {
                OK : "Match!",
                WARN: "Differences has been found. Please review table below.",
                ERROR: "",
                ERROR2: "Oops! Something went wrong.",
                INFO: "Press 'Start' Button to Launch testing.",
                INFO2: "In progress."
            };

            this.init = function(){
                $('[data-toggle="tooltip"]').tooltip();
                
                
                $scope.status = 'INFO';
                $scope.curDb2DbParam = {};
                $scope.form2Struct = {name: "", model: {}};
                $scope.form3Struct = {name: "", model: {}};
                $scope.form4Struct = {name: "", model: {}};
                $scope.form5Struct = {name: "", model: {}};

                $scope.form3Tmp = [];
                $scope.form5Tmp = [];

                $scope.resultObject = {
                    /*totalOriginal1: 0,
                    totalOriginal2: 0,
                    totalPrep1: 0,
                    totalPrep2: 0,*/
                    totalOut1: 0,
                    totalOut2: 0
                };
                
                $ctrl.getEntList("db2DbParam", false, {new: true, mType: "nameId"});
            };

            this.startTest = function(){
                $scope.adErrors = "";
                $scope.status = "INFO2";
                //$("#originaloutput").empty();
                //$("#prepoutput").empty();
                $("#diffoutput").empty();

                $http({
                    url: "/db2DbComparison/startTest",
                    method: "POST",
                    data: {
                        'id': $scope.curDb2DbParam.id
                    }
                }).then(function(response){
                    $ctrl.processTestResult(response.data.result);
                }, function(response){
                    $ctrl.errorHandler(response);
                });
            };
            
            this.checkTest = function(change){
                if($ctrl.testPromise != null){
                    $timeout.cancel($ctrl.testPromise);
                }
                $http({
                    method: "POST",
                    url: "/db2DbComparison/checkTest",
                    data: {
                        id: $scope.curDb2DbParam.id
                    }
                }).then(function(response){
                    $ctrl.processTestResult(response.data.result);
                }, function(response){
                    if(!change){
                        $ctrl.errorHandler(response);
                    }else{
                        $scope.status = 'INFO';
                    }
                });
            };

            this.processTestResult = function(response){
                if(response.isRunning){
                    $scope.status = "INFO2";
                    $scope.progress = Math.round(response.counter*100/(response.total > 0 ? response.total : 1));
                    $ctrl.testPromise = $timeout(function(){
                        $ctrl.checkTest();
                    }, 5000);
                }else{
                    $ctrl._showResults(response);
                }
            };
            
            this._showResults = function(response){
                $scope.resultObject = {
                    /*totalOriginal1: response.totalOriginal1,
                    totalOriginal2: response.totalOriginal2,
                    totalPrep1: response.totalPrep1,
                    totalPrep2: response.totalPrep2,*/
                    totalOut1: response.totalOut1,
                    totalOut2: response.totalOut2
                };
                //$("#originaloutput").append($ctrl.getDiffOutput(response.original1, response.original2));
                //$("#prepoutput").append($ctrl.getDiffOutput(response.prep1, response.prep2));

                if(!response.passed){
                    $scope.status = 'WARN';
                    $("#diffoutput").append($ctrl.getDiffOutput(response.out1, response.out2, $scope.curDb2DbParam.dbSetting1.name, $scope.curDb2DbParam.dbSetting2.name));
                }else{
                    $scope.status = 'OK';
                }
            };

            this.errorHandler = function(response){
                if(response.data.errors != null){
                    $scope.status = 'ERROR';
                    $scope.statuses.ERROR = response.data.errors;
                }else{
                    $scope.status = 'ERROR2';
                    $scope.htmlError = response.data;
                }
            };
            
            this.loadTest = function(db2DbParam){
                if(db2DbParam.id != $scope.curDb2DbParam.id){
                    if($ctrl.formSanityCheck([$scope.form5Struct.name, $scope.form4Struct.name, $scope.form3Struct.name, $scope.form2Struct.name, 'db2DbParam'])){
                        if(db2DbParam.id > 0){
                            resUtil["db2DbParam"].get({id: db2DbParam.id, mType: "deep"}, function(response){
                                $scope.curDb2DbParam = response;
                                $ctrl.checkTest(true);
                                angular.forEach($ctrl.incEntities.db2DbParam, function(form){
                                    $scope[form.name + 'Tmp'] = angular.copy($scope.curDb2DbParam[form.name]);
                                });
                            });
                        }else{
                            $scope.curDb2DbParam = angular.copy(db2DbParam);
                            $scope.curDb2DbParam.query1 = "SELECT * FROM";
                            angular.forEach($ctrl.incEntities.db2DbParam, function(form){
                                $scope[form.name + "Tmp"] = {};
                            });
                        }
                        $scope.form2Struct = {name: "", model: {}};
                        $scope.form3Struct = {name: "", model: {}};
                        $scope.form4Struct = {name: "", model: {}};
                        $scope.form5Struct = {name: "", model: {}};
                        angular.forEach($ctrl.incEntities.db2DbParam, function(form){
                            $ctrl.getEntList(form.name, false, {new: form.source != null, mType: "nameId"});
                        });
                    }
                }
            };

            this.submitTest = function(){
                $ctrl.saveSingleForm(false, {name: "db2DbParam", model: $scope.curDb2DbParam}, null, [$scope.form2Struct.name, $scope.form3Struct.name, $scope.form4Struct.name, $scope.form5Struct.name], function(response){
                    $scope.curDb2DbParam = angular.copy(response);
                });
            };

            this.canReloadForm2 = function(model, name, changed){
                return $ctrl.canReloadSingleForm(model, name, changed, $scope.form2Struct, {model: $scope.curDb2DbParam, name: "db2DbParam"}, [$scope.form5Struct.name]);
            };

            this.loadForm2 = function(model, name, changed){
                $ctrl.loadSingleForm(model, name, changed, $scope.form2Struct, {name: "db2DbParam", model: $scope.curDb2DbParam}, function(){
                    $scope.form3Struct = {name: "", model: {}};
                    $scope.form4Struct = {name: "", model: {}};
                    $scope.form5Struct = {name: "", model: {}};
                    $scope.form5Tmp[0] = {};
                });
            };

            this.submitForm2 = function(force){
                $ctrl.saveSingleForm(force, $scope.form2Struct, {name: "db2DbParam", model: $scope.curDb2DbParam}, [$scope.form3Struct.name, $scope.form4Struct.name, $scope.form5Struct.name], function(response){
                    $scope.form3Struct = {name:"", model:{}};
                    $scope.form3Tmp = [];
                });
            };

            this.loadForm3 = function(name, model){
                $ctrl.loadMultiForm(model, name, $scope.form3Struct, $scope.form2Struct, [$scope.form4Struct.name], function(){
                    $scope.form4Struct = {name: "", model: {}};
                });
            };

            this.newItem3 = function(name, namingField){
                $ctrl.newMultiItem(name, $scope.form3Struct, $scope.form2Struct, [$scope.form4Struct.name], namingField, function(){
                    $scope.form4Struct = {name: "", model: {}};
                });
            };

            this.submitForm3 = function(){
                $ctrl.submitMultiForm($scope.form3Struct, $scope.form2Struct);
            };

            this.deleteForm3 = function(){
                $ctrl.deleteMultiForm($scope.form3Struct, $scope.form2Struct);
            };

            this.canReloadForm4 = function(model, name, changed){
                return $ctrl.canReloadSingleForm(model, name, changed, $scope.form4Struct, $scope.form2Struct);
            };

            this.loadForm4 = function(model, name, changed){
                $ctrl.loadSingleForm(model, name, changed, $scope.form4Struct, $scope.form2Struct, function(){
                    $scope.form3Struct = {name: "", model: {}};
                });
            };

            this.submitForm4 = function(force){
                $ctrl.saveSingleForm(force, $scope.form4Struct, $scope.form2Struct, ["db2DbParam"]);
            };

            this.loadForm5 = function(name, model){
                $ctrl.loadMultiForm(model, name, $scope.form5Struct, {name: "db2DbParam", model: $scope.curDb2DbParam}, [$scope.form2Struct.name], function(){
                    $scope.form2Struct = {name: "", model: {}};
                    $scope.form3Struct = {name: "", model: {}};
                    
                    if(name == "ignoredRow"){
                        $scope.hasChildren = $ctrl.hasChildren(model.id);
                    }
                });
            };

            this.newItem5 = function(name, namingField){
                $ctrl.newMultiItem(name, $scope.form5Struct, {name: "db2DbParam", model: $scope.curDb2DbParam}, [$scope.form2Struct.name], namingField, function(){
                    $scope.form2Struct = {name: "", model: {}};
                    $scope.form3Struct = {name: "", model: {}};
                });
            };

            this.submitForm5 = function(){
                $ctrl.submitMultiForm($scope.form5Struct, {name: "db2DbParam", model: $scope.curDb2DbParam});
            };

            this.deleteForm5 = function(){
                $ctrl.deleteMultiForm($scope.form5Struct, {name: "db2DbParam", model: $scope.curDb2DbParam});
            };
        }
    ]
);