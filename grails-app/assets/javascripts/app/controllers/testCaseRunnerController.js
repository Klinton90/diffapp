//= require app/app.js
//= require_self

angular.module("diffapp").controller("testCaseRunnerController", ["$scope", "$http", "$controller", "$timeout", "$q", "resUtil", function($scope, $http, $controller, $timeout, $q, resUtil) {
    var $ctrl = this;

    angular.extend(this, $controller("baseFormController", {$scope: $scope}));

    this.init = function(){
        $scope.adErrors = "";
        $scope.htmlError = "";
        $scope.testRunInProgress = {id:0};
        $scope.testRunStatuses = [];
        
        $ctrl.getEntList("testCase", false, {mType: "nameId"}, function(){
            if($scope.defTestCaseId > 0){
                $ctrl.loadTestCase($scope.defTestCaseId);
            }
        });
    };
    
    this.initTooltip = function(){
        $('[data-toggle="tooltip"]').tooltip();
    };
    
    this.resetStatus = function(){
        $("#diffoutput").empty();
        $scope.testCaseRunResult = null;
        $scope.curTestRun = null;
        $scope.isTestCaseRunning = false;
    };

    this.loadTestCase = function(testCaseId){
        $scope.testRunInProgress = {id:0};
        $scope.testRunStatuses = [];
        $ctrl.resetStatus();
        
        resUtil["testCase"].get({id: testCaseId, mType: "deep"}, function(response){
            $scope.curTestCase = response;
            $ctrl.checkTestCaseStatus(true);
            $http({
                method: "POST",
                url: "/comboTest/findTCRbyTC/",
                data: {testCaseId: testCaseId}
            }).then(function(response){
                $scope.testCaseRunList = response.data;
            },
            function(response){
                $ctrl.errorHandler(response);
            });
        });
    };

    this.runTestCase = function(){
        $scope.testRunInProgress = {id:0};
        $scope.testRunStatuses = [];
        $ctrl.resetStatus();
        
        $http({
            method: 'POST',
            url: '/comboTest/runTestCase',
            data: {id: $scope.curTestCase.id}
        }).then(function(response){
            $ctrl.testCaseStatusHandler(response);
            $scope.testCaseRunList.push(response.data.result.testCaseRunResult);
            $scope.curTestCaseRun = response.data.result.testCaseRunResult;
        },
        function(response){
            $ctrl.errorHandler(response);
        });
    };
    
    this.checkTestCaseStatus = function(force, testCaseRunId){
        if($ctrl.testCasePromise != null){
            $timeout.cancel($ctrl.testCasePromise);
        }
        $http({
            method: "POST",
            url: "/comboTest/isTestCaseRunning",
            data: {
                id: $scope.curTestCase.id,
                testCaseRunId: testCaseRunId
            }
        }).then(function(response){
            $ctrl.testCaseStatusHandler(response);
            if(force || testCaseRunId > 0){
                $scope.curTestCaseRun = response.data.result.testCaseRunResult;
            }
        }, function(response){
            $ctrl.errorHandler(response);
        });
    };
    
    this.testCaseStatusHandler = function(response){
        $scope.adErrors = "";
        $scope.htmlError = "";
        var tcr = response.data.result.testCaseRunResult;
        if(tcr != null){
            var totalTests = tcr.testCase.test.length;
            var completedTests = 0;
            var totalIterations = 1;
            var completedIterations = 0;
            
            $scope.isTestCaseRunning = tcr.status == "RUNNING" || tcr.status == "CANCELING";
            $scope.testCaseRunResult = tcr;
            
            if(tcr.testRun != null){
                angular.forEach(tcr.testRun, function(c_tr){
                    var testRunStatus = $scope.testRunStatuses[c_tr.id];
                    if(!testRunStatus){
                        testRunStatus = {passed: true, total: 0};
                        $scope.testRunStatuses[c_tr.id] = testRunStatus;
                    }
                    
                    if(c_tr.iterationRun != null && testRunStatus.total < c_tr.iterationRun.length){
                        for(var i = 0; i < c_tr.iterationRun.length; i++){
                            if(!c_tr.iterationRun[i].passed){
                                testRunStatus.passed = false;
                                break;
                            }
                        }
                    }
                    
                    if($scope.testRunInProgress.id <= c_tr.id){
                        $scope.testRunInProgress = c_tr;
                    }

                    if(c_tr.id == $scope.curTestRunId){
                        $scope.curTestRun = c_tr;
                    }
                });
                
                totalTests = tcr.testCase.test.length;
                completedTests = tcr.testRun.length - (tcr.testRun.length > 0 ? 0.5 : 0);
                totalIterations = $scope.testRunInProgress.total > 0 ? $scope.testRunInProgress.total : 1;
                completedIterations = $scope.testRunInProgress.iterationRun != null ? $scope.testRunInProgress.iterationRun.length : 0;
            }

            $scope.testCaseProgress = Math.round(completedTests*100/totalTests);
            $scope.testProgress = Math.round(completedIterations*100/totalIterations);
        }
        
        if($scope.isTestCaseRunning){
            $ctrl.testCasePromise = $timeout(function(){
                $ctrl.checkTestCaseStatus();
            }, 5000);
        }
    };

    this.errorHandler = function(response){
        if(response.data.errors != null){
            $scope.adErrors = response.data.errors;
        }else{
            $scope.htmlError = response;
        }
    };

    this.loadTestRun = function(testRun){
        $("#diffoutput").empty();
        $scope.curTestRunId = testRun.id;
        $scope.curTestRun = testRun;
    };
    
    this.compareResults = function(row){
        var p1 = $http.get('/comboTest/fileData?testCaseRunId='+$scope.testCaseRunResult.id+'&testRunId='+$scope.curTestRun.id+'&iterationRunId='+row.id+'&type=1');
        var p2 = $http.get('/comboTest/fileData?testCaseRunId='+$scope.testCaseRunResult.id+'&testRunId='+$scope.curTestRun.id+'&iterationRunId='+row.id+'&type=2');

        $q.all([p1,p2]).then(function(res){
            var diffoutputdiv = $("#diffoutput").empty().append($ctrl.getDiffOutput(res[0].data, res[1].data));

            $('html, body').animate({
                scrollTop: diffoutputdiv.offset().top
            }, 200);
        });
    };
    
    this.unlockTestCase = function(){
        $http({
            method: "GET",
            url: "/comboTest/unlockTestCase/"+$scope.curTestCase.id
        }).then(function(response){
            $ctrl.checkTestCaseStatus();
        }, function(response){
            $ctrl.errorHandler(response);
        });
    };
    
    this.loadOldTestCaseRun = function(){
        if($scope.curTestCaseRun){
            $ctrl.checkTestCaseStatus(true, $scope.curTestCaseRun.id);
        }
    };
}]);