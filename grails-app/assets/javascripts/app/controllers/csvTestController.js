//= require app/app.js
//= require_self

angular.module('diffapp').controller('csvTestController',
    ['$scope', '$http', "$controller", "$timeout", "resUtil",
        function($scope, $http, $controller, $timeout, resUtil){
            angular.extend(this, $controller("baseFormController", {$scope: $scope}));
            var $ctrl = this;
            
            $ctrl.resultObject = {
                totalOriginal1: 0,
                totalOriginal2: 0,
                totalPrep1: 0,
                totalPrep2: 0,
                totalOut1: 0,
                totalOut2: 0
            };

            $scope.codemirrorOptions = {
                //lineWrapping : true,
                lineNumbers: true,
                theme:'eclipse',
                mode: 'text/x-mysql',
                autoRefresh: true,
                delay: 250
            };

            $ctrl.incEntities = {
                csv2DbParam:[
                    {
                        name: "dbSetting",
                        type: "single",
                        source: "form2"
                    },
                    {
                        name: "csvSetting",
                        type: "single",
                        source: "form2"
                    },
                    {
                        name: "ignoredRow",
                        type: "multiple",
                        source: "form5"
                    }
                ],
                dbSetting:[
                    {
                        name: "dbDriver",
                        type: "single"
                    }
                ],
                csvSetting:[
                    {
                        name: "csvColumn",
                        type: "multiple",
                        source: "form3"
                    }
                ],
                csvColumn:[
                    {
                        name: "modificator",
                        type: "single",
                        nullable: true
                    }
                ],
                /*ignoredRow:[
                    {
                        name: "parentRow",
                        type: "single",
                        nullable: true,
                        special: true
                    }
                ]*/
            };
            
            /*$ctrl.entMapping={
                parentRow: "ignoredRow"
            };*/

            $scope.statuses = {
                OK : "Match!",
                WARN: "Differences has been found. Please review table below.",
                ERROR: "",
                ERROR2: "Oops! Something went wrong.",
                INFO: "Press 'Start' Button to Launch testing.",
                INFO2: "In progress."
            };

            this.init = function(){
                $('[data-toggle="tooltip"]').tooltip();
                
                
                $scope.status = 'INFO';
                $scope.curCsv2DbParam = {};
                $scope.form2Struct = {name: "", model: {}};
                $scope.form3Struct = {name: "", model: {}};
                $scope.form4Struct = {name: "", model: {}};
                $scope.form5Struct = {name: "", model: {}};

                $scope.form3Tmp = [];
                $scope.form5Tmp = [];

                $scope.resultObject = $ctrl.resultObject;
                
                $ctrl.getEntList("csv2DbParam", false, {new: true, mType: "nameId"});
            };
            
            this.iRowTypeChanged = function(){
                $scope.form5Struct.model.value = "";
                if($scope.form5Struct.model.type != "REGULAR"){
                    $scope.form5Struct.model.name = $scope.form5Struct.model.type;
                }else{
                    $scope.form5Struct.model.name = "";
                }
            };
            
            $ctrl._getPrefixRecursive = function(option, prefix){
                var result = prefix ? prefix : "";
                
                if(option.parentRow != null){
                    result += "--";
                    result = $ctrl._getPrefixRecursive(option.parentRow, result);
                }
                
                return result;
            };

            $ctrl._getPrefixRecursive2 = function(option, prefix){
                var result = prefix ? prefix : "";

                if(option.parentRow != null){
                    result = option.parentRow.name + " " + result;
                    result = $ctrl._getPrefixRecursive2(option.parentRow, result);
                }

                return result;
            };
            
            $scope.getIgnoredRowName = function(option){
                var result = $ctrl._getPrefixRecursive(option) + " ";
                
                if(option.type == "REGULAR"){
                    result += option.name + " = " + option.value;
                }else{
                    result += option.name;
                }
                return result;
            };
            
            $scope.myOrderBy = function(option){
                return $ctrl._getPrefixRecursive2(option) + option.name + option.value;
            };
            
            this.hasChildren = function(id){
                var result = false;
                if($scope.curCsv2DbParam && $scope.curCsv2DbParam.ignoredRow){
                    for(var i = 0; i < $scope.curCsv2DbParam.ignoredRow.length; i++){
                        var curRow = $scope.curCsv2DbParam.ignoredRow[i];
                        if(curRow.parentRow && curRow.parentRow.id == id){
                            result = true;
                            break;
                        }
                    }
                }
                return result;
            };
            
            this._prep = function(){
                $scope.adErrors = "";
                var feedFile = $("#feedFile")[0].files[0];
                if(feedFile == null){
                    $scope.status = 'ERROR';
                    $scope.statuses.ERROR = 'Please select file to upload!';
                    return false;
                }else if($scope.curCsv2DbParam == null || $scope.curCsv2DbParam.id == null || $scope.curCsv2DbParam.id <= 0){
                    $scope.status = 'ERROR';
                    $scope.statuses.ERROR = 'Please select Test Case to run!';
                    return false;
                }else{

                    $scope.status = "INFO2";
                    $("#originaloutput").empty();
                    $("#prepoutput").empty();
                    $("#diffoutput").empty();
                    $scope.resultObject = $ctrl.resultObject;
                    var formData = new FormData();
                    formData.append('feed', feedFile);
                    formData.append('id', $scope.curCsv2DbParam.id);
                    return formData;
                }
            };

            this.runTest = function(){
                var formData = $ctrl._prep();
                if(!formData){
                    return;
                }

                $http({
                    url: "/csvTest/runTest",
                    method: "POST",
                    data: formData,
                    transformRequest: angular.identity,
                    headers: {
                        'enctype': 'multipart/form-data',
                        'Content-Type': undefined
                    }
                }).then(function(response){
                    $ctrl._showResults(response.data.result);
                }, function(response){
                    $ctrl.errorHandler(response);
                });
            };

            this.startTest = function(){
                var formData = $ctrl._prep();
                if(!formData){
                    return;
                }

                $http({
                    url: "/csvTest/startTest",
                    method: "POST",
                    data: formData,
                    transformRequest: angular.identity,
                    headers: {
                        'enctype': 'multipart/form-data',
                        'Content-Type': undefined
                    }
                }).then(function(response){
                    $ctrl.processTestResult(response.data.result);
                }, function(response){
                    $ctrl.errorHandler(response);
                });
            };
            
            this.checkTest = function(change){
                if($ctrl.testPromise != null){
                    $timeout.cancel($ctrl.testPromise);
                }
                $http({
                    method: "POST",
                    url: "/csvTest/checkTest",
                    data: {
                        id: $scope.curCsv2DbParam.id
                    }
                }).then(function(response){
                    $ctrl.processTestResult(response.data.result);
                }, function(response){
                    if(!change){
                        $ctrl.errorHandler(response);
                    }else{
                        $scope.status = 'INFO';
                    }
                });
            };

            this.unlockTest = function(){
                $http({
                    method: "POST",
                    url: "/csvTest/unlockTest",
                    data: {
                        id: $scope.curCsv2DbParam.id
                    }
                }).then(function(response){
                    console.log("unlocked?");
                }, function(response){
                    if(!change){
                        $ctrl.errorHandler(response);
                    }else{
                        $scope.status = 'INFO';
                    }
                });
            };

            this.processTestResult = function(response){
                if(response.isRunning){
                    $scope.status = "INFO2";
                    $scope.progress = Math.round(response.counter*100/(response.total > 0 ? response.total : 1));
                    $ctrl.testPromise = $timeout(function(){
                        $ctrl.checkTest();
                    }, 5000);
                }else{
                    $ctrl._showResults(response);
                }
            };
            
            this._showResults = function(response){
                $scope.resultObject = {
                    totalOriginal1: response.totalOriginal1,
                    totalOriginal2: response.totalOriginal2,
                    totalPrep1: response.totalPrep1,
                    totalPrep2: response.totalPrep2,
                    totalOut1: response.totalOut1,
                    totalOut2: response.totalOut2
                };
                //$("#originaloutput").append($ctrl.getDiffOutput(response.original1, response.original2, "DataBase", "Feed"));
                //$("#prepoutput").append($ctrl.getDiffOutput(response.prep1, response.prep2, "DataBase", "Feed"));

                if(!response.passed != "true"){
                    $scope.status = 'WARN';
                    $("#diffoutput").append($ctrl.getDiffOutput(response.out1, response.out2, "DataBase", "Feed"));
                }else{
                    $scope.status = 'OK';
                }
            };

            this.errorHandler = function(response){
                if(response.data.errors != null){
                    $scope.status = 'ERROR';
                    $scope.statuses.ERROR = response.data.errors;
                }else{
                    $scope.status = 'ERROR2';
                    $scope.htmlError = response.data;
                }
            };
            
            this.loadTest = function(csv2DbParam){
                if(csv2DbParam.id != $scope.curCsv2DbParam.id){
                    if($ctrl.formSanityCheck([$scope.form5Struct.name, $scope.form4Struct.name, $scope.form3Struct.name, $scope.form2Struct.name, 'csv2DbParam'])){
                        if(csv2DbParam.id > 0){
                            resUtil["csv2DbParam"].get({id: csv2DbParam.id, mType: "deep"}, function(response){
                                $scope.curCsv2DbParam = response;
                                $ctrl.checkTest(true);
                                angular.forEach($ctrl.incEntities.csv2DbParam, function(form){
                                    $scope[form.name + 'Tmp'] = angular.copy($scope.curCsv2DbParam[form.name]);
                                });
                            });
                        }else{
                            $scope.curCsv2DbParam = angular.copy(csv2DbParam);
                            angular.forEach($ctrl.incEntities.csv2DbParam, function(form){
                                $scope[form.name + "Tmp"] = {};
                            });
                        }
                        $scope.form2Struct = {name: "", model: {}};
                        $scope.form3Struct = {name: "", model: {}};
                        $scope.form4Struct = {name: "", model: {}};
                        $scope.form5Struct = {name: "", model: {}};
                        angular.forEach($ctrl.incEntities.csv2DbParam, function(form){
                            $ctrl.getEntList(form.name, false, {new: form.source != null, mType: "nameId"});
                        });
                    }
                }
            };

            this.submitTest = function(){
                $ctrl.saveSingleForm(false, {name: "csv2DbParam", model: $scope.curCsv2DbParam}, null, [$scope.form2Struct.name, $scope.form3Struct.name, $scope.form4Struct.name, $scope.form5Struct.name], function(response){
                    $scope.curCsv2DbParam = angular.copy(response);
                });
            };

            this.canReloadForm2 = function(model, name, changed){
                return $ctrl.canReloadSingleForm(model, name, changed, $scope.form2Struct, {model: $scope.curCsv2DbParam, name: "csv2DbParam"}, [$scope.form5Struct.name]);
            };

            this.loadForm2 = function(model, name, changed){
                $ctrl.loadSingleForm(model, name, changed, $scope.form2Struct, {name: "csv2DbParam", model: $scope.curCsv2DbParam}, function(){
                    $scope.form3Struct = {name: "", model: {}};
                    $scope.form4Struct = {name: "", model: {}};
                    $scope.form5Struct = {name: "", model: {}};
                    $scope.form5Tmp[0] = {};
                });
            };

            this.submitForm2 = function(force){
                $ctrl.saveSingleForm(force, $scope.form2Struct, {name: "csv2DbParam", model: $scope.curCsv2DbParam}, [$scope.form3Struct.name, $scope.form4Struct.name, $scope.form5Struct.name], function(response){
                    $scope.form3Struct = {name:"", model:{}};
                    $scope.form3Tmp = [];
                });
            };

            this.loadForm3 = function(name, model){
                $ctrl.loadMultiForm(model, name, $scope.form3Struct, $scope.form2Struct, [$scope.form4Struct.name], function(){
                    $scope.form4Struct = {name: "", model: {}};
                });
            };

            this.newItem3 = function(name, namingField){
                $ctrl.newMultiItem(name, $scope.form3Struct, $scope.form2Struct, [$scope.form4Struct.name], namingField, function(){
                    $scope.form4Struct = {name: "", model: {}};
                });
            };

            this.submitForm3 = function(){
                $ctrl.submitMultiForm($scope.form3Struct, $scope.form2Struct);
            };

            this.deleteForm3 = function(){
                $ctrl.deleteMultiForm($scope.form3Struct, $scope.form2Struct);
            };

            this.canReloadForm4 = function(model, name, changed){
                return $ctrl.canReloadSingleForm(model, name, changed, $scope.form4Struct, $scope.form2Struct);
            };

            this.loadForm4 = function(model, name, changed){
                $ctrl.loadSingleForm(model, name, changed, $scope.form4Struct, $scope.form2Struct, function(){
                    $scope.form3Struct = {name: "", model: {}};
                });
            };

            this.submitForm4 = function(force){
                $ctrl.saveSingleForm(force, $scope.form4Struct, $scope.form2Struct, ["csv2DbParam"]);
            };

            this.loadForm5 = function(name, model){
                $ctrl.loadMultiForm(model, name, $scope.form5Struct, {name: "csv2DbParam", model: $scope.curCsv2DbParam}, [$scope.form2Struct.name], function(){
                    $scope.form2Struct = {name: "", model: {}};
                    $scope.form3Struct = {name: "", model: {}};
                    
                    if(name == "ignoredRow"){
                        $scope.hasChildren = $ctrl.hasChildren(model.id);
                    }
                });
            };

            this.newItem5 = function(name, namingField){
                $ctrl.newMultiItem(name, $scope.form5Struct, {name: "csv2DbParam", model: $scope.curCsv2DbParam}, [$scope.form2Struct.name], namingField, function(){
                    $scope.form2Struct = {name: "", model: {}};
                    $scope.form3Struct = {name: "", model: {}};
                });
            };

            this.submitForm5 = function(){
                $ctrl.submitMultiForm($scope.form5Struct, {name: "csv2DbParam", model: $scope.curCsv2DbParam});
            };

            this.deleteForm5 = function(){
                $ctrl.deleteMultiForm($scope.form5Struct, {name: "csv2DbParam", model: $scope.curCsv2DbParam});
            };
        }
    ]
);