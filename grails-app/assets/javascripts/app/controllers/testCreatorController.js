//= require app/app.js
//= require_self

angular.module('diffapp').controller('testCreatorController', ['$scope', '$controller', '$document', 'resUtil', function($scope, $controller, $document, resUtil){
    var $ctrl = this;

    $scope.codemirrorOptions = {
        //lineWrapping : true,
        lineNumbers: true,
        theme:'eclipse',
        mode: 'text/x-mysql',
        autoRefresh: true,
        delay: 250
    };

    angular.extend(this, $controller('baseFormController', {$scope: $scope}));

    $ctrl.incEntities = {
        test:[
            {
                name: "webTest1",
                type: "single",
                source: "form2"
            },
            {
                name: "webTest2",
                type: "single",
                source: "form2"
            },
            {
                name: "arrayComboGroup",
                type: "single",
                source: "form2"
            },
            {
                name: "sqlComboGroup",
                type: "single",
                source: "form2"
            },
            {
                name: "sqlComboParam",
                type: "multiple",
                source: "form4"
            },
            {
                name: "xml2XmlParam",
                type: "single",
                source: "form2"
            },
            {
                name: "xml2DbParam",
                type: "single",
                source: "form2"
            }
        ],
        arrayComboGroup:[
            {
                name: "arrayComboParam",
                type: "multiple",
                source: "form3"
            }
        ],
        xml2DbParam:[
            {
                name: "includedMapping",
                type: "multiple",
                source: "form3"
            }
        ],
        xml2XmlParam:[
            {
                name: "ignoredElement",
                type: "multiple",
                source: "form3"
            }
        ]
    };

    $ctrl.entMapping = {
        webTest1: "webTest",
        webTest2: "webTest"
    };

    this.init = function(){
        $ctrl.getEntList('test', false, {new: true, mType: "nameId"});

        $scope.curTest = {};
        $scope.adErrors = "";

        $scope.form2Struct = {name: "", model: {}};
        $scope.form3Struct = {name: "", model: {}};
        $scope.form4Struct = {name: "", model: {}};
        $scope.form5Struct = {name: "", model: {}};

        $scope.form3Tmp = [];
        $scope.form5Tmp = [];
        
        $document.ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    };

    this.loadTest = function(test){
        if(test.id != $scope.curTest.id){
            if($ctrl.formSanityCheck([$scope.form5Struct.name, $scope.form4Struct.name, $scope.form3Struct.name, $scope.form2Struct.name, 'test'])){
                if(test.id > 0){
                    resUtil["test"].get({id: test.id, mType: "deep"}, function(response){
                        $scope.curTest = response;
                        angular.forEach($ctrl.incEntities.test, function(form){
                            $scope[form.name + 'Tmp'] = angular.copy($scope.curTest[form.name]);
                        });
                    });
                }else{
                    $scope.curTest = angular.copy(test);
                    angular.forEach($ctrl.incEntities.test, function(form){
                        $scope[form.name + "Tmp"] = {};
                    });
                }
                $scope.form2Struct = {name: "", model: {}};
                $scope.form3Struct = {name: "", model: {}};
                $scope.form4Struct = {name: "", model: {}};
                $scope.form5Struct = {name: "", model: {}};
                angular.forEach($ctrl.incEntities.test, function(form){
                    $ctrl.getEntList(form.name, false, {new: form.source != null, mType: "nameId"});
                });
            }
        }
    };

    this.submitTest = function(){
        $ctrl.saveSingleForm(false, {name: "test", model: $scope.curTest}, null, [$scope.form2Struct.name, $scope.form3Struct.name, $scope.form4Struct.name, $scope.form5Struct.name], function(response){
            $scope.curTest = angular.copy(response);
        });
    };

    this.canReloadForm2 = function(model, name, changed){
        return $ctrl.canReloadSingleForm(model, name, changed, $scope.form2Struct, {model: $scope.curTest, name: "test"}, [$scope.form5Struct.name]);
    };

    this.loadForm2 = function(model, name, changed){
        $ctrl.loadSingleForm(model, name, changed, $scope.form2Struct, {name: "test", model: $scope.curTest}, function(){
            $scope.form3Struct = {name: "", model: {}};
            $scope.form4Struct = {name: "", model: {}};
            $scope.form5Struct = {name: "", model: {}};
            $scope.form5Tmp[0] = {};
        });
    };

    this.submitForm2 = function(force){
        $ctrl.saveSingleForm(force, $scope.form2Struct, {name: "test", model: $scope.curTest}, [$scope.form3Struct.name, $scope.form4Struct.name, $scope.form5Struct.name], function(response){
            $scope.form3Struct = {name:"", model:{}};
            $scope.form3Tmp = [];
        });
    };

    this.loadForm3 = function(name, model){
        $ctrl.loadMultiForm(model, name, $scope.form3Struct, $scope.form2Struct, [$scope.form4Struct.name], function(){
            $scope.form4Struct = {name: "", model: {}};
        });
    };

    this.newItem3 = function(name, namingField){
        $ctrl.newMultiItem(name, $scope.form3Struct, $scope.form2Struct, [$scope.form4Struct.name], namingField, function(){
            $scope.form4Struct = {name: "", model: {}};
        });
    };

    this.submitForm3 = function(){
        $ctrl.submitMultiForm($scope.form3Struct, $scope.form2Struct);
    };

    this.deleteForm3 = function(){
        $ctrl.deleteMultiForm($scope.form3Struct, $scope.form2Struct);
    };

    this.canReloadForm4 = function(model, name, changed){
        return $ctrl.canReloadSingleForm(model, name, changed, $scope.form4Struct, $scope.form2Struct);
    };

    this.loadForm4 = function(model, name, changed){
        $ctrl.loadSingleForm(model, name, changed, $scope.form4Struct, $scope.form2Struct, function(){
            $scope.form3Struct = {name: "", model: {}};
        });
    };

    this.submitForm4 = function(force){
        $ctrl.saveSingleForm(force, $scope.form4Struct, $scope.form2Struct, ["test"]);
    };

    this.loadForm5 = function(name, model){
        $ctrl.loadMultiForm(model, name, $scope.form5Struct, {name: "test", model: $scope.curTest}, [$scope.form2Struct.name], function(){
            $scope.form2Struct = {name: "", model: {}};
            $scope.form3Struct = {name: "", model: {}};
        });
    };

    this.newItem5 = function(name, namingField){
        $ctrl.newMultiItem(name, $scope.form5Struct, {name: "test", model: $scope.curTest}, [$scope.form2Struct.name], namingField, function(){
            $scope.form2Struct = {name: "", model: {}};
            $scope.form3Struct = {name: "", model: {}};
        });
    };

    this.submitForm5 = function(){
        $ctrl.submitMultiForm($scope.form5Struct, {name: "test", model: $scope.curTest});
    };

    this.deleteForm5 = function(){
        $ctrl.deleteMultiForm($scope.form5Struct, {name: "test", model: $scope.curTest});
    };

    this.init();
}]);