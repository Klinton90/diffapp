//= require app/app.js
//= require_self

angular.module('diffapp').controller('modificatorCreatorController',
    ['$scope', '$http', "$controller", "$timeout", "resUtil",
        function($scope, $http, $controller, $timeout, resUtil){
            angular.extend(this, $controller("baseFormController", {$scope: $scope}));
            var $ctrl = this;

            $scope.statuses = {
                OK : "Match!",
                WARN: "Differences has been found. Please review table below.",
                ERROR: "",
                ERROR2: "Oops! Something went wrong.",
                INFO: "Press 'Start' Button to Launch testing.",
                INFO2: "In progress."
            };
            
            this.init = function(){
                $ctrl.getEntList("modificator", false, {new: true, mType: "nameId"});
                $scope.status = "INFO";
                $scope.curModificator = {};
                $scope.testParams = {
                    input: "myValue",
                    key: "myKey",
                    obj: ''
                };

                $timeout(function(){
                    $scope["form_modificator"].$setPristine();
                });
                
                $scope.codemirrorOptions = {
                    lineNumbers: true,
                    theme:'eclipse',
                    mode: 'text/x-groovy',
                    autoRefresh: true,
                    delay: 250,
                    onLoad: function(_cm){
                        /*_cm.on("beforeChange", function(cm,change){
                            var ll = cm.lastLine();
                            if ([0, ll-2, ll-1, ll].indexOf(change.from.line) > -1) {
                                change.cancel();
                            }
                        });*/
                        $ctrl.cm = _cm;
                    }
                };

                $scope.codemirrorOptions2 = {
                    lineNumbers: true,
                    theme: 'eclipse',
                    mode: 'text/javascript',
                    autoRefresh: true,
                    delay: 250,
                    onLoad: function(_cm){
                        _cm.setValue('{\r\n    "myKey": "myValue",\r\n    "otherKey": "otherValue"\r\n}');
                    }
                };
            };
            
            this.tryTest = function(){
                $scope.status = "INFO2";
                $scope.curModificator.code = $ctrl.cm.getValue();
                $http({
                    method: 'post',
                    url: '/modificatorCreator/tryTest',
                    data: {
                        mod: $scope.curModificator,
                        testParams: $scope.testParams
                    }
                }).then(function(response){
                    console.log(response);
                    $scope.statuses["OK"] = "RESULT: '" + response.data.result.modified + "'";
                    $scope.status = "OK";
                }, function(response){
                    $scope.statuses["ERROR"] = response.data.errors;
                    $scope.status = "ERROR";
                });
            };
            
            this.loadModificator = function(modificator){
                if($scope.curModificator == null || modificator.id != $scope.curModificator.id){
                    $scope.statusXml = 'INFO';

                    if($ctrl.formSanityCheck(["modificator"])){
                        resUtil["modificator"].get({id: (modificator.id > 0 ? modificator.id : 0), mType: "deep"}, function(response){
                            $scope.curModificator = response;
                            /*$ctrl.cm.replaceRange(
                             response.code,
                             {line: 1, ch: 0},
                             {line: $ctrl.cm.lastLine() - 3}
                             );*/
                            $ctrl.markReadOnly(false);
                            $ctrl.cm.setValue(response.code);
                            $timeout(function(){
                                $ctrl.cm.refresh();
                            });
                            $ctrl.cm.setCursor({line: $ctrl.cm.lastLine() - 3});
                            $ctrl.markReadOnly(true);
                            $ctrl.cm.focus();

                            $timeout(function(){
                                $scope["form_modificator"].$setPristine();
                            });
                        });
                    }
                }
            };
            
            this.saveModificator = function(copy){
                if(copy){
                    $scope.curModificator.id = -1;
                }

                $scope.curModificator.code = $ctrl.cm.getValue();
                
                $ctrl.saveSingleForm(false, {name: "modificator", model: $scope.curModificator}, null, [], function(response){
                    $scope.curModificator = angular.copy(response);
                });
            };
            
            this.markReadOnly = function(val){
                var ll = $ctrl.cm.lastLine();
                
                $ctrl.cm.markText(
                    { line: 0, ch: 0 },
                    { line: 1, ch: 0 },
                    { readOnly: val, css: "background-color: darkgrey" }
                );
                $ctrl.cm.markText(
                    { line: ll-2, ch: 0 },
                    { line: ll },
                    { readOnly: val, css: "background-color: darkgrey" }
                );
            };
            
            this.init();
        }
    ]
);