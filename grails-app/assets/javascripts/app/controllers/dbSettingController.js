//= require angular.min.js
//= require app/app.js

angular.module('diffapp').controller('dbSettingController', ['$scope', '$http', 'dbSetting', function($scope, $http, dbSetting){
    $scope.testVar = 'myTest';
    var dbSettings = dbSetting.query(function(){
        $scope.dbSettings = dbSettings;
    });

    var dbSetting1 = dbSetting.get({dbSettingId: 1}, function(){
        $scope.dbSetting1 = dbSetting1;
    });
}]);