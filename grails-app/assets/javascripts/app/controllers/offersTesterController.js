//= require app/app.js
//= require_self

angular.module('diffapp').controller('offersTesterController',
    ['$scope', '$http', '$timeout', '$controller',
        function($scope, $http, $timeout, $controller){
            angular.extend(this, $controller("baseFormController", {$scope: $scope}));
            var $ctrl = this;
            $ctrl.offerPromise = null;

            $scope.statuses = {
                OK : "Match!",
                WARN: "Differences has been found. Please review table below.",
                ERROR: "Oops! Something went wrong.",
                INFO: "Press Button to Launch testing.",
                INFO2: "In progress."
            };
            
            this.init = function(){
                $scope.statusOffers = "INFO";
                $scope.adErrors = "";
                $scope.htmlError = "";
                $scope.isTestingOffers = false;
                $scope.offersProgress = 0;
                $scope.offersData = {};
                $scope.isDownloading = false;
                $scope.downloadedFiles = [];
                /*$scope.offerForm = {
                    host: "http://prods.nissanusa.com/nissan-tools-ws/rest/ws/",
                    type: "MODEL",
                    model: "Q5,X6",
                    acode: "XGC60NIS111A0",
                    dma: 505,
                    year: 2015,
                    all: "",
                    
                    discontinued: '',
                    cmpType: 't2'
                };*/
                
                //$scope.feedType = "nissanAll";
            };
            
            this.testOffers = function(){
                $scope.offersProgress = 0;
                $scope.adErrors = "";
                $scope.htmlError = "";
                $scope.offersData = {};
                $scope.isTestingOffers = true;
                $scope.statusOffers = "INFO2";
                $("#diffoutput").empty();
                $("#originaloutput").empty();
                
                $http({
                    method: 'post',
                    url: '/offersTester/testOffers',
                    data: $scope.offerForm
                }).then(function(response){
                    $ctrl.processOffersStatus(response);
                }, function(response){
                    $scope.statusOffers = "ERROR";
                    $ctrl.errorHandler(response);
                });
            };
            
            this.isOffersRunning = function(){
                if($ctrl.offerPromise != null){
                    $timeout.cancel($ctrl.offerPromise);
                }
                $http({
                    method: 'get',
                    url: '/offersTester/isOffersRunning'
                }).then(function(response){
                    $ctrl.processOffersStatus(response);
                }, function(response){
                    $scope.statusOffers = "ERROR";
                    $ctrl.errorHandler(response);
                });
            };
            
            this.processOffersStatus = function(response){
                if(response.data.errors == null || response.data.errors.length == 0){
                    $scope.isTestingOffers = response.data.result.isOffersRunning;
                    if($scope.isTestingOffers){
                        var cur = response.data.result.offersProgress.cur - (response.data.result.offersProgress.cur > 0 ? 0.5 : 0);
                        $scope.offersProgress = Math.round(cur*100/(response.data.result.offersProgress.max ? response.data.result.offersProgress.max : 1));
                    }else{
                        $ctrl.getOffersResult();
                    }
                }else{
                    $scope.statusOffers = "ERROR";
                    $scope.isTestingOffers = false;
                    $ctrl.errorHandler(response);
                }

                if($scope.isTestingOffers){
                    $ctrl.offerPromise = $timeout(function(){
                        $ctrl.isOffersRunning();
                    }, 5000);
                }
            };
            
            this.getOffersResult = function(){
                $http({
                    method: 'get',
                    url: '/offersTester/getOffersResult'
                }).then(function(response){
                    var offersData = response.data.result;
                    
                    if(offersData!= null && offersData.length > 0 && 0 < $.grep(offersData, function(e){
                        return e.feedList || e.wsList;
                    }).length){
                        $scope.statusOffers = "WARN";
                    }else{
                        $scope.statusOffers = "OK";
                    }
                    
                    if($scope.type == 'MODEL' && $scope.offerForm.cmpType == 't1'){
                        $scope.offersData = offersData;
                    }else{
                        var diffOutput = $("#diffoutput").empty();
                        var originalOutput = $("#originaloutput").empty();

                        angular.forEach(offersData, function(offer){
                            originalOutput.append("<h4>" + offer.paramSet + "</h4>");
                            originalOutput.append($ctrl.getDiffOutput(offer.feedList_original, offer.wsList_original, "Feed List", "WS List"));
                            
                            if(offer.feedList || offer.wsList){
                                diffOutput.append("<h4>" + offer.paramSet + "</h4>");
                                diffOutput.append($ctrl.getDiffOutput(offer.feedList, offer.wsList, "Feed List", "WS List"));
                            }
                        });
                    }
                }, function(response){
                    $scope.statusOffers = "ERROR";
                    $ctrl.errorHandler(response);
                });
            };
            
            this.downloadFiles = function(){
                $scope.adErrors = "";
                $scope.htmlError = "";
                $scope.isDownloading = true;
                $http({
                    method: 'get',
                    url: '/offersTester/downloadFiles'
                }).then(function(response){
                    $scope.isDownloading = false;
                    $scope.downloadedFiles = response.data.result.files;
                }, function(response){
                    $scope.isDownloading = false;
                    $ctrl.errorHandler(response);
                });
            };

            this.errorHandler = function(response){
                if(response.data.errors != null){
                    $scope.adErrors = response.data.errors;
                }else{
                    $scope.htmlError = response;
                }
            };
            
            this.uploadFiles = function(){
                $scope.adErrors = "";
                var feedFile = $("#feedFile")[0].files[0];
                if(feedFile != null){
                    var formData = new FormData();
                    formData.append('data', feedFile);
                    //formData.append('type', $scope.feedType);

                    $http({
                        url: "/offersTester/uploadFile",
                        method: "POST",
                        data: formData,
                        transformRequest: angular.identity,
                        headers: {
                            'enctype': 'multipart/form-data',
                            'Content-Type': undefined
                        }
                    }).then(function(response){
                        $scope.adErrors = response.data.status;
                    }, function(response){
                        $ctrl.errorHandler(response);
                    });
                }else{
                    $scope.adErrors = 'Please select file to upload!';
                }
            };
        }
    ]
);
