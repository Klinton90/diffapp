//= require angular.min.js
//= require angular-resource.min.js
//= require angular-sanitize.min.js
//= require ui-codemirror.js
//= require_self

(function(){
    angular.module('diffapp', ['ngResource', 'ngSanitize', 'ui.codemirror'])
        .factory("resUtil", ['$resource', function($resource){
            var resUtil = {
                defaultConfig : {id: "@id"},

                extraMethods: {
                    update : {
                        method: "PUT"
                    }
                },

                add : function (config) {
                    var params,
                        url;

                    if (angular.isString(config)) {
                        config = {
                            resource: config,
                            url: "/" + config
                        };
                    }

                    if (!config.unnatural) {
                        var orig = angular.copy(resUtil.defaultConfig);
                        params = angular.extend(orig, config.params);
                        url = config.url + "s/:id";
                    } else {
                        params = config.params;
                        url = config.url;
                    }

                    var methods = config.methods || resUtil.extraMethods;

                    resUtil[config.resource] = $resource(url, params, methods);
                }
            };

            return resUtil;
        }])
        .factory('PathInterceptor', ['$location',function($location) {
            var path = {
                request: function(config) {
                    config.url = CONTEXTPATH+config.url;
                    return config;
                },
                response: function(response) {
                    return response;
                }
            };
            return path;
        }])
        .run(function(resUtil){
            resUtil.add('test');
            resUtil.add('testCase');
            resUtil.add('testCaseRun');

            resUtil.add('webTest');
            resUtil.add('webEnvironment');
            resUtil.add('dbSetting');
            resUtil.add('arrayComboGroup');
            resUtil.add('sqlComboGroup');
            resUtil.add('xml2XmlParam');
            resUtil.add('xml2DbParam');

            resUtil.add('sqlComboParam');
            resUtil.add('arrayComboParam');
            resUtil.add('includedMapping');
            resUtil.add('ignoredElement');

            resUtil.add('dbDriver');
            resUtil.add('simpleComparisonSetting');

            resUtil.add('csv2DbParam');
            resUtil.add('csvColumn');
            resUtil.add('csvSetting');
            resUtil.add('ignoredRow');
            resUtil.add('modificator');

            resUtil.add('db2DbParam');

            resUtil.add({
                resource: "demo",
                url: "/simpleComparison",
                params: {
                    id: "@id"
                },
                methods: {
                    query:{
                        url: "/simpleComparison/list",
                        isArray:true
                    },
                    get:{
                        url: "/simpleComparison/show/:id"
                    }
                }
            });
        });
    angular.module('diffapp').config(['$httpProvider', function($httpProvider) {
        $httpProvider.interceptors.push('PathInterceptor');
    }]);
})();

//= require app/directives/ignoreDirty.js
//= require app/directives/selectSearch.js
//= require app/directives/isChangedLabel.js
//= require app/directives/elemReady.js
//= require app/directives/lastReady.js
//= require app/filters/findByNameOrId.js
//= require app/filters/hideByBool.js
//= require app/controllers/baseFormController.js
//= require codemirror.js
//= require codemirror-autorefresh.js
//= require codemirror-sql.js
//= require codemirror-groovy.js