grails.gorm.default.constraints = {
    isFakeDomain(
        validator: {val, obj, errors->
            if(obj.id == 0){
                return errors.rejectValue(propertyName, 'required')
            }
        }
    )
}
