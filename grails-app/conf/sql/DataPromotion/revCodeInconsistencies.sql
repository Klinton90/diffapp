SELECT e2.`Info1` AS "modelCode", v51.`Acode` AS "Acode", v51.`RevCode` AS "enRevCode", v52.`RevCode` AS "frRevCode"
FROM VT05_Trim AS v51
  LEFT JOIN VT05_Trim AS v52 ON v51.`Acode` = v52.`Acode` AND v52.`LngCode` = 'FR'
  LEFT JOIN EI02_ExtraVehicleInfo AS e2 ON e2.`Acode` = v51.`Acode` AND e2.`LngCode` = v51.`LngCode`
WHERE (v51.`RevCode` != v52.`RevCode` OR v51.`Acode` IS NULL OR v52.`Acode` IS NULL)
      AND v51.`LngCode` = 'EN'
      AND v51.Acode NOT IN ('CAC60NIT161C0', 'CAC60NIT161E0')
      AND v51.`Acode` NOT LIKE '%NIV%'

UNION

SELECT e2.`Info1` AS "modelCode", v51.`Acode` AS "Acode", v51.`RevCode` AS "enRevCode", v52.`RevCode` AS "frRevCode"
FROM VT05_Trim AS v51
  LEFT JOIN VT05_Trim AS v52 ON v51.`Acode` = v52.`Acode` AND v52.`LngCode` = 'EN'
  LEFT JOIN EI02_ExtraVehicleInfo AS e2 ON e2.`Acode` = v51.`Acode` AND e2.`LngCode` = v51.`LngCode`
WHERE (v51.`RevCode` != v52.`RevCode` OR v51.`Acode` IS NULL OR v52.`Acode` IS NULL)
      AND v51.`LngCode` = 'FR'
      AND v51.Acode NOT IN ('CAC60NIT161C0', 'CAC60NIT161E0')
      AND v51.`Acode` NOT LIKE '%NIV%';