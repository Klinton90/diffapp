SELECT v5.`Acode` AS "Acode", v5.LngCode AS "lngCode", e2.`Info1` AS "modelCode"
FROM VT05_Trim AS v5
LEFT JOIN EI02_ExtraVehicleInfo AS e2 ON e2.`Acode` = v5.`Acode` AND e2.`LngCode` = v5.`LngCode`
WHERE e2.`Acode` IS NULL
AND v5.`Acode` NOT LIKE '%NIV%'
AND v5.`Acode` NOT LIKE '%C20%'
ORDER BY v5.`Acode` DESC;