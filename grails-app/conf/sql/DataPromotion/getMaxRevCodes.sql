SELECT DISTINCT SUBSTRING(v5.`Acode`, 1, 11) AS PreAcode, MAX(v5.`RevCode`) AS RevCode, '0' AS UnlockedRevCode, v4.ModelDesc AS ModelDesc
FROM VT05_Trim AS v5
LEFT JOIN VT04_Model AS v4 ON v4.LngCode = v5.LngCode AND v4.ModelID = v5.ModelID
WHERE CASE WHEN :year IS NULL THEN 1 = 1 ELSE v5.`YearCode` = :year END
GROUP BY PreAcode;