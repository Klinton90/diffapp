SELECT v17.*
FROM VT17_TrimOptions AS v17
  LEFT JOIN SPECIAL_PACKAGES AS sp ON sp.`acode` = v17.`Acode` AND sp.`OptCode` = v17.`OptCode`
WHERE v17.`LngCode` = 'EN'
  AND SUBSTR(v17.`OptCode`, 4) = '0'
  AND v17.`ECC` = '0006'
  AND sp.`acode` IS NULL
  AND SUBSTR(v17.`Flags`, 27, 1) != '1'
ORDER BY v17.`Acode` DESC