SELECT sp.*
FROM SPECIAL_PACKAGES AS sp
  LEFT JOIN VT17_TrimOptions AS v17
    ON sp.`acode` = v17.`Acode`
       AND sp.`OptCode` = v17.`OptCode`
       AND v17.`LngCode` = 'EN'
       AND SUBSTR(v17.`OptCode`, 4) = '0'
       AND v17.`ECC` = '0006'
       AND SUBSTR(v17.`Flags`, 27, 1) != '1'
WHERE v17.`acode` IS NULL
ORDER BY sp.`Acode` DESC