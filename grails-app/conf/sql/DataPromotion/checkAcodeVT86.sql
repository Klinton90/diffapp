SELECT v5.`Acode` AS "Acode", v5.`LngCode` AS "lngCode", e2.`Info1` AS "modelCode"
FROM VT05_Trim AS v5
LEFT JOIN VT86_AcodeSegments AS v86 ON v5.`Acode` = v86.`Acode` AND v5.`LngCode` = v86.`LngCode`
LEFT JOIN EI02_ExtraVehicleInfo AS e2 ON e2.`Acode` = v5.`Acode` AND e2.`LngCode` = v5.`LngCode`
WHERE v86.`Acode` IS NULL;