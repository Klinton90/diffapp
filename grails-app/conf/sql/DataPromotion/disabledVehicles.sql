SELECT DISTINCT e2.`Info1` AS "modelCode", v5.Acode AS "Acode", v5.RevCode AS "revCode", v5.LngCode AS "lngCode", v5.DISPLAY_VEHICLE AS "isEnabled", IF(v5.DelayedRelease IS NULL, 'N', 'Y') AS "isDelayed", v5.`MSRP` AS "msrp"
FROM VT05_Trim AS v5
LEFT JOIN EI02_ExtraVehicleInfo AS e2 ON v5.`Acode` = e2.`Acode` AND v5.`LngCode` = e2.`LngCode`
WHERE DISPLAY_VEHICLE = 'N'
AND v5.Acode NOT LIKE 'CAC40NIV1%'
AND v5.Acode NOT LIKE 'CAC50NIV1%'
AND v5.Acode NOT LIKE 'CAC60NIV1%'
AND v5.`Acode` NOT IN (:in_excludedAcodes)
ORDER BY v5.`Acode`, v5.`LngCode` DESC;