SELECT e2.Acode
FROM EI02_ExtraVehicleInfo AS e2
GROUP BY Acode
HAVING COUNT(*) = 1;