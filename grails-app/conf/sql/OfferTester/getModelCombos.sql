SELECT e2.INFO1 AS "model", SUBSTR(e2.Acode, 6, 2) AS "division", v8.YEARDESC AS "year", fdpa.DMA AS "dma", :all AS "all", '' AS "discontinued"
FROM EI02_EXTRAVEHICLEINFO e2
  LEFT JOIN FCT_DIME_PROMO_DMA fdpa ON 1 = CASE WHEN :in_dmas = 'ALL' THEN 1 ELSE (CASE WHEN fdpa.DMA IN (:in_dmas) THEN 1 ELSE 0 END) END
  LEFT JOIN VT08_YEAR v8 ON e2.ACODE LIKE CONCAT('%', CONCAT(v8.YEARCODE, '%'))
WHERE 1 = CASE WHEN :in_models = 'ALL' THEN 1 ELSE (CASE WHEN e2.INFO1 IN (:in_models) THEN 1 ELSE 0 END) END
      AND v8.YEARDESC IN (:in_years)