<g:applyLayout name="links">
<head>
    <title><g:layoutTitle/></title>
    <g:layoutHead/>
</head>
<body>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="${createLink(controller: 'home', action: 'index')}" title="Diff App">
                    <asset:image src="logo.png"/>
                </a>
            </div>
            <div id="navbar" class="collapse navbar-collapse navbar-right">
                <ul class="nav navbar-nav">
                    <li>
                        <a ${controllerName == "home" && actionName == "index" ? 'class=active' : ""} href="${createLink(controller: 'home', action: 'index')}">Home</a>
                    </li>
                    <li class="dropdown">
                        <a href="${createLink(controller: 'home', action: 'nna')}" class="${controllerName == "offersTester" || (controllerName == "home" && actionName == "nna") ? 'active' : ''}">NNA <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li ${controllerName == "home" && actionName == "nna" ? 'class=active' : ""}><a href="${createLink(controller: 'home', action: 'nna')}">NNA Info</a></li>
                            <li ${controllerName == "offersTester" ? 'class=active' : ""}><a href="${createLink(controller: 'offersTester', action: 'index')}">Offers Tester</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="${createLink(controller: 'home', action: 'nci')}" class="${controllerName == "dataPromotion" || (controllerName == "home" && actionName == "nci") ? 'active' : ''}">NCI <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li ${controllerName == "home" && actionName == "nci" ? 'class=active' : ""}><a href="${createLink(controller: 'home', action: 'nci')}">NCI Info</a></li>
                            <li ${controllerName == "dataPromotion" ? 'class=active' : ""}><a href="${createLink(controller: 'dataPromotion', action: 'index')}">Data Promotion Tester</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a ${["simpleComparison", "db2DbComparison", "csvTest", "modificatorCreator"].contains(controllerName) ? 'class=active' : ""} href="${createLink(controller: 'simpleComparison')}">Simple Comparison <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li ${controllerName == "simpleComparison" && actionName == "index" ? 'class=active' : ""}><a href="${createLink(controller: 'simpleComparison')}">WS Comparison</a></li>
                            <li ${controllerName == "simpleComparison" && actionName == "demo" ? 'class=active' : ""}><a href="${createLink(controller: 'simpleComparison', action:"demo")}">XML Comparison</a></li>
                            <li ${controllerName == "csvTest" ? 'class=active' : ""}><a href="${createLink(controller: 'csvTest')}">Csv Test</a></li>
                            <li ${controllerName == "db2DbComparison" ? 'class=active' : ""}><a ${controllerName == "db2DbComparison" ? 'class=active' : ""} href="${createLink(controller: 'db2DbComparison', action: 'index')}">Db 2 Db comparison</a></li>
                            <li role="separator" class="divider"></li>
                            <li ${controllerName == "modificatorCreator" ? 'class=active' : ""}><a href="${createLink(controller: 'modificatorCreator')}">Modificator Creator</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="${createLink(controller: 'comboTest')}" class="${controllerName == "comboTest" ? 'active' : ''}">Combo Tests <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li ${controllerName == "comboTest" && actionName == "index" ? 'class=active' : ""}><a href="${createLink(controller: 'comboTest')}">Test Case Runner</a></li>
                            <li ${controllerName == "comboTest" && actionName == "testCreator" ? 'class=active' : ""}><a href="${createLink(controller: 'comboTest', action:"testCreator")}">Test Creator</a></li>
                            <li ${controllerName == "comboTest" && actionName == "testCaseCreator" ? 'class=active' : ""}><a href="${createLink(controller: 'comboTest', action: 'testCaseCreator')}">Test Case Creator</a></li>
                            <li role="separator" class="divider"></li>
                            <li ${controllerName == "comboTest" && actionName == "dbMappingCreator" ? 'class=active' : ""}><a href="${createLink(controller: 'comboTest', action: 'dbMappingCreator')}">DB Mapping Creator</a></li>
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="${controllerName == "home" && (actionName == "status" || actionName == "contact") ? 'active' : ''}">About <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li ${controllerName == "home" && actionName == "status" ? 'class=active' : ""}><a href="${createLink(controller: 'home', action:"status")}">Status</a></li>
                            <li ${controllerName == "home" && actionName == "contact" ? 'class=active' : ""}><a href="${createLink(controller: 'home', action: 'contact')}">Contact</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </nav>
    <div class="container-fluid">
        <div style="min-height: 80vh;">
            <g:layoutBody/>
        </div>

        <footer class="footer">
            <hr>
            <p>(R) 2015-2016 Alex Tymofieiev</p>
        </footer>
    </div>
</body>
</g:applyLayout>