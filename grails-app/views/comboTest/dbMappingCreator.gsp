<!DOCTYPE html>
<html xmlns:asset="http://www.w3.org/1999/XSL/Transform">
<head>
    <meta name="layout" content="menu"/>
    <title>${title}</title>
    <asset:javascript src="app/controllers/dbMappingCreator.js"/>
</head>
<body>
<div ng-app="diffapp">
    <div ng-controller="dbMappingCreator as dmc">
        <div class="container">
            <h2>XML 2 DB mapping creator</h2>
            <hr>
            <div class="row main">
                <span id="adErrors">{{adErrors}}</span>
                <div ng-show="htmlError != null && htmlError.length > 0" style="padding-bottom: 15px;">
                    <span class="text-danger">Something went wrong. Below you can see error.</span>
                    <br/>
                    <button class="btn btn-warning" type="button" data-toggle="collapse" data-target="#htmlError" aria-expanded="false" aria-controls="html_error">Show error</button>
                    <div id="htmlError" class="collapse">
                        <div class="well">
                            <span>{{htmlError}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row main">
                <div class="col-md-6">
                    <g:btpTextArea id="source" label="Put you XML here" ng-model="xmlSource" rows="15"/>
                </div>
                <div class="col-md-6">
                    <g:btpTextArea id="result" label="Result mapping" ng-model="dbResult" readonly="" rows="15"/>
                </div>
                <button type="button" class="btn btn-default" ng-click="dmc.createMapping()">Create Mapping</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>