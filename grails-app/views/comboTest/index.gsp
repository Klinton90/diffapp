<!DOCTYPE html>
<html xmlns:asset="http://www.w3.org/1999/XSL/Transform">
<head>
    <meta name="layout" content="menu"/>
    <title>${title}</title>
    <asset:javascript src="app/controllers/testCaseRunnerController.js"/>
</head>
<body>
<div ng-app="diffapp">
    <div ng-controller="testCaseRunnerController as tcr" ng-init="defTestCaseId = ${params.id ?: 0}; tcr.init();">
        <div class="container">
            <h2>Test Case Runner</h2>
            <hr>
            <div class="row main">
                <span id="adErrors">{{adErrors}}</span>
                <div ng-show="htmlError != null && htmlError.length > 0" style="padding-bottom: 15px;">
                    <span class="text-danger">Something went wrong. Below you can see error.</span>
                    <br/>
                    <button class="btn btn-warning" type="button" data-toggle="collapse" data-target="#htmlError" aria-expanded="false" aria-controls="html_error">Show error</button>
                    <div id="htmlError" class="collapse">
                        <div class="well">
                            <span>{{htmlError}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row main">
                <div class="col-md-2">
                    <h4 class="simple-header">Select Test Case</h4>
                    <g:btpText id="testCaseFilter" label="Test Case Filter" ng-model="testCaseFilter" placeholder="Quick search..."/>
                    <div class="list-group searchable-list combo-testcase-list">
                        <button type="button" class="list-group-item text-nooverflow" ng-repeat="testCase in testCaseList | orderBy:'name' | findByNameOrId:testCaseFilter:curTestCase.id track by testCase.id"
                                ng-class="{active : testCase.id == curTestCase.id}" ng-click="tcr.loadTestCase(testCase.id)" data-toggle="tooltip" data-original-title="{{testCase.name}}" data-placement="left"
                                last-ready="tcr.initTooltip()">
                            {{testCase.name}}
                        </button>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="row" ng-show="curTestCase.id != null">
                        <h4 class="simple-header">Test Case Listing</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <g:btpText id="testCase_name" label="Test Case Name" readonly="" ng-model="curTestCase.name" class="input-sm"/>
                                <g:btpText id="testCase_dbSetting_name" label="DB Setting Name" readonly="" ng-model="curTestCase.dbSetting.name" class="input-sm"/>
                                <g:btpText id="testCase_dbSetting_name" label="DB Setting URL" readonly="" ng-model="curTestCase.dbSetting.url" class="input-sm"/>
                                <g:btpText id="testCase_dbSetting_name" label="DB Driver" readonly="" ng-model="curTestCase.dbSetting.dbDriver.name" class="input-sm"/>
                            </div>
                            <div class="col-md-4">
                                <g:btpSelectBox id="testCase_test" label="Included Tests" angularOptions="curTestCase.test" ng-model="testTmp" readonly="" multiple="true" rows="15" class="input-sm"/>
                            </div>
                            <div class="col-md-4">
                                <g:btpText id="testCase_webEnvironment1_name" label="WebEnvironment1 Name" readonly="" ng-model="curTestCase.webEnvironment1.name" class="input-sm"/>
                                <g:btpText id="testCase_webEnvironment1_url" label="WebEnvironment1 URL" readonly="" ng-model="curTestCase.webEnvironment1.url" class="input-sm"/>
                                <g:btpText id="testCase_webEnvironment2_name" label="WebEnvironment2 Name" readonly="" ng-model="curTestCase.webEnvironment2.name" class="input-sm"/>
                                <g:btpText id="testCase_webEnvironment2_url" label="WebEnvironment2 URL" readonly="" ng-model="curTestCase.webEnvironment2.url" class="input-sm"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <g:btpTextArea id="testCase_description" lable="Test Case Description" readonly="" ng-model="curTestCase.description" class="input-sm"/>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-4">
                                <label>Actions</label>
                                <br/>
                                <button class="btn btn-default" type="button" ng-click="tcr.runTestCase()" ng-show="!isTestCaseRunning">Run this Test Case</button>
                                <div ng-show="isTestCaseRunning">
                                    <button id="checkTestCaseStatus" class="btn btn-default" ng-click="tcr.checkTestCaseStatus()" autocomplete="off">Is ready?</button>
                                    <button id="unlockTestCase" class="btn btn-default" ng-click="tcr.unlockTestCase()" autocomplete="off" ng-disabled="testCaseRunResult.status == 'CANCELING'">Unlock Test Case</button>
                                </div>
                            </div>
                            <div class="col-md-8" ng-show="testCaseRunList.length > 0">
                                <g:btpSelectBox id="testCaseRunList" angularOptions="testCaseRunList" ng-model="curTestCaseRun" optionsMapping="${[name: 'dateCreated']}"
                                                ng-change="tcr.loadOldTestCaseRun()" ng-show="testCaseRunList.length" label="Review Historical Test Case Run Data"/>
                            </div>
                        </div>
                        
                        <div ng-show="isTestCaseRunning">
                            <br/>
                            <label>TestCase progress</label>
                            <div class="progress">
                                <div class="progress-bar progress-bar-striped active" role="progressbar" style="width: {{testCaseProgress}}%">
                                    <span>{{testCaseProgress}}% Complete</span>
                                </div>
                            </div>
                            <div ng-show="testRunInProgress.id > 0">
                                <label>Test Progress for: <i>{{testRunInProgress.test.name}}</i></label>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-striped active" role="progressbar" style="width: {{testProgress}}%">
                                        <span>{{testProgress}}% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row main" ng-show="testCaseRunResult != null">
                <hr/>
                <div class="panel panel-warning" ng-show="testCaseRunResult.messages.length > 0">
                    <div class="panel-heading">Other errors occurred on TestCaseRun</div>
                    <div class="panel-body">
                        {{testCaseRunResult.messages}}
                    </div>
                </div>
                <div ng-show="testCaseRunResult.testRun != null && testCaseRunResult.testRun.length > 0">
                    <div class="col-md-2">
                        <h4 class="simple-header">Select Test</h4>
                        <g:btpText id="testFilter" label="Test Filter" ng-model="testFilter" placeholder="Quick search..."/>
                        <div class="list-group searchable-list medium-list">
                            <button type="button" class="list-group-item text-nooverflow" data-toggle="tooltip" data-original-title="{{test.name}}" data-placement="left"
                                    ng-repeat="testRun in testCaseRunResult.testRun | orderBy:'test.name' | findByNameOrId:testFilter:curTestRun.id:null:'test.name' track by testRun.test.id"
                                    ng-class="{true: 'list-group-item-success', false: 'list-group-item-warning'}[testRunStatuses[testRun.id].passed]" ng-click="tcr.loadTestRun(testRun)"
                            >
                                {{testRun.id == curTestRun.id ? '(C)' : ''}} {{testRun.test.name}}
                            </button>
                        </div>
                    </div>
                    <div class="col-md-10">
                        <div ng-show="curTestRun != null">
                            <h4 class="simple-header">Test Results for Test '{{curTestRun.test.name}}'</h4>
                            <g:btpTextArea id="test_description" label="Description" ng-model="curTestRun.test.description" readonly=""/>
                            <a href="https://spiratest.london.autodata.net/SpiraTest/{{curTestRun.test.spiraPrjId}}/TestCase/{{curTestRun.test.spiraTestId}}.aspx" target="_blank"
                               ng-show="curTestRun.test.spiraPrjId > 0 && curTestRun.test.spiraTestId > 0">
                                Open SpiraTest
                            </a>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" ng-model="showFailed"> Show failed Iterations only
                                </label>
                            </div>
                            <table class="table table-condensed table-striped">
                                <thead>
                                <tr>
                                    <th>Test Params</th>
                                    <th>Result</th>
                                    <th>Original1</th>
                                    <th>Original2</th>
                                    <th>File1</th>
                                    <th>File2</th>
                                    <th>Compare</th>
                                </tr>
                                </thead>
                                <tbody>
                                <!-- <tr ng-repeat="row in curTestRun.iterationRun | orderBy:'row.wsParams' | hideByBool:showFailed:'passed':false:true" ng-class="{true: 'success', false: 'danger'}[row.passed]"> -->
                                <tr ng-repeat="row in curTestRun.iterationRun | orderBy:'row.wsParams' | filter:{passed:!showFailed && passed}" ng-class="{true: 'success', false: 'danger'}[row.passed]">
                                    <td>{{row.wsParams}}</td>
                                    <td>{{row.passed ? "Passed" : "Failed"}}</td>
                                    <td><a href="/comboTest/fileAction?testCaseRunId={{testCaseRunResult.id}}&testRunId={{curTestRun.id}}&iterationRunId={{row.id}}&type=1&original=true" download="">Original1</a></td>
                                    <td><a href="/comboTest/fileAction?testCaseRunId={{testCaseRunResult.id}}&testRunId={{curTestRun.id}}&iterationRunId={{row.id}}&type=2&original=true" download="">Original2</a></td>
                                    <td><a ng-show="!row.passed" href="/comboTest/fileAction?testCaseRunId={{testCaseRunResult.id}}&testRunId={{curTestRun.id}}&iterationRunId={{row.id}}&type=1" download="">File1</a></td>
                                    <td><a ng-show="!row.passed" href="/comboTest/fileAction?testCaseRunId={{testCaseRunResult.id}}&testRunId={{curTestRun.id}}&iterationRunId={{row.id}}&type=2" download="">File2</a></td>
                                    <td><a href="#" class="btn btn-default btn-xs" ng-show="!row.passed" ng-click="tcr.compareResults(row)">Compare</a></td>
                                </tr>
                                </tbody>
                            </table>
                            <div id="diffoutput"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>