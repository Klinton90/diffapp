<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="menu"/>
    <title>${title}</title>
    <asset:javascript src="app/controllers/testCreatorController.js"/>
</head>
<body>
<div ng-app="diffapp">
    <div ng-controller="testCreatorController as tc">
        <h2>Test Creator</h2>
        <hr>
        <div class="row main">
            <span id="adErrors" class="text-danger">{{adErrors}}</span>
            <div ng-show="htmlError != null && htmlError.length > 0" style="padding-bottom: 15px;">
                <span class="text-danger">Something went wrong. Below you can see error.</span>
                <br/>
                <button class="btn btn-warning" type="button" data-toggle="collapse" data-target="#htmlError" aria-expanded="false" aria-controls="html_error">Show error</button>
                <div id="htmlError" class="collapse">
                    <div class="well">
                        <span>{{htmlError}}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row main">
            <div class="col-md-2">
                <h4 class="simple-header">Select Test</h4>
                <g:btpText id="testFilter" label="Test Filter" ng-model="testFilter" placeholder="Quick search..."/>
                <div class="list-group searchable-list long-list">
                    <button type="button" class="list-group-item text-nooverflow" ng-repeat="test in testList | orderBy:'name' | findByNameOrId:testFilter:curTest.id track by test.id"
                            ng-class="{active : test.id == curTest.id}" ng-click="tc.loadTest(test)">{{test.name}}</button>
                </div>
            </div>
            <div class="col-md-10">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form1-holder" ng-show="curTest.id != null">
                            <form name="form_test">
                                <h4 class="simple-header">Test Settings</h4>
                                <g:btpText id="test_name" label="Test Name" ng-model="curTest.name" angError="true"/>
                                <g:btpTextArea id="test_description" label="Description" ng-model="curTest.description" angError="true"/>
                                <g:btpText id="test_listPath" label="Path to List of elements in JSON or XML source" ng-model="curTest.listPath" angError="true"/>
                                <g:btpText id="test_spiraPrjId" label="Project ID from SpiraTest system" ng-model="curTest.spiraPrjId" angError="true"/>
                                <g:btpText id="test_spiraTestId" label="Test Case ID from SpiraTest system" ng-model="curTest.spiraTestId" angError="true"/>
    
                                <select-search id="test_webTest1" model="webTest1Tmp" options="webTestList" before-callback="tc.canReloadForm2" after-callback="tc.loadForm2"
                                               val-message="test_webTest1Err">
                                    <select-label>
                                        <is-changed-label descr="Web Service 1" tooltip="Field has been changed. Do not forget to apply changes" value="curTest.webTest1.name"></is-changed-label>
                                    </select-label>
                                </select-search>
                                <select-search id="test_webTest2" model="webTest2Tmp" options="webTestList" before-callback="tc.canReloadForm2" after-callback="tc.loadForm2"
                                               val-message="test_webTest2Err" ng-show="xml2XmlParamTmp.id > 0">
                                    <select-label>
                                        <is-changed-label descr="Web Service 2" tooltip="Field has been changed. Do not forget to apply changes" value="curTest.webTest2.name"></is-changed-label>
                                        <span aria-hidden="true" class="glyphicon glyphicon-info-sign text-danger" data-toggle="tooltip"
                                              data-original-title="Leave empty, if testing same WS on different environments"></span>
                                    </select-label>
                                </select-search>
                                <select-search id="test_arrayComboGroup" model="arrayComboGroupTmp" options="arrayComboGroupList" before-callback="tc.canReloadForm2" after-callback="tc.loadForm2"
                                               val-message="test_arrayComboGroupErr">
                                    <select-label>
                                        <is-changed-label descr="Array Combo Group" tooltip="Field has been changed. Do not forget to apply changes" value="curTest.arrayComboGroup.name"></is-changed-label>
                                    </select-label>
                                </select-search>
                                <select-search id="test_sqlComboGroup" model="sqlComboGroupTmp" options="sqlComboGroupList" before-callback="tc.canReloadForm2" after-callback="tc.loadForm2"
                                               val-message="test_sqlComboGroupErr">
                                    <select-label>
                                        <is-changed-label descr="Sql Combo Group" tooltip="Field has been changed. Do not forget to apply changes" value="curTest.sqlComboGroup.name"></is-changed-label>
                                    </select-label>
                                </select-search>
                                <select-search id="test_xml2XmlParam" model="xml2XmlParamTmp" options="xml2XmlParamList" before-callback="tc.canReloadForm2" after-callback="tc.loadForm2"
                                               val-message="test_xml2XmlParamErr">
                                    <select-label>
                                        <is-changed-label descr="Xml 2 Xml Comparison Test Parameters" tooltip="Field has been changed. Do not forget to apply changes" value="curTest.xml2XmlParam.name"></is-changed-label>
                                    </select-label>
                                </select-search>
                                <select-search id="test_xml2DbParam" model="xml2DbParamTmp" options="xml2DbParamList" before-callback="tc.canReloadForm2" after-callback="tc.loadForm2"
                                               val-message="test_xml2DbParamErr">
                                    <select-label>
                                        <is-changed-label descr="Xml 2 DB Comparison Test Parameters" tooltip="Field has been changed. Do not forget to apply changes" value="curTest.xml2DbParam.name"></is-changed-label>
                                    </select-label>
                                </select-search>
                            </form>
                            
                            <div ng-show="sqlComboGroupTmp.id > 0">
                                <g:btpSelectBox multiple="true" id="test_sqlComboParam" label="Sql Combo Parameters" angularOptions="curTest.sqlComboParam"
                                                ng-model="form5Tmp" ng-change="tc.loadForm5('sqlComboParam', form5Tmp[0])" optionsMapping="${[name:"name +' = '+ option.value ", orderBy:"'name'"]}"/>
                                <button type="button" class="btn btn-block" ng-show="curTest.id > 0" ng-click="tc.newItem5('sqlComboParam')">Create new Parameter</button>
                                <span class="text-warning" ng-show="curTest.id <= 0">Create new test before Parameter creation</span>
                            </div>

                            <button type="button" class="btn btn-default" ng-click="tc.submitTest()">{{tc.getSaveName(curTest.id)}}</button>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div ng-show="form2Struct.name != '' && form2Struct.model.id != 0">
                            <form name="form_webTest" ng-show="['webTest1', 'webTest2'].indexOf(form2Struct.name) >= 0">
                                <h4 class="simple-header">Web Service Settings</h4>
                                <g:btpText id="webTest_name" label="Web Service Name (exact WS name)" ng-model="form2Struct.model.name" angError="true"/>
                                <g:btpText id="webTest_pathPattern" label="Path pattern" ng-model="form2Struct.model.pathPattern" angError="true"/>
                                <g:btpText id="webTest_webServiceName" label="Web Service" ng-model="form2Struct.model.webServiceName" angError="true"/>
                                <g:btpSelectBox id="webTest_method" label="Method" angularOptionsArray=" ['GET', 'POST']" ng-model="form2Struct.model.hMethod"/>
                                <!-- <g:btpSelectBox id="webTest_format" label="Format" angularOptionsArray=" ['XML', 'JSON']" ng-model="form2Struct.model.format"/> -->
                            </form>
                            <form name="form_sqlComboGroup" ng-show="form2Struct.name == 'sqlComboGroup'">
                                <h4 class="simple-header">Sql Combinations</h4>
                                <g:btpText id="sqlComboGroup_name" label="Combo Group Name" ng-model="form2Struct.model.name" angError="true"/>
                                <g:btpTextArea id="sqlComboGroup_query" label="SQL Query" ng-model="form2Struct.model.query" angError="true" ui-codemirror="codemirrorOptions"/>
                            </form>
                            <div ng-show="form2Struct.name == 'arrayComboGroup'">
                                <form name="form_arrayComboGroup">
                                    <h4 class="simple-header">Array Combinations</h4>
                                    <g:btpText id="arrayComboGroup_name" label="Combo Group Name" ng-model="form2Struct.model.name" angError="true"/>
                                </form>
                                <g:btpSelectBox multiple="true" id="arrayComboGroup_arrayComboParam" label="Array Combo Parameters" angularOptions="form2Struct.model.arrayComboParam"
                                                ng-model="form3Tmp" ng-change="tc.loadForm3('arrayComboParam', form3Tmp[0])" optionsMapping="${[name:"name +' = '+ option.value ", orderBy:"'name'"]}"/>
                                <button type="button" class="btn btn-block" ng-show="form2Struct.model.id > 0" ng-click="tc.newItem3('arrayComboParam')">Create new Parameter</button>
                                <span class="text-warning" ng-show="form2Struct.model.id <= 0">Create new group before Parameter creation</span>
                            </div>
                            <div ng-show="form2Struct.name == 'xml2DbParam'">
                                <form name="form_xml2DbParam">
                                    <h4 class="simple-header">Xml 2 DB Comparison Test Parameters</h4>
                                    <g:btpText id="xml2DbParam_name" label="Xml 2 DB Test Name" ng-model="form2Struct.model.name" angError="true"/>
                                    <g:btpText id="xml2DbParam_ignoredValue" label="Ignored Value" ng-model="form2Struct.model.ignoredValue" angError="true"/>
                                    <g:btpTextArea id="xml2DbParam_query" label="SQL Query" ng-model="form2Struct.model.query" angError="true" ui-codemirror="codemirrorOptions"/>
                                    <g:btpCheckbox id="xml2DbParam_orderlySafeMode" label="Orderly Safe Mode" ng-model="form2Struct.model.orderlySafeMode"/>
                                    <g:btpCheckbox id="xml2DbParam_orderlySafeArrayMode" label="Orderly Safe Mode for elements in Array and Included nodes" ng-model="form2Struct.model.orderlySafeArrayMode"/>
                                    <!-- <g:btpCheckbox id="xml2DbParam_orderlySafeIncludedMode" label="Orderly Safe Mode for elements found in SubQuery" ng-model="form2Struct.model.orderlySafeIncludedMode"/> -->
                                </form>
                                <g:btpSelectBox multiple="true" id="xml2DbParam_includedMapping" label="Xml 2 DB Test Included Queries" angularOptions="form2Struct.model.includedMapping"
                                                ng-model="form3Tmp" ng-change="tc.loadForm3('includedMapping', form3Tmp[0])" optionsMapping="${[name:"name +' = '+ option.query ", orderBy:"'name'"]}"/>
                                <button type="button" class="btn btn-block" ng-show="form2Struct.model.id > 0" ng-click="tc.newItem3('includedMapping')">Create new Parameter</button>
                                <span class="text-warning" ng-show="form2Struct.model.id <= 0">Create new group before Parameter creation</span>
                            </div>
                            <div ng-show="form2Struct.name == 'xml2XmlParam'">
                                <form name="form_xml2XmlParam">
                                    <h4 class="simple-header">Xml 2 Xml Comparison Test Parameters</h4>
                                    <g:btpText id="xml2XmlParam_name" label="Xml 2 Xml Test Name" ng-model="form2Struct.model.name" angError="true" ng-disabled="form2Struct.model.id == 1"/>
                                    <g:btpCheckbox id="xml2XmlParam_orderlySafeMode" label="Orderly Safe Mode" ng-model="form2Struct.model.orderlySafeMode" ng-click="form2Struct.model.needleHelper = ''"
                                                   ng-disabled="form2Struct.model.id == 1"/>
                                    <g:btpText id="xml2XmlParam_needleHelper" label="XmlTree path to attributes or nodes to be pre-matched" ng-model="form2Struct.model.needleHelper" angError="true" 
                                               ng-disabled="form2Struct.model.orderlySafeMode || form2Struct.model.id == 1"/>
                                    <g:btpCheckbox id="xml2XmlParam_orderlySafeChildrenMode" label="Orderly Safe Mode for Inner Elements" ng-model="form2Struct.model.orderlySafeChildrenMode"
                                                   ng-disabled="form2Struct.model.id == 1"/>
                                </form>
                                <g:btpSelectBox multiple="true" id="xml2XmlParam_ignoredElement" label="Xml 2 Xml Test Ignored Elements" angularOptions="form2Struct.model.ignoredElement"
                                                ng-model="form3Tmp" ng-change="tc.loadForm3('ignoredElement', form3Tmp[0])" optionsMapping="${[name:"name +' = '+ option.value ", orderBy:"'name'"]}"
                                                ng-disabled="form2Struct.model.id == 1"/>
                                <button type="button" class="btn btn-block" ng-show="form2Struct.model.id > 1" ng-click="tc.newItem3('ignoredElement')">Create new Parameter</button>
                                <span class="text-warning" ng-show="form2Struct.model.id <= 0">Create new group before Parameter creation</span>
                            </div>

                            <button type="button" class="btn btn-default" ng-click="tc.submitForm2()" ng-show="form2Struct.model.id != 1">{{tc.getSaveName(form2Struct.model.id)}}</button>
                            <button type="button" class="btn btn-default" ng-click="tc.submitForm2(true)" ng-show="curTest.id > 0">{{tc.getSaveName(form2Struct.model.id)}} & Select</button>
                        </div>
                        <div class="form5-holder" ng-show="form5Struct.model.id != null && form5Struct.name != ''">
                            <form name="form_sqlComboParam" ng-show="form5Struct.name == 'sqlComboParam'">
                                <h4 class="">Combo parameters (this will be passed into DbQuery and WsCall)</h4>
                                <g:btpText id="sqlComboParam_name" label="Parameter" ng-model="form5Struct.model.name" angError="true"/>
                                <g:btpText id="sqlComboParam_value" label="Combinations Parameter Value" ng-model="form5Struct.model.value" angError="true"/>
                            </form>
                            <button type="button" class="btn btn-default" ng-click="tc.submitForm5()">{{tc.getSaveName(form5Struct.model.id)}}</button>
                            <button type="button" class="btn btn-default" ng-show="form5Struct.model.id > 0" ng-click="tc.deleteForm5()">Delete</button>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form3-holder" ng-show="form3Struct.model.id != null && form3Struct.name != ''">
                            <form name="form_arrayComboParam" ng-show="form3Struct.name == 'arrayComboParam'">
                                <h4 class="">Combo parameters (this will be passed into DbQuery and WsCall)</h4>
                                <g:btpText id="arrayComboParam_name" label="Parameter" ng-model="form3Struct.model.name" angError="true"/>
                                <g:btpText id="arrayComboParam_value" label="Combinations Parameter Values (separate by comma)" ng-model="form3Struct.model.value" angError="true"/>
                            </form>
                            <form name="form_includedMapping" ng-show="form3Struct.name == 'includedMapping'">
                                <h4 class="">SQL queries for included Elements</h4>
                                <g:btpText id="includedMapping_name" label="XmlTree Path to the element" ng-model="form3Struct.model.name" angError="true"/>
                                <g:btpTextArea id="includedMapping_query" label="SQL Query" ng-model="form3Struct.model.query" angError="true" ui-codemirror="codemirrorOptions"/>
                            </form>
                            <form name="form_ignoredElement" ng-show="form3Struct.name == 'ignoredElement'">
                                <h4 class="">XML Ignored Elements</h4>
                                <g:btpText id="ignoredElement_name" label="XmlTree Path to the element to be ignored" ng-model="form3Struct.model.name" angError="true"/>
                                <g:btpText id="ignoredElement_value" label="Ignore element only if it has value (Optional)" ng-model="form3Struct.model.value" angError="true"/>
                            </form>
                            <button type="button" class="btn btn-default" ng-click="tc.submitForm3()">{{tc.getSaveName(form3Struct.model.id)}}</button>
                            <button type="button" class="btn btn-default" ng-show="form3Struct.model.id > 0" ng-click="tc.deleteForm3()">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>