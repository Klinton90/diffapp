<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="menu"/>
    <title>${title}</title>
    <asset:javascript src="app/controllers/testCaseCreatorController.js"/>
</head>
<body>
<div ng-app="diffapp">
    <div ng-controller="testCaseCreatorController as tcc">
        <div class="container">
            <h2>Test Case Creator</h2>
            <hr>
            <div class="row main">
                <span id="adErrors">{{adErrors}}</span>
                <div ng-show="htmlError != null && htmlError.length > 0" style="padding-bottom: 15px;">
                    <span class="text-danger">Something went wrong. Below you can see error.</span>
                    <br/>
                    <button class="btn btn-warning" type="button" data-toggle="collapse" data-target="#htmlError" aria-expanded="false" aria-controls="html_error">Show error</button>
                    <div id="htmlError" class="collapse">
                        <div class="well">
                            <span>{{htmlError}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row main">
                <div class="col-md-2">
                    <h4 class="simple-header">Select Test Case</h4>
                    <g:btpText id="testCaseFilter" label="Test Case Filter" ng-model="testCaseFilter" placeholder="Quick search..."/>
                    <div class="list-group searchable-list medium-list">
                        <button type="button" class="list-group-item text-nooverflow" ng-repeat="testCase in testCaseList | orderBy:'name' | findByNameOrId:testCaseFilter:curTestCase.id track by testCase.id"
                                ng-class="{active : testCase.id == curTestCase.id}" ng-click="tcc.loadTestCase(testCase)" data-toggle="tooltip" data-original-title="{{testCase.name}}" data-placement="left" 
                                last-ready="tcc.initTooltip()">{{testCase.name}}</button>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="row" ng-show="curTestCase.id != null">
                        <div class="col-md-6">
                            <form name="form_testCase">
                                <h4 class="simple-header">Test Case Listing</h4>
                                <g:btpText id="testCase_name" label="Test Case Name" ng-model="curTestCase.name" angError="true"/>
                                <g:btpTextArea id="testCase_description" label="Description" ng-model="curTestCase.description" angError="true"/>
                                
                                <select-search id="testCase_dbSetting" model="dbSettingTmp" options="dbSettingList" before-callback="tcc.canReloadForm2" after-callback="tcc.loadForm2"
                                               val-message="testCase_dbSettingErr">
                                    <select-label>
                                        <is-changed-label descr="DB Settings" tooltip="Field has been changed. Do not forget to apply changes" value="curTestCase.dbSetting.name"></is-changed-label>
                                    </select-label>
                                </select-search>
                                <select-search id="testCase_webEnvironment1" model="webEnvironment1Tmp" options="webEnvironmentList" before-callback="tcc.canReloadForm2" after-callback="tcc.loadForm2"
                                               val-message="testCase_webEnvironment1Err">
                                    <select-label>
                                        <is-changed-label descr="Web Environment" tooltip="Field has been changed. Do not forget to apply changes" value="curTestCase.webEnvironment1.name"></is-changed-label>
                                    </select-label>
                                </select-search>
                                <select-search id="testCase_webEnvironment2" model="webEnvironment2Tmp" options="webEnvironmentList" before-callback="tcc.canReloadForm2" after-callback="tcc.loadForm2"
                                               val-message="testCase_webEnvironment2Err">
                                    <select-label>
                                        <is-changed-label descr="Web Environment" tooltip="Field has been changed. Do not forget to apply changes" value="curTestCase.webEnvironment2.name"></is-changed-label>
                                    </select-label>
                                </select-search>
                                
                                <g:btpSelectBox multiple="true" id="testCase_test" label="Included Tests" angularOptions="curTestCase.test" ng-model="_testTmp"/>
                                <span class="text-warning" ng-show="curTestCase.id <= 0">Save Test Case before adding Tests</span>
                                <br/>
                                <button type="button" class="btn btn-default" ng-click="tcc.submitTestCase()">{{tcc.getSaveName(curTestCase.id)}}</button>
                                <button type="button" class="btn btn-default" ng-show ="_testTmp != null && _testTmp.length > 0" ng-click="tcc.removeTest()">Remove Test</button>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <div class="formAdd-holder" ng-show="curTestCase.id > 0">
                                <h4 class="simple-header">Add Tests</h4>
                                <g:btpText id="testFilter" label="Test Filter" ng-model="testFilter" placeholder="Quick search..."/>
                                <div class="list-group searchable-list testcase-test-list">
                                    <button type="button" class="list-group-item text-nooverflow" ng-repeat="test in testList | orderBy:'name' | findByNameOrId:testFilter:null:curTestCase.test:'name':null:'id' track by test.id"
                                            ng-class="{active : test.id == curTest.id}" ng-click="tcc.selectTest(test)">{{test.name}}</button>
                                </div>
                                <button ng-show="curTest != null && tcc.findIndex(curTestCase.test, curTest) == -1" type="button" class="btn btn-default" ng-click="tcc.addTest()">Add Selected test to TestCase</button>
                            </div>
                            <hr/>
                            <div class="form2-holder" ng-show="form2Struct.model.id != null && form2Struct.name != ''">
                                <form name="form_dbSetting" ng-show="form2Struct.name == 'dbSetting'">
                                    <h4 class="simple-header">Data Base Settings</h4>
                                    <g:btpText id="dbSetting_name" label="Data Base Name (Description)" ng-model="form2Struct.model.name" angError="true"/>
                                    <g:btpText id="dbSetting_url" label="Data Base Url" ng-model="form2Struct.model.url" angError="true"/>
                                    <g:btpText id="dbSetting_login" label="Login (Credentials)" ng-model="form2Struct.model.login" angError="true"/>
                                    <g:btpText id="dbSetting_password" label="Password (Credentials)" ng-model="form2Struct.model.password" angError="true"/>
                                    <g:btpSelectBox id="dbSetting_dbDriver" label="Data Base Driver" angularOptions="dbDriverList" ng-model="dbDriverTmp" angError="true"/>
                                </form>
                                <form name="form_webEnvironment" ng-show="['webEnvironment1', 'webEnvironment2'].indexOf(form2Struct.name) >= 0">
                                    <h4 class="simple-header">Web Environment Settings</h4>
                                    <g:btpText id="webEnvironment_name" label="Web Environment Name" ng-model="form2Struct.model.name" angError="true"/>
                                    <g:btpText id="webEnvironment_url" label="Url" ng-model="form2Struct.model.url" angError="true"/>
                                </form>

                                <button type="button" class="btn btn-default" ng-click="tcc.submitForm2()">{{tcc.getSaveName(form2Struct.model.id)}}</button>
                                <button type="button" class="btn btn-default" ng-click="tcc.submitForm2(true)" ng-show="form2Struct.model.id > 0">{{tcc.getSaveName(form2Struct.model.id)}} & Select</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>