<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="menu"/>
    <title>${title}</title>
    <asset:javascript src="app/controllers/db2DbComparisonController.js"/>
</head>
<body>
<div ng-app="diffapp">
    <div ng-controller="db2DbComparisonController as ctrl" ng-init="defDb2DbParamId = ${params.id ?: 0}; ctrl.init();">
        <h1>DB Comparison</h1>
        <div class="row">
            <div class="alert" role="alert" ng-class="{'OK':'alert-success', 'WARN':'alert-warning', 'ERROR':'alert-danger', 'ERROR2':'alert-danger', 'INFO':'alert-info', 'INFO2':'alert-info'}[status]">
                Current status: <p ng-bind-html="statuses[status]"></p>
            </div>
            <div ng-show="htmlError != null" class="panel panel-default">
                <div class="panel-heading" role="tab" id="htmlErrorHeading">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#htmlErrorPanel" aria-expanded="false" aria-controls="htmlErrorPanel">
                            Click here to review Error code.
                        </a>
                    </h4>
                </div>
                <div id="htmlErrorPanel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="htmlErrorHeading">
                    <div id="htmlError" class="panel-body" ng-bind-html="htmlError"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <h4 class="simple-header">Select DB Test</h4>
                <g:btpText id="db2DbParam" label="DB Test Filter" ng-model="db2DbParamFilter" placeholder="Quick search..."/>
                <div class="list-group searchable-list long-list">
                    <button type="button" class="list-group-item text-nooverflow"
                            ng-repeat="db2DbParam in db2DbParamList | orderBy:'name' | findByNameOrId:db2DbParamFilter:curDb2DbParam.id track by db2DbParam.id"
                            ng-class="{active : db2DbParam.id == curDb2DbParam.id}" ng-click="ctrl.loadTest(db2DbParam)">{{db2DbParam.name}}</button>
                </div>
            </div>
            <div class="col-md-10" ng-show="curDb2DbParam.id != null">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="testCreatorHeading">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#testCreatorBody" aria-expanded="false" aria-controls="testCreatorBody">Test Creator</a>
                        </h4>
                    </div>
                    <div id="testCreatorBody" class="panel-collapse collapse" role="tabpanel" aria-labelledby="testCreatorHeading">
                        <div class="panel-body">
                            <div class="col-md-4">
                                <div class="form1-holder">
                                    <form name="form_db2DbParam">
                                        <h4 class="simple-header">DB Test Settings</h4>
                                        <g:btpText id="db2DbParam_name" label="Test Setting Name" ng-model="curDb2DbParam.name" angError="true"/>
                                        <g:btpTextArea id="db2DbParam_description" label="Description/Notes" ng-model="curDb2DbParam.description" placeholder="Description/Notes" angError="true" rows="3"/>

                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="query1Heading">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#query1Body" aria-expanded="false" aria-controls="query1Body">
                                                        SQL Query 1 (Required)<br/>
                                                        <span ng-show="db2DbParam_query1Err" class="text-danger">{{db2DbParam_query1Err}}</span>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="query1Body" class="panel-collapse collapse" role="tabpanel" aria-labelledby="query1Heading">
                                                <div class="panel-body">
                                                    <g:btpTextArea id="db2DbParam_query1" ng-model="curDb2DbParam.query1" rows="3" ui-codemirror="codemirrorOptions"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="query2Heading">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#query2Body" aria-expanded="false" aria-controls="query2Body">
                                                        SQL Query 2 (Optional)<br/>
                                                        <span ng-show="db2DbParam_query2Err" class="text-danger">{{db2DbParam_query2Err}}</span>
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="query2Body" class="panel-collapse collapse" role="tabpanel" aria-labelledby="query2Heading">
                                                <div class="panel-body">
                                                    <g:btpTextArea id="db2DbParam_query2" ng-model="curDb2DbParam.query2" rows="3" ui-codemirror="codemirrorOptions"/>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <g:btpText id="db2DbParam_ignoredElements" label="Excluded Columns" ng-model="curDb2DbParam.ignoredElements" angError="true"/>
                                        <g:btpCheckbox id="db2DbParam_orderlySafeMode" label="Set 'Y' to check row order" ng-model="curDb2DbParam.orderlySafeMode" angError="true"/>

                                        <select-search id="db2DbParam_dbSetting1" model="dbSetting1Tmp" options="dbSettingList" before-callback="ctrl.canReloadForm2" after-callback="ctrl.loadForm2"
                                                       val-message="db2DbParam_dbSetting1Err">
                                            <select-label>
                                                <is-changed-label descr="DB Setting 1" tooltip="Field has been changed. Do not forget to apply changes" value="curDb2DbParam.dbSetting1.name"></is-changed-label>
                                            </select-label>
                                        </select-search>
                                        <select-search id="db2DbParam_dbSetting2" model="dbSetting2Tmp" options="dbSettingList" before-callback="ctrl.canReloadForm2" after-callback="ctrl.loadForm2"
                                                       val-message="db2DbParam_dbSetting2Err">
                                            <select-label>
                                                <is-changed-label descr="DB Setting 2" tooltip="Field has been changed. Do not forget to apply changes" value="curDb2DbParam.dbSetting2.name"></is-changed-label>
                                            </select-label>
                                        </select-search>

                                        <button type="button" class="btn btn-default" ng-click="ctrl.submitTest()">{{ctrl.getSaveName(curDb2DbParam.id)}}</button>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form2-holder" ng-show="form2Struct.model.id != null && form2Struct.name != ''">
                                    <form name="form_dbSetting" ng-show="['dbSetting1', 'dbSetting2'].indexOf(form2Struct.name) >= 0">
                                        <h4 class="simple-header">Data Base Settings</h4>
                                        <g:btpText id="dbSetting_name" label="Data Base Name (Description)" ng-model="form2Struct.model.name" angError="true"/>
                                        <g:btpText id="dbSetting_url" label="Data Base Url" ng-model="form2Struct.model.url" angError="true"/>
                                        <g:btpText id="dbSetting_login" label="Login (Credentials)" ng-model="form2Struct.model.login" angError="true"/>
                                        <g:btpText id="dbSetting_password" label="Password (Credentials)" ng-model="form2Struct.model.password" angError="true"/>
                                        <g:btpSelectBox id="dbSetting_dbDriver" label="Data Base Driver" angularOptions="dbDriverList" ng-model="dbDriverTmp" angError="true"/>
                                    </form>

                                    <button type="button" class="btn btn-default" ng-click="ctrl.submitForm2()">{{ctrl.getSaveName(form2Struct.model.id)}}</button>
                                    <button type="button" class="btn btn-default" ng-click="ctrl.submitForm2(true)" ng-show="curDb2DbParam.id > 0">{{ctrl.getSaveName(form2Struct.model.id)}} & Select</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="testRunnerHeading">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#testRunnerBody" aria-expanded="false" aria-controls="testRunnerBody">Test Runner</a>
                        </h4>
                    </div>
                    <div id="testRunnerBody" class="panel-collapse collapse" role="tabpanel" aria-labelledby="testRunnerHeading">
                        <div class="panel-body">
                            <div>
                                <button class="btn btn-default" ng-click="ctrl.startTest()" ng-show="status != 'INFO2'">Start my Test</button>
                                <button class="btn btn-default" ng-click="ctrl.checkTest()" ng-show="status == 'INFO2'">Refresh</button>
                            </div>
                            <div class="progress" ng-show="status == 'INFO2'">
                                <div class="progress-bar progress-bar-striped active" role="progressbar" style="width: {{progress}}%">
                                    <span>{{progress}}% Complete</span>
                                </div>
                            </div>

                            <!-- <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="originalOutHeading">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#originalOutBody" aria-expanded="false" aria-controls="originalOutBody">
                                            Original ({{resultObject.totalOriginal1}} : {{resultObject.totalOriginal2}})
                                        </a>
                                    </h4>
                                </div>
                                <div id="originalOutBody" class="panel-collapse collapse" role="tabpanel" aria-labelledby="originalOutHeading">
                                    <div class="panel-body">
                                        <div id="originaloutput"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingPreOutput">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsePreOutput" aria-expanded="false" aria-controls="collapsePreOutput">
                                            Prepared ({{resultObject.totalPrep1}} : {{resultObject.totalPrep2}})
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapsePreOutput" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPreOutput">
                                    <div class="panel-body">
                                        <div id="prepoutput"></div>
                                    </div>
                                </div>
                            </div> -->
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingDiffOutput">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDiffOutput" aria-expanded="false" aria-controls="collapseDiffOutput">
                                            Found Differences ({{resultObject.totalOut1}} : {{resultObject.totalOut2}})
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseDiffOutput" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingDiffOutput">
                                    <div class="panel-body">
                                        <div id="diffoutput"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>