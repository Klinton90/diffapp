<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="menu"/>
    <title>${title}</title>
    <asset:javascript src="app/controllers/csvTestController.js"/>
</head>
<body>
<div ng-app="diffapp">
    <div ng-controller="csvTestController as ctrl" ng-init="defCsv2DbParamId = ${params.id ?: 0}; ctrl.init();">
        <h1>CSV Comparison</h1>
        <div class="row">
            <div class="alert" role="alert" ng-class="{'OK':'alert-success', 'WARN':'alert-warning', 'ERROR':'alert-danger', 'ERROR2':'alert-danger', 'INFO':'alert-info', 'INFO2':'alert-info'}[status]">
                Current status: <p ng-bind-html="statuses[status]"></p>
            </div>
            <div ng-show="htmlError != null" class="panel panel-default">
                <div class="panel-heading" role="tab" id="htmlErrorHeading">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#htmlErrorPanel" aria-expanded="false" aria-controls="htmlErrorPanel">
                            Click here to review Error code.
                        </a>
                    </h4>
                </div>
                <div id="htmlErrorPanel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="htmlErrorHeading">
                    <div id="htmlError" class="panel-body" ng-bind-html="htmlError"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <h4 class="simple-header">Select CSV Test</h4>
                <g:btpText id="csv2DbParam" label="Csv Test Filter" ng-model="csv2DbParamFilter" placeholder="Quick search..."/>
                <div class="list-group searchable-list long-list">
                    <button type="button" class="list-group-item text-nooverflow"
                            ng-repeat="csv2DbParam in csv2DbParamList | orderBy:'name' | findByNameOrId:csv2DbParamFilter:curCsv2DbParam.id track by csv2DbParam.id"
                            ng-class="{active : csv2DbParam.id == curCsv2DbParam.id}" ng-click="ctrl.loadTest(csv2DbParam)">{{csv2DbParam.name}}</button>
                </div>
            </div>
            <div class="col-md-10" ng-show="curCsv2DbParam.id != null">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="testCreatorHeading">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#testCreatorBody" aria-expanded="false" aria-controls="testCreatorBody">Test Creator</a>
                        </h4>
                    </div>
                    <div id="testCreatorBody" class="panel-collapse collapse" role="tabpanel" aria-labelledby="testCreatorHeading">
                        <div class="panel-body">
                            <div class="col-md-4">
                                <div class="form1-holder">
                                    <form name="form_csv2DbParam">
                                        <h4 class="simple-header">CSV Test Settings</h4>
                                        <g:btpText id="csv2DbParam_name" label="Test Setting Name" ng-model="curCsv2DbParam.name" placeholder="Test Setting Name" angError="true"/>
                                        <g:btpTextArea id="csv2DbParam_description" label="Description/Notes" ng-model="curCsv2DbParam.description" placeholder="Description/Notes" angError="true" rows="3"/>
                                        <g:btpTextArea id="csv2DbParam_query" label="SQL Query" ng-model="curCsv2DbParam.query" placeholder="SQL Query" angError="true" rows="3" ui-codemirror="codemirrorOptions"/>
                                        <g:btpCheckbox id="csv2DbParam_skipMissedFeed" label="Set 'Y' to skip additional columns that don't exist in feed mapping" ng-model="curCsv2DbParam.skipMissedFeed" angError="true"/>
                                        <g:btpCheckbox id="csv2DbParam_orderlySafeMode" label="Set 'Y' to check row order" ng-model="curCsv2DbParam.orderlySafeMode" angError="true"/>
                
                                        <select-search id="csv2DbParam_dbSetting" model="dbSettingTmp" options="dbSettingList" before-callback="ctrl.canReloadForm2" after-callback="ctrl.loadForm2"
                                                       val-message="csv2DbParam_dbSettingErr">
                                            <select-label>
                                                <is-changed-label descr="DB Settings" tooltip="Field has been changed. Do not forget to apply changes" value="curCsv2DbParam.dbSetting.name"></is-changed-label>
                                            </select-label>
                                        </select-search>
            
                                        <select-search id="csv2DbParam_csvSetting" model="csvSettingTmp" options="csvSettingList" before-callback="ctrl.canReloadForm2" after-callback="ctrl.loadForm2"
                                                       val-message="csv2DbParam_csvSettingErr">
                                            <select-label>
                                                <is-changed-label descr="CSV Importer Settings" tooltip="Field has been changed. Do not forget to apply changes" value="curCsv2DbParam.csvSetting.name"></is-changed-label>
                                            </select-label>
                                        </select-search>
                                        
                                        <div>
                                            <g:btpSelectBox multiple="true" id="csv2DbParam_ignoredRow" label="Ignored Rows" angularOptions="curCsv2DbParam.ignoredRow" rows="15"
                                                            ng-model="form5Tmp" ng-change="ctrl.loadForm5('ignoredRow', form5Tmp[0])" optionsMapping="${[namePattern:"getIgnoredRowName", orderBy:"myOrderBy"]}"/>
                                            <button type="button" class="btn btn-block" ng-show="curCsv2DbParam.id > 0" ng-click="ctrl.newItem5('ignoredRow')">Create new Parameter</button>
                                            <span class="text-warning" ng-show="curCsv2DbParam.id <= 0">Create new test before Parameter creation</span>
                                        </div>
                                            
                                        <button type="button" class="btn btn-default" ng-click="ctrl.submitTest()">{{ctrl.getSaveName(curCsv2DbParam.id)}}</button>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form2-holder" ng-show="form2Struct.model.id != null && form2Struct.name != ''">
                                    <form name="form_dbSetting" ng-show="form2Struct.name == 'dbSetting'">
                                        <h4 class="simple-header">Data Base Settings</h4>
                                        <g:btpText id="dbSetting_name" label="Data Base Name (Description)" ng-model="form2Struct.model.name" angError="true"/>
                                        <g:btpText id="dbSetting_url" label="Data Base Url" ng-model="form2Struct.model.url" angError="true"/>
                                        <g:btpText id="dbSetting_login" label="Login (Credentials)" ng-model="form2Struct.model.login" angError="true"/>
                                        <g:btpText id="dbSetting_password" label="Password (Credentials)" ng-model="form2Struct.model.password" angError="true"/>
                                        <g:btpSelectBox id="dbSetting_dbDriver" label="Data Base Driver" angularOptions="dbDriverList" ng-model="dbDriverTmp" angError="true"/>
                                    </form>
                                    <div ng-show="form2Struct.name == 'csvSetting'">
                                        <form name="form_csvSetting">
                                            <h4 class="simple-header">CSV Importer Settings</h4>
                                            <g:btpText id="csvSetting_name" label="Settings Group Name" ng-model="form2Struct.model.name" angError="true"/>
                                            <g:btpCheckbox id="csvSetting_skipFirstLine" label="Set 'Y' to skip first line" ng-model="form2Struct.model.skipFirstLine" ng-click="needleHelper = ''" angError="true"/>
                                            <div class="col-md-6">
                                                <g:btpText id="csvSetting_splitChar" label="Split line by (character)" ng-model="form2Struct.model.splitChar" angError="true" maxlength="3"/>
                                            </div>
                                            <div class="col-md-6">
                                                <g:btpText id="csvSetting_quoteChar" label="Quote symbol" ng-model="form2Struct.model.quoteChar" angError="true" maxlength="3"/>
                                            </div>
                                        </form>
                                        <g:btpSelectBox multiple="true" id="csvSetting_csvColumn" label="CSV Columns" angularOptions="form2Struct.model.csvColumn" rows="15"
                                                        ng-model="form3Tmp" ng-change="ctrl.loadForm3('csvColumn', form3Tmp[0])" optionsMapping="${[name:"name +' = '+ option.position ", orderBy:"'position'"]}"/>
                                        <button type="button" class="btn btn-block" ng-show="form2Struct.model.id > 0" ng-click="ctrl.newItem3('csvColumn')">Create new Column</button>
                                        <span class="text-warning" ng-show="form2Struct.model.id <= 0">Create new Setting before Column creation</span>
                                    </div>
            
                                    <button type="button" class="btn btn-default" ng-click="ctrl.submitForm2()">{{ctrl.getSaveName(form2Struct.model.id)}}</button>
                                    <button type="button" class="btn btn-default" ng-click="ctrl.submitForm2(true)" ng-show="curCsv2DbParam.id > 0">{{ctrl.getSaveName(form2Struct.model.id)}} & Select</button>
                                </div>
                                <div class="form5-holder" ng-show="form5Struct.model.id != null && form5Struct.name != ''">
                                    <form name="form_ignoredRow" ng-show="form5Struct.name == 'ignoredRow'">
                                        <h4 class="">Ignored Row</h4>
                                        <g:btpText id="ignoredRow_name" label="Key/Column name" ng-model="form5Struct.model.name" angError="true" ng-disabled="form5Struct.model.type != 'REGULAR'"/>
                                        <g:btpText id="ignoredRow_value" label="Value" ng-model="form5Struct.model.value" angError="true" ng-disabled="form5Struct.model.type != 'REGULAR'"/>
                                        <g:btpSelectBox id="ignoredRow_type" label="Type" angularOptionsArray=" ['REGULAR', 'AND', 'OR']" ng-model="form5Struct.model.type" ng-change="ctrl.iRowTypeChanged();"/>
                                        <g:btpSelectBox id="ignoredRow_parentRow" label="Parent Row" angularOptions="curCsv2DbParam.ignoredRow" withDefault="true"
                                                        ng-model="form5Struct.model.parentRow" optionsMapping="${[namePattern:"getIgnoredRowName", orderBy:"myOrderBy", filter:"filter:{type:'!REGULAR', id:'!'+form5Struct.model.id}"]}"/>
                                    </form>
                                    <button type="button" class="btn btn-default" ng-click="ctrl.submitForm5()">{{ctrl.getSaveName(form5Struct.model.id)}}</button>
                                    <button type="button" class="btn btn-default" ng-show="form5Struct.model.id > 0 && !form5Tmp[0].parentRow && !hasChildren" ng-click="ctrl.deleteForm5()">Delete</button>
                                    <label ng-show="form5Tmp[0].parentRow || hasChildren">If you want to delete this option, it should NOT have Parent Row and Children Rows</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form3-holder" ng-show="form3Struct.model.id != null && form3Struct.name != ''">
                                    <form name="form_csvColumn" ng-show="form3Struct.name == 'csvColumn'">
                                        <h4 class="">CSV Column</h4>
                                        <g:btpText id="csvColumn_name" label="Name" ng-model="form3Struct.model.name" angError="true"/>
                                        <g:btpText id="csvColumn_position" label="Column Position" ng-model="form3Struct.model.position" angError="true" type="number"/>
                                        <g:btpText id="csvColumn_ignored" label="Ignored Value" ng-model="form3Struct.model.ignored" angError="true"/>
                                        <g:btpText id="csvColumn_splitChar" label="Split Char" ng-model="form3Struct.model.splitChar" angError="true"/>
                                        <select-search id="csvColumn_modificator" model="modificatorTmp" options="modificatorList" val-message="csvColumn_modificatorErr">
                                            <select-label>
                                                <is-changed-label descr="Modificator" tooltip="Field has been changed. Do not forget to apply changes" value="form3Struct.model.modificator.name"></is-changed-label>
                                            </select-label>
                                        </select-search>
                                    </form>
                                    
                                    <button type="button" class="btn btn-default" ng-click="ctrl.submitForm3()">{{ctrl.getSaveName(form3Struct.model.id)}}</button>
                                    <button type="button" class="btn btn-default" ng-show="form3Struct.model.id > 0" ng-click="ctrl.deleteForm3()">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="testRunnerHeading">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#testRunnerBody" aria-expanded="false" aria-controls="testRunnerBody">Test Runner</a>
                        </h4>
                    </div>
                    <div id="testRunnerBody" class="panel-collapse collapse" role="tabpanel" aria-labelledby="testRunnerHeading">
                        <div class="panel-body">
                            <div>
                                <div class="form-group">
                                    <label for="feedFile">Feed File</label>
                                    <input type="file" id="feedFile">
                                </div>
                                <!-- <button class="btn btn-default" ng-click="ctrl.runTest()">Run my Test</button> -->
                                <button class="btn btn-default" ng-click="ctrl.startTest()" ng-show="status != 'INFO2'">Start my Test</button>
                                <button class="btn btn-default" ng-click="ctrl.checkTest()" ng-show="status == 'INFO2'">Refresh</button>
                                <button class="btn btn-default" ng-click="ctrl.unlockTest()">Unlock my Test</button>
                            </div>
                            <div class="progress" ng-show="status == 'INFO2'">
                                <div class="progress-bar progress-bar-striped active" role="progressbar" style="width: {{progress}}%">
                                    <span>{{progress}}% Complete</span>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="originalOutHeading">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#originalOutBody" aria-expanded="false" aria-controls="originalOutBody">
                                            Original ({{resultObject.totalOriginal1}} : {{resultObject.totalOriginal2}})
                                        </a>
                                    </h4>
                                </div>
                                <div id="originalOutBody" class="panel-collapse collapse" role="tabpanel" aria-labelledby="originalOutHeading">
                                    <div class="panel-body">
                                        <div id="originaloutput"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingPreOutput">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsePreOutput" aria-expanded="false" aria-controls="collapsePreOutput">
                                            Prepared ({{resultObject.totalPrep1}} : {{resultObject.totalPrep2}})
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapsePreOutput" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPreOutput">
                                    <div class="panel-body">
                                        <div id="prepoutput"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingDiffOutput">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDiffOutput" aria-expanded="false" aria-controls="collapseDiffOutput">
                                            Found Differences ({{resultObject.totalOut1}} : {{resultObject.totalOut2}})
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseDiffOutput" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingDiffOutput">
                                    <div class="panel-body">
                                        <div id="diffoutput"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>