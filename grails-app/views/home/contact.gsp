<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="menu"/>
    <title>${title}</title>
</head>
<body>
<h1>Hello User!</h1>
<div class="main">
    <p>If you have questions or suggestions, please contact me with mail</p>
    <a href="mailTo:Alex.Tymofieiev@autodata.net">Alex Tymofieiev</a>
</div>
</body>
</html>