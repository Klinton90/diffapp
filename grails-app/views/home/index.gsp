<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="menu"/>
    <title>${title}</title>
</head>
<body>
    <h1>Welcome to Nissan testing portal</h1>
    <div class="main">
        <p>This tool is designed for testing of Autodata Nissan projects</p>
        <div>
            <label>Available testing</label>
            <ul>
                <li>
                    NCI
                    <ul>
                        <li><g:link controller="Home" action="nci">NCI overview</g:link></li>
                        <li><g:link controller="DataPromotion">NCI data promotion validation</g:link></li>
                    </ul>
                </li>
                <li>
                    NNA
                    <ul>
                        <li><g:link controller="Home" action="nna">NNA Overview</g:link></li>
                        <li><g:link controller="OffersTester">NNA offers validation</g:link></li>
                    </ul>
                </li>
                <li>
                    Simple comparison
                    <ul>
                        <li><g:link controller="SimpleComparison" action="demo">Compare XML texts</g:link></li>
                        <li><g:link controller="SimpleComparison">Compare WS XML response</g:link></li>
                        <li><g:link controller="csvTest">Csv Test</g:link></li>
                        <li><g:link controller="db2DbComparison">Db 2 Db comparison</g:link></li>
                        <li class="list-unstyled">----------------------</li>
                        <li><g:link controller="modificatorCreator">Modificator Creator</g:link></li>
                    </ul>
                </li>
                <li>
                    Automated XML2XML/XML2DB comparison
                    <ul>
                        <li><g:link controller="ComboTest">Test Runner</g:link></li>
                        <li><g:link controller="ComboTest" action="testCreator">Create automated tests</g:link></li>
                        <li><g:link controller="ComboTest" action="testCaseCreator">Create automated TestCases</g:link></li>
                        <li class="list-unstyled">----------------------</li>
                        <li><g:link controller="ComboTest" action="testCaseCreator">XML2DB map helper</g:link></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</body>
</html>