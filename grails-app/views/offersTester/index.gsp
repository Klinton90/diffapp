<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="menu"/>
    <title>${title}</title>
    <asset:javascript src="app/controllers/offersTesterController.js"/>
</head>
<body>
<div class="container" ng-app="diffapp">
    <div ng-controller="offersTesterController as ot" ng-init="offerForm = ${params}; ot.init()">
        <div class="row main">
            <span id="adErrors" class="text-danger">{{adErrors}}</span>
            <div ng-show="htmlError != null && htmlError.length > 0" style="padding-bottom: 15px;">
                <span class="text-danger">Something went wrong. Below you can see error.</span>
                <br/>
                <button class="btn btn-warning" type="button" data-toggle="collapse" data-target="#htmlError" aria-expanded="false" aria-controls="html_error">Show error</button>
                <div id="htmlError" class="collapse">
                    <div class="well">
                        <span>{{htmlError}}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <h1>NNA Offers Testing</h1>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingDownload">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDownload" aria-expanded="false" aria-controls="collapseDownload">Download feed files</a>
                        </h4>
                    </div>
                    <div id="collapseDownload" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingDownload">
                        <div class="panel-body">
                            <button class="btn btn-default" ng-click="ot.downloadFiles()" ng-disabled="isDownloading">Download Offers</button>
                            <div class="well well-lg" ng-show="downloadedFiles != null && downloadedFiles.length > 0">
                                <ul>
                                    <li ng-repeat="file in downloadedFiles">
                                        <a href="fileAction?fileName={{file}}" download="">{{file}}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingUpload">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseUpload" aria-expanded="false" aria-controls="collapseUpload">Upload feed files</a>
                        </h4>
                    </div>
                    <div id="collapseUpload" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingUpload">
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="feedFile">Feed File</label>
                                <input type="file" id="feedFile">
                            </div>
                            <button class="btn btn-default" ng-click="ot.uploadFiles()">Upload Offers</button>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTestOffers">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTestOffers" aria-expanded="false" aria-controls="collapseTestOffers">Test Offers</a>
                        </h4>
                    </div>
                    <div id="collapseTestOffers" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTestOffers">
                        <div class="panel-body">
                            <div class="alert" role="alert" ng-class="{'OK':'alert-success', 'WARN':'alert-warning', 'ERROR':'alert-danger', 'INFO':'alert-info', 'INFO2':'alert-info'}[statusOffers]">
                                Current status: {{statuses[statusOffers]}}
                            </div>
                            <form>
                                <g:btpText id="envLink" label="Environment HOST" ng-model="offerForm.host" placeholder="Environment HOST"/>
                                <g:btpSelectBox id="offerType" label="Select WS type" ng-model="offerForm.type" angularOptionsArray=" ['MODEL', 'TRIM', 'CURRENT']"/>
                                <g:btpText id="offerDma" label="Select DMAs to test" ng-model="offerForm.dma" placeholder="Type DMAs separated by comma ('ALL' function has been turned off due to high load)"/>
                                <div ng-show="offerForm.type == 'MODEL'">
                                    <!-- <g:btpSelectBox id="comparisonMethod" label="Comparison Type" ng-model="offerForm.cmpType" angularOptionsArray=" ['t1', 't2']"/> -->
                                    <g:btpText id="offerYear" label="Select Years to test" ng-model="offerForm.year" placeholder="Type Years separated by comma or type 'ALL' to test all (previous, current, next)"/>
                                    <g:btpText id="offerModel" label="Select Models to test" ng-model="offerForm.model" placeholder="Type Models separated by comma or type 'ALL' to test all"/>
                                </div>
                                <div ng-show="offerForm.type == 'TRIM'">
                                    <g:btpText id="offerTrim" label="Select Trims to test" ng-model="offerForm.acode" placeholder="Type Trims separated by comma or type 'ALL' to test all"/>
                                </div>
                                <div ng-show="offerForm.type == 'MODEL' || offerForm.type == 'TRIM'">
                                    <g:btpCheckbox id="offerAll" label="YES - all offers; NO - featured offers" ng-model="offerForm.all" ng-true-value="'/all'" ng-false-value="''"/>
                                </div>
                                <button class="btn btn-default" ng-click="ot.testOffers()" ng-disabled="isTestingOffers">Test Offers</button>
                                <button class="btn btn-default" ng-click="ot.isOffersRunning()" ng-disabled="!isTestingOffers">Chech Status</button>
                            </form>

                            <div class="progress" ng-show="isTestingOffers">
                                <div class="progress-bar progress-bar-striped active" role="progressbar" style="width: {{offersProgress}}%">
                                    <span>{{offersProgress > 0 ? offersProgress.toString() + "% Complete" : "Initialization"}}</span>
                                </div>
                            </div>
                            <div ng-show="offerForm.cmpType == 't1'">
                                <div class="row">
                                    <div class="col-md-3">
                                        <h4>WS OfferId</h4>
                                    </div>
                                    <div class="col-md-3">
                                        <h4>WS Acodes List</h4>
                                    </div>
                                    <div class="col-md-3">
                                        <h4>Offer OfferId</h4>
                                    </div>
                                    <div class="col-md-3">
                                        <h4>Offer Acodes List</h4>
                                    </div>
                                </div>
                                <div class="row" ng-repeat="row in offersData" style="border-color: black; border-style: solid; border-width: 4px;">
                                    <div class="col-md-12">
                                        <h5>Parameters: <i>{{row.paramSet}}</i></h5>
                                        <div class="col-md-6">
                                            <div class="row" ng-repeat="(feedId, feedAcodes) in row.feedList" style="border-color: brown; border-style: solid; border-width: 0px 2px 2px 0px;">
                                                <div class="col-md-6">
                                                    {{feedId}}
                                                </div>
                                                <div class="col-md-6">
                                                    <span ng-repeat="feedAcode in feedAcodes">
                                                        {{feedAcode}}, 
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row" ng-repeat="(wsId, wsAcodes) in row.wsList" style="border-color: brown; border-style: solid; border-width: 0px 0px 2px 0px;">
                                                <div class="col-md-6">
                                                    {{wsId}}
                                                </div>
                                                <div class="col-md-6">
                                                    <span ng-repeat="wsAcode in wsAcodes">
                                                        {{wsAcode}}, 
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div ng-show="offerForm.cmpType == 't2'">
                                <div class="panel-group" id="accordion_original" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="originalOutHeading">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#accordion_output" href="#originalOutBody" aria-expanded="false" aria-controls="originalOutBody">Original WS outputs</a>
                                            </h4>
                                        </div>
                                        <div id="originalOutBody" class="panel-collapse collapse" role="tabpanel" aria-labelledby="originalOutHeading">
                                            <div class="panel-body">
                                                <div id="originaloutput"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="diffoutput"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>