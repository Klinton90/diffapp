<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="menu"/>
    <title>${title}</title>
    <asset:javascript src="app/controllers/simpleComparisonController.js"/>
</head>
<body>
<div ng-app="diffapp">
    <div ng-controller="simpleComparisonController as sc" ng-init="defScId = ${params.id ?: 0}; sc.init()">
        <h1>Simple WS Comparison</h1>
        <div class="row">
            <div class="col-md-2">
                <h4 class="simple-header">Select WebService</h4>
                <g:btpText id="simpleComparisonSettingFilter" label="Test Filter" ng-model="simpleComparisonSettingFilter" placeholder="Quick search..."/>
                <div class="list-group searchable-list long-list">
                    <button type="button" class="list-group-item text-nooverflow" 
                            ng-repeat="simpleComparisonSetting in simpleComparisonSettingList | orderBy:'name' | findByNameOrId:simpleComparisonSettingFilter:curSimpleComparisonSetting.id track by simpleComparisonSetting.id"
                            ng-class="{active : simpleComparisonSetting.id == curSimpleComparisonSetting.id}" ng-click="sc.loadSettings(simpleComparisonSetting)">{{simpleComparisonSetting.name}}</button>
                </div>
            </div>
            <div class="col-md-10">
                <div class="alert" role="alert" ng-class="{'OK':'alert-success', 'WARN':'alert-warning', 'ERROR':'alert-danger', 'ERROR2':'alert-danger', 'INFO':'alert-info', 'INFO2':'alert-info'}[statusXml]">
                    Current status: <p ng-bind-html="statuses[statusXml]"></p>
                </div>
                <div ng-show="statusXml == 'ERROR2'" class="panel panel-default">
                    <div class="panel-heading" role="tab" id="htmlErrorHeading">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#htmlErrorPanel" aria-expanded="false" aria-controls="htmlErrorPanel">
                                Click here to review Error code.
                            </a>
                        </h4>
                    </div>
                    <div id="htmlErrorPanel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="htmlErrorHeading">
                        <div id="htmlError" class="panel-body"></div>
                    </div>
                </div>
                <div class="container-fluid">
                    <form name="form_simpleComparisonSetting">
                        <div class="row">
                            <div class="col-md-12">
                                <g:btpText id="simpleComparisonSetting_name" label="Test Setting Name" ng-model="curSimpleComparisonSetting.name" placeholder="Test Setting Name" angError="true"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <g:btpTextArea id="simpleComparisonSetting_description" label="Description/Notes" ng-model="curSimpleComparisonSetting.description" placeholder="Description/Notes" 
                                               angError="true" rows="3"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <g:btpText id="simpleComparisonSetting_address1" label="WS Address 1" ng-model="curSimpleComparisonSetting.address1" placeholder="WS Address 1" angError="true"/>
                            </div>
                            <div class="col-md-6">
                                <g:btpText id="simpleComparisonSetting_path1" label="WS Path 1" ng-model="curSimpleComparisonSetting.path1" placeholder="WS Path 1" angError="true"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <g:btpText id="simpleComparisonSetting_address2" label="WS Address 2" ng-model="curSimpleComparisonSetting.address2" placeholder="WS Address 2" angError="true"/>
                            </div>
                            <div class="col-md-6">
                                <g:btpText id="simpleComparisonSetting_path2" label="WS Path 2 (leave empty if you want to use same parameters in both WS calls)" ng-model="curSimpleComparisonSetting.path2" 
                                           placeholder="{{curSimpleComparisonSetting.path1}}" angError="true"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <g:btpLabelWithTooltip forId="xmlPath" label="XML Path" postTooltip="{{xmlPathTooltip}}" class="tooltipWithXml"/>
                                <g:btpText id="simpleComparisonSetting_xmlPath" ng-model="curSimpleComparisonSetting.xmlPath" placeholder="XML Path" angError="true"/>
                            </div>
                            <div class="col-md-6">
                                <g:btpSelectBox id="simpleComparisonSetting_hMethod" label="Method" angularOptionsArray=" ['GET', 'POST']" ng-model="curSimpleComparisonSetting.hMethod" angError="true"/>
                            </div>
                        </div>
                        <div class="row" ng-show="curSimpleComparisonSetting.hMethod == 'POST'">
                            <div class="col-md-12">
                                <g:btpTextArea id="simpleComparisonSetting_postParams" label="POST parameters (JSON string)" ng-model="curSimpleComparisonSetting.postParams" 
                                               placeholder="POST parameters" angError="true" rows="5" ui-codemirror="codemirrorOptions"/>
                                <g:btpCheckbox id="simpleComparisonSetting_postAsString" label="Set 'Y' to treat POST params as String" ng-model="curSimpleComparisonSetting.postAsString" angError="true"/>
                                <g:btpText id="simpleComparisonSetting_postParamsKey" label="Key for POST request text data" ng-model="curSimpleComparisonSetting.postParamsKey" 
                                           placeholder="Key for POST request text data" ng-show="curSimpleComparisonSetting.postAsString" angError="true"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <g:btpCheckbox id="simpleComparisonSetting_orderlySafeMode" label="Set 'Y' to check elements order" ng-model="curSimpleComparisonSetting.orderlySafeMode" ng-click="needleHelper = ''" angError="true"/>
                            </div>
                            <div class="col-md-6">
                                <g:btpCheckbox id="simpleComparisonSetting_orderlySafeChildrenMode" label="Set 'Y' to check INNER elements order" ng-model="curSimpleComparisonSetting.orderlySafeChildrenMode" angError="true"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <g:btpText id="simpleComparisonSetting_needleHelper" label="XmlTree path to attributes or nodes to be pre-matched (MAY improve performance)" ng-model="curSimpleComparisonSetting.needleHelper" 
                                           angError="true" ng-disabled="curSimpleComparisonSetting.orderlySafeMode" angError="true"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <g:btpLabelWithTooltip forId="ignoredElements" label="Ignored Elements" postTooltip="{{ignoredElementsTooltip}}" class="tooltipWithXml2"/>
                                <g:btpText id="simpleComparisonSetting_ignoredElements" ng-model="curSimpleComparisonSetting.ignoredElements" placeholder="Ignored Elements" angError="true"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <button type="submit" class="btn btn-block" ng-click="sc.testXml()" ng-disabled="statusXml == 'INFO2'">Start Testing</button>
                            </div>
                            <div class="col-md-3">
                                <g:btpCheckbox id="autoScroll" label="AutoScroll to Diff Output" ng-model="autoScroll"/>
                            </div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-block" ng-click="sc.saveSettings()" ng-show="curSimpleComparisonSetting.id > 0">Update <b>Existing</b> settings</button>
                            </div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-block" ng-click="sc.saveSettings(true)">Save <b>New</b> settings</button>
                            </div>
                        </div>
                        <div class="row" ng-show="statusXml == 'INFO2'">
                            <div class="col-md-12">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-striped active" role="progressbar" style="width: 100%">
                                        <span>In Proccess</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- <g:btpSelectBox id="webFormat" label="Format" angularOptionsArray=" ['XML', 'JSON']" ng-model="curSimpleComparisonSetting.webFormat"/> -->
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="originalOutHeading">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#originalOutBody" aria-expanded="false" aria-controls="originalOutBody">Original WS outputs</a>
                        </h4>
                    </div>
                    <div id="originalOutBody" class="panel-collapse collapse" role="tabpanel" aria-labelledby="originalOutHeading">
                        <div class="panel-body">
                            <div ng-show="showGetLinks">
                                <a href="{{curSimpleComparisonSetting.address1 + curSimpleComparisonSetting.path1}}" target="_blank">Link1</a>
                                <a href="{{curSimpleComparisonSetting.address2 + (curSimpleComparisonSetting.path1 ? curSimpleComparisonSetting.path1 : curSimpleComparisonSetting.path2)}}" target="_blank">Link2</a>
                            </div>
                            <div ng-show="showPostLinks">
                                <a href="#" ng-click="sc.openPostLink(1)">Link1</a>
                                <a href="#" ng-click="sc.openPostLink(2)">Link2</a>
                            </div>
                            <div id="originaloutput"></div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingPreOutput">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsePreOutput" aria-expanded="false" aria-controls="collapsePreOutput">
                                Prepared XML
                            </a>
                        </h4>
                    </div>
                    <div id="collapsePreOutput" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingPreOutput">
                        <div class="panel-body">
                            <div id="prepoutput"></div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingDiffOutput">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDiffOutput" aria-expanded="false" aria-controls="collapseDiffOutput">
                                Found Differences
                            </a>
                        </h4>
                    </div>
                    <div id="collapseDiffOutput" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingDiffOutput">
                        <div class="panel-body">
                            <div id="diffoutput"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>