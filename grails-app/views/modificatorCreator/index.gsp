<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="menu"/>
    <title>${title}</title>
    <asset:javascript src="app/controllers/modificatorCreatorController.js"/>
</head>
<body>
<div ng-app="diffapp">
    <div ng-controller="modificatorCreatorController as ctrl" ng-init="ctrl.init();">
        <div class="container">
            <h2>Modificator Creator</h2>
            <hr>
            <div class="row main">
                <span id="adErrors" class="text-danger">{{adErrors}}</span>
                <div ng-show="htmlError != null && htmlError.length > 0" style="padding-bottom: 15px;">
                    <span class="text-danger">Something went wrong. Below you can see error.</span>
                    <br/>
                    <button class="btn btn-warning" type="button" data-toggle="collapse" data-target="#htmlError" aria-expanded="false" aria-controls="html_error">Show error</button>
                    <div id="htmlError" class="collapse">
                        <div class="well">
                            <span>{{htmlError}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <h4 class="simple-header">Select Modificator</h4>
                    <g:btpText id="modificatorFilter" label="Modificator Filter" ng-model="modificatorFilter" placeholder="Quick search..."/>
                    <div class="list-group searchable-list medium-list">
                        <button type="button" class="list-group-item text-nooverflow"
                                ng-repeat="modificator in modificatorList | orderBy:'name' | findByNameOrId:modificatorFilter:modificator.id:0 track by modificator.id"
                                ng-class="{active : modificator.id == curModificator.id}" ng-click="ctrl.loadModificator(modificator)">{{modificator.name}}</button>
                    </div>
                </div>
                <div class="col-md-10" ng-show="curModificator.id != null">
                    <div class="alert" role="alert" ng-class="{'OK':'alert-success', 'WARN':'alert-warning', 'ERROR':'alert-danger', 'ERROR2':'alert-danger', 'INFO':'alert-info', 'INFO2':'alert-info'}[status]">
                        Current status: <p ng-bind-html="statuses[status]"></p>
                    </div>
                    <form name="form_modificator">
                        <g:btpText id="modificator_name" label="Test Setting Name" ng-model="curModificator.name" placeholder="Modificator Name" angError="true"/>
                        <g:btpText id="modificator_input" label="Test Value" placeholder="Test Value" ng-model="testParams.input" ignore-dirty="true"/>
                        <g:btpText id="modificator_key" label="Test Key" placeholder="Test Key" ng-model="testParams.key" ignore-dirty="true"/>
                        <g:btpTextArea id="modificator_obj" label="Test Object" placeholder="Test Object" ng-model="testParams.obj" ui-codemirror="codemirrorOptions2" ignore-dirty="true"/>
                        <g:btpTextArea id="modificator_code" label="Code" ui-codemirror="codemirrorOptions" ng-model="curModificator.code"/>
                        
                        <div class="row">
                            <div class="col-md-4">
                                <button type="button" class="btn btn-default" ng-click="ctrl.tryTest()">Try Test</button>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-block" ng-click="ctrl.saveModificator()" ng-show="curModificator.id > 0">Update <b>Existing</b> modificator</button>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-block" ng-click="ctrl.saveModificator(true)">Save <b>New</b> modificator</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>