<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="menu"/>
    <title>${title}</title>
    <asset:javascript src="app/controllers/dataPromotionController.js"/>
</head>
<body>
<div class="container" ng-app="diffapp">
    <div ng-controller="dataPromotionController as dp">
        <div class="row main">
            <span id="adErrors" class="text-danger">{{adErrors}}</span>
            <div ng-show="htmlError != null && htmlError.length > 0" style="padding-bottom: 15px;">
                <span class="text-danger">Something went wrong. Below you can see error.</span>
                <br/>
                <button class="btn btn-warning" type="button" data-toggle="collapse" data-target="#htmlError" aria-expanded="false" aria-controls="html_error">Show error</button>
                <div id="htmlError" class="collapse">
                    <div class="well">
                        <span>{{htmlError}}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <h1>NCI data promotion</h1>
            <p>Follow next steps to find out if NCI data is ready for promotion to PROD</p>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingCada">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseCada" aria-expanded="false" aria-controls="collapseCada">Cada Tester</a>
                        </h4>
                    </div>
                    <div id="collapseCada" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingCada">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="alert" role="alert" ng-class="{'OK':'alert-success', 'BACKUP':'alert-success', 'WARN':'alert-warning', 'ERROR':'alert-danger', 'INFO':'alert-info', 'INFO2':'alert-info'}[cadaData.status]">
                                    Current status: {{statuses[cadaData.status]}}
                                </div>
                                <g:btpText id="cadaYearCodes" label="List of Year Codes to be checked" ng-model="cadaYearCodes" ng-init="cadaYearCodes='${params.cadaYearCodes}'"/>
                                <button id="startCheckCada" class="btn btn-default" ng-show="!isCadaRunning" ng-click="dp.checkCada()" autocomplete="off">Compare CADA to NCI DB</button>
                                <button id="checkCadaStatus" class="btn btn-default" ng-show="isCadaRunning || true" ng-click="dp.checkCadaStatus()" autocomplete="off">Is ready?</button>
                                <button id="backupRevCodes" class="btn btn-default" ng-click="dp.backupRevCodes()" autocomplete="off">Backup RevCodes</button>
                                <div class="progress" ng-show="isCadaRunning">
                                    <div class="progress-bar progress-bar-striped active" role="progressbar" style="width: {{cadaProgress}}%">
                                        <span>{{cadaProgress}}% Complete</span>
                                    </div>
                                </div>
                            </div>
                            <div ng-show="cadaData.model.length > 0">
                                
                                <table class="table table-condensed table-striped">
                                    <caption>Legend</caption>
                                    <thead>
                                        <th>Color</th>
                                        <th>Meaning</th>
                                    </thead>
                                    <tbody>
                                        <tr class="info">
                                            <td>Blue</td>
                                            <td>Has UNLOCKED Cada Revision, but locked data match in STaging and CADA</td>
                                        </tr>
                                        <tr class="success">
                                            <td>Green</td>
                                            <td>Current STaging RevCode matches with Cada RevCode, but this model has been updated since last release (for info/report only/ready for promotion)</td>
                                        </tr>
                                        <tr class="warning">
                                            <td>Yellow</td>
                                            <td>Current STaging RevCode matches with Cada RevCode, but some trims doesn't have pricing, so DTP cannot translate data for VT86 table</td>
                                        </tr>
                                        <tr class="danger">
                                            <td>Red</td>
                                            <td>Current STaging environment requires FORCE T&DTP for that RevCode</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table table-condensed table-striped">
                                    <caption>RevCodes data</caption>
                                    <thead>
                                    <tr>
                                        <th>Acode</th>
                                        <th>Model</th>
                                        <th>BACKUP<br/> RevCode</th>
                                        <th>Db<br/> RevCode</th>
                                        <th>Latest LOCKED<br/> Cada RevCode</th>
                                        <th>Is DELAYED<br/> Latest LOCKED</th>
                                        <th>All trims<br/> HAS price</th>
                                        <th>UNLOCKED<br/> Cada RevCode</th>
                                        <th>CadaViewer</th>
                                        <th>Cada Rev Data</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="row in cadaData.model" ng-class="row.dbRevCode != row.cadaRevCode ? 'danger' : (row.hasPrice != 'Y' ? 'warning' : (row.backRevCode != row.cadaRevCode ? 'success' : 'info'))" last-ready="dp.initPopovers()">
                                        <td>{{row.acode}}</td>
                                        <td>{{row.modelDesc2}}</td>
                                        <td>{{row.backRevCode}}</td>
                                        <td>{{row.dbRevCode}}</td>
                                        <td>{{row.cadaRevCode}}</td>
                                        <td>{{row.isDelayed}}</td>
                                        <td>{{row.hasPrice}}</td>
                                        <td>{{row.unlockedRevCode}}</td>
                                        <td><a href="http://cadaviewer.london.autodata.net/CADA.EXE/AcodeRev?DB=1&ACODE={{row.acode}}" target="_blank">CadaViewer</a></td>
                                        <td>
                                            <button class="btn btn-danger btn-xs" role="button" data-toggle="popover" data-trigger="focus" data-placement="left" data-html="true"
                                                    title="New Cada RevCodes for <b>{{row.modelDesc}}</b> (<i>{{row.acode}}</i>)"
                                                    data-content="{{dp.getCadaRevCodesTemplate(row.cadaNotes)}}"
                                            >
                                                All New Cada RevCodes
                                            </button>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingDb">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseDb" aria-expanded="false" aria-controls="collapseDb">DB Tester</a>
                        </h4>
                    </div>
                    <div id="collapseDb" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingDb">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="alert" role="alert" ng-class="{'OK':'alert-success', 'WARN':'alert-warning', 'ERROR':'alert-danger', 'INFO':'alert-info', 'INFO2':'alert-info'}[dbData.status]">
                                    Current status: {{statuses[dbData.status]}}
                                </div>
                                <g:btpText id="excludedMfgCode" label="Excluded MfgCode's for Incentives Table testing" ng-model="excludedMfgCode" ng-init="excludedMfgCode='${params.excludedMfgCodes}'"/>
                                <button id="startCheckDb" class="btn btn-default" ng-disabled="isDbRunning" ng-click="dp.checkDb()" autocomplete="off">Check DB miscellaneous issues</button>
                            </div>
                            <div ng-show="dbData.model.revCode.length > 0">
                                <label>Acodes that have different RevCode for EN and FR data</label>
                                <table class="table table-condensed table-striped">
                                    <thead>
                                    <tr>
                                        <th>Acode</th>
                                        <th>Model Code</th>
                                        <th>EN RevCode</th>
                                        <th>FR RevCode</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="row in dbData.model.revCode">
                                        <td>{{row.Acode}}</td>
                                        <td>{{row.modelCode}}</td>
                                        <td>{{row.enRevCode}}</td>
                                        <td>{{row.frRevCode}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div ng-show="dbData.model.ei02.length > 0">
                                <label>Acodes that doesn't have records in 'EI02_ExtraVehicleData' table</label>
                                <table class="table table-condensed table-striped">
                                    <thead>
                                    <tr>
                                        <th>Acode</th>
                                        <th>Model Code</th>
                                        <th>Language</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="row in dbData.model.ei02">
                                        <td>{{row.Acode}}</td>
                                        <td>{{row.modelCode}}</td>
                                        <td>{{row.lngCode}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div ng-show="dbData.model.vt86.length > 0">
                                <label>Acodes that doesn't have records in 'VT86_AcodeSegments' table</label>
                                <table class="table table-condensed table-striped">
                                    <thead>
                                    <tr>
                                        <th>Acode</th>
                                        <th>Model Code</th>
                                        <th>Language</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="row in dbData.model.vt86">
                                        <td>{{row.Acode}}</td>
                                        <td>{{row.modelCode}}</td>
                                        <td>{{row.lngCode}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div ng-show="dbData.model.incentives.length > 0">
                                <label>MFGCODES that has Incentives but doesn't exist in DB</label>
                                <table class="table table-condensed table-striped">
                                    <thead>
                                    <tr>
                                        <th>Manufacturer Code</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr ng-repeat="row in dbData.model.incentives">
                                        <td>{{row.mfgCode}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingHiddenVehicles">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseHiddenVehicles" aria-expanded="false" aria-controls="collapseHiddenVehicles">Hidden Vehicles Tester</a>
                        </h4>
                    </div>
                    <div id="collapseHiddenVehicles" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingHiddenVehicles">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="alert" role="alert" ng-class="{'OK':'alert-success', 'WARN':'alert-warning', 'ERROR':'alert-danger', 'INFO':'alert-info', 'INFO2':'alert-info'}[hiddenData.status]">
                                    Current status: {{statuses[hiddenData.status]}}
                                </div>
                                <g:btpText id="excludedAcode" label="Acodes that are expected to be hidden" ng-model="excludedAcode"  ng-init="excludedAcode='${params.excludedAcodes}'"/>
                                <button id="startCheckHidden" class="btn btn-default" ng-disabled="isHiddenRunning" ng-click="dp.checkHidden()" autocomplete="off">Check Hidden Vehicles</button>
                                <div ng-show="hiddenData.model.length > 0">
                                    <table class="table table-condensed table-striped">
                                        <thead>
                                        <tr>
                                            <th>Acode</th>
                                            <th>Model Code</th>
                                            <th>Language</th>
                                            <th>Rev Code</th>
                                            <th>Is Enabled</th>
                                            <th>Is Delayed</th>
                                            <th>MSRP</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr ng-repeat="row in hiddenData.model">
                                            <td>{{row.Acode}}</td>
                                            <td>{{row.modelCode}}</td>
                                            <td>{{row.lngCode}}</td>
                                            <td>{{row.revCode}}</td>
                                            <td>{{row.isEnabled}}</td>
                                            <td>{{row.isDelayed}}</td>
                                            <td>{{row.msrp}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <g:btpSelectBox id="enableAcodesSelect" multiple="true" label="Select Acodes to enable" angularOptionsArray="hiddenData.list" ng-model="enableAcodesList"/>
                                    <button id="enableAcodes" class="btn btn-default" ng-click="dp.enableAcodes()" autocomplete="off" ng-disabled="isHiddenRunning">Enable Acodes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingSpecialPackages">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSpecialPackages" aria-expanded="false" aria-controls="collapseSpecialPackages">Special Packages Tester</a>
                        </h4>
                    </div>
                    <div id="collapseSpecialPackages" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSpecialPackages">
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="alert" role="alert" ng-class="{'OK':'alert-success', 'WARN':'alert-warning', 'ERROR':'alert-danger', 'INFO':'alert-info', 'INFO2':'alert-info'}[specialPackagesData.status]">
                                    Current status: {{statuses[specialPackagesData.status]}}
                                </div>
                                <button id="startCheckSpecialPackages" class="btn btn-default" ng-disabled="isSpecialPackagesRunning" ng-click="dp.checkSpecialPackages()" autocomplete="off">Check Special Packages</button>
                                <div ng-show="specialPackagesData.model.new.length > 0">
                                    <h3>Options that could be Special Packages (This is not necessarily true! Add packages only when you sure!)</h3>
                                    <table class="table table-condensed table-striped">
                                        <thead>
                                        <tr>
                                            <th>Acode</th>
                                            <th>Option Code</th>
                                            <th>Description</th>
                                            <th>MSRP</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr ng-repeat="row in specialPackagesData.model.new">
                                            <td>{{row.Acode}}</td>
                                            <td>{{row.OptCode}}</td>
                                            <td>{{row.Description}}</td>
                                            <td>{{row.MSRP}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <g:btpSelectBox id="enableSpecialPackagesSelect" multiple="true" label="Select Acode-OptCode pairs to add to Special Packages table" 
                                                    angularOptions="specialPackagesData.list.new" ng-model="enableSpecialPackagesList" angError="true"/>
                                    <button id="enableSpecialPackages" class="btn btn-default" ng-click="dp.enableSpecialPackages()" autocomplete="off" ng-disabled="isSpecialPackagesRunning">Enable Special Packages</button>
                                </div>
                                <div ng-show="specialPackagesData.model.red.length > 0">
                                    <h3>Special Packages that could be redundant (This is not necessarily true! Delete packages only when you sure!)</h3>
                                    <table class="table table-condensed table-striped">
                                        <thead>
                                        <tr>
                                            <th>Acode</th>
                                            <th>Option Code</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr ng-repeat="row in specialPackagesData.model.red">
                                            <td>{{row.acode}}</td>
                                            <td>{{row.OptCode}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <g:btpSelectBox id="disableSpecialPackagesSelect" multiple="true" label="Select Acode-OptCode pairs to remove from Special Packages table"
                                                    angularOptions="specialPackagesData.list.red" ng-model="disableSpecialPackagesList" angError="true"/>
                                    <button id="disableSpecialPackages" class="btn btn-default" ng-click="dp.disableSpecialPackages()" autocomplete="off" ng-disabled="isSpecialPackagesRunning">Disable Special Packages</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTotal">
                        <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTotal" aria-expanded="false" aria-controls="collapseTotal">Total Tester</a>
                        </h4>
                    </div>
                    <div id="collapseTotal" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTotal">
                        <div class="panel-body">
                            <a href="${grailsApplication.config.custom.contextPath}/comboTest/index/7" target="_blank">Proceed to link and run Test Case.</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>