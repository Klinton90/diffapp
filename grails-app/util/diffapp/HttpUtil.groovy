package diffapp

import groovy.util.logging.Slf4j
import groovy.util.slurpersupport.NodeChild
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.URIBuilder

@Slf4j
class HttpUtil{
    
    public static NodeChild getWsResponse(String url, String pathPattern, String webServiceName, String method, String format, Map<String, Object> wsParam){
        NodeChild result;
        
        String path = webServiceName + parsePattern(pathPattern, wsParam);
        URIBuilder uri = new URIBuilder(url+path);
        
        try{
            HTTPBuilder httpService = new HTTPBuilder("http://${uri.host}${uri.port > 0 ? ':'+uri.port : ''}/");
            if(method == "GET"){
                uri.addQueryParams(wsParam);
                result = httpService.get(path: uri.path, contentType: ContentType[format], query: uri.query);
            }else{
                result = httpService.post(path: uri.path, contentType: ContentType[format], body: wsParam, query: uri.query);
            }
        }catch(Exception e){
            throw new CustomException("<span>Cannot connect to: <a href='${uri.toString()}'>${uri.toString()}</a></span>")
        }

        return result;
    }

    public static String parsePattern(String pathPattern, Map<String, Object> structure){
        structure.each{String name, Object val ->
            pathPattern = pathPattern.replace('$' + name, (val != null ? val.toString() : ""))
        };
        return pathPattern;
    }
}
