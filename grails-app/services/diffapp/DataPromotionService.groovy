package diffapp

import com.silly_diff.Helpers.MapDiffHelper
import com.silly_diff.Util.SqlUtil
import grails.core.GrailsApplication
import groovy.json.JsonSlurper
import groovy.sql.GroovyRowResult;
import groovy.sql.Sql
import groovy.util.logging.Slf4j;
import groovy.util.slurpersupport.NodeChild;
import groovyx.net.http.HTTPBuilder

import static groovyx.net.http.ContentType.HTML;

@Slf4j
class DataPromotionService{

    GrailsApplication grailsApplication
    
    protected static Sql sql;
    
    public DataPromotionService(){
        
    }
    
    protected synchronized Sql _getSql(){
        if(sql == null){
            sql = Sql.newInstance(
                url: grailsApplication.config.getProperty('custom.sqlConnections.CA_ST.url'),
                user: grailsApplication.config.getProperty('custom.sqlConnections.CA_ST.login'),
                password: grailsApplication.config.getProperty('custom.sqlConnections.CA_ST.pass'),
                driver: grailsApplication.config.getProperty('custom.sqlConnections.CA_ST.driver')
            );
        }
        return sql;
    }

    protected static Sql sql2;

    protected synchronized Sql _getSql2(){
        if(sql2 == null){
            sql2 = Sql.newInstance(
                url: grailsApplication.config.getProperty('custom.sqlConnections.TEST_DATA.url'),
                user: grailsApplication.config.getProperty('custom.sqlConnections.TEST_DATA.login'),
                password: grailsApplication.config.getProperty('custom.sqlConnections.TEST_DATA.pass'),
                driver: grailsApplication.config.getProperty('custom.sqlConnections.TEST_DATA.driver')
            );
        }
        return sql2;
    }

    public List<HashMap> checkCada(Map<String, Object> params, Map<String, Integer> counter){
        List<HashMap> diff = new ArrayList<HashMap>();
        ArrayList<GroovyRowResult> latestRevisionsDb = new ArrayList<GroovyRowResult>();
        ArrayList<Acode> latestRevisionsBackup = new ArrayList<Acode>();

        HTTPBuilder http = new HTTPBuilder(grailsApplication.config.getProperty('custom.cadaAddress'));
        ArrayList<HashMap<String,String>> models = new ArrayList<HashMap<String, String>>();
        params.cadaYearCodes.each{String year ->
            latestRevisionsDb.addAll(_getMaxRevCodes(year));
            Acode.withNewSession{
                latestRevisionsBackup.addAll(Acode.findAllByIdLike("CA${year}%"));
            }

            List<String> divisions = ["IN", "NI"];
            divisions.each{String division->
                HashMap<String, String> structure = [
                    DB:1,
                    YR:year,
                    CNTRY:'CA',
                    DIV:division,
                    walk:'Go%21%21%21'
                ];
                NodeChild optionsXml = http.get(contentType: HTML, query: structure);
                def mods = optionsXml.'**'.find{NodeChild node->
                    return node.@name == "MDL";
                };
                if(mods != null){
                    mods.children().each{NodeChild option ->
                        String acodeSuffix = option.@value.toString();
                        if(acodeSuffix != "__"){
                            structure.MDL = acodeSuffix;
                            structure.modelDesc = option.localText()[0];
                            models.add(structure.clone());
                        }
                    };
                }
            };
        };

        counter["max"] = models.size();

        models.each{HashMap<String, String> structure->
            NodeChild acodesXml = http.get(contentType: HTML, query: structure);
            acodesXml.'**'.findAll{NodeChild node->
                return node.@bgcolor == '#AAAAAA';
            }.each{NodeChild acode ->
                String acodeVal = acode.children().list()[0].text();
                
                Map<String, String> structure2 = [
                    DB:'1',
                    ACODE:acodeVal
                ];
                NodeChild revCodesXml = http.get(path: 'CADA.EXE/AcodeRev', contentType: HTML, query: structure2);

                def rows = revCodesXml.'**'.findAll{NodeChild node->
                    return node.@valign == "TOP";
                }[0].'**'.findAll{NodeChild node->
                    return node.name() == "TR" && node.@bgcolor != null && node.@bgcolor.size() > 0;
                };
                Map<String, String> revData = [
                    PreAcode: acodeVal,
                    UnlockedRevCode: "0"
                ];

                HashMap<String, String> dbRev = latestRevisionsDb.find{GroovyRowResult _dbRev->
                    return _dbRev.PreAcode == acodeVal;
                };
                Acode backRev = latestRevisionsBackup.find{Acode _backRev->
                    return _backRev.id == acodeVal;
                }

                HashMap<String, Object> tmpDiff = [
                    acode: acodeVal,
                    modelDesc: dbRev != null ? dbRev.ModelDesc : '',
                    modelDesc2: structure["modelDesc"],
                    dbRevCode: dbRev != null ? dbRev.RevCode : 0,
                    backRevCode: backRev != null ? backRev.revCode : 0,
                    cadaNotes: new ArrayList<HashMap<String, String>>(), 
                    unlockedRevCode: 0,
                    cadaRevCode: dbRev != null ? dbRev.RevCode : 0,
                    isDelayed: 'N',
                    hasPrice: _hasPrice(acodesXml, acodeVal) ? "Y" : "N"
                ];
                
                for(int i = 0; i < rows.size(); i++){
                    Map<String, String> cadaRev =_getRevData(rows[i]);
                    float cadaRevCode = Float.parseFloat(cadaRev.RevCode);
                    if(backRev == null || dbRev == null || cadaRevCode > dbRev.RevCode.toFloat() || cadaRevCode > backRev.revCode){
                        tmpDiff.cadaNotes.add(cadaRev);
                        if(i == 0){
                            tmpDiff.isDelayed = cadaRev.isDelayed;
                        }
                        if(cadaRev.RevLocked.size() == 1){
                            tmpDiff.unlockedRevCode = cadaRevCode;
                        }else if(cadaRevCode > tmpDiff.cadaRevCode.toFloat()){
                            tmpDiff.cadaRevCode = cadaRevCode;
                            tmpDiff.isDelayed = cadaRev.isDelayed
                        }
                    }else{
                        break;
                    }
                }
                if(tmpDiff.cadaNotes.size() > 0 || tmpDiff.hasPrice != "Y"){
                    diff.add(tmpDiff);
                }

                log.info(acodeVal + " " + (tmpDiff.dbRevCode == tmpDiff.cadaRevCode && tmpDiff.hasPrice == "Y").toString());
            };
            counter["cur"]++;
        };

        return diff;
    }
    
    protected static Boolean _hasPrice(NodeChild xml, String acodeVal){
        Boolean result = true;
        List<NodeChild> rows = xml.'**'.findAll{NodeChild node ->
            return node.@bgcolor == '#AAAAAA' || node.@bgcolor == '#EEEEEE';
        };
        int startRow = rows.findIndexOf{NodeChild row ->
            return acodeVal == row.children().list()[0].text();
        }
        for(int i = startRow + 1; i < rows.size(); i++){
            NodeChild row = rows[i];
            if(row.@bgcolor == '#EEEEEE'){
                if(row.children().list()[4].text().size() <= 1){
                    result = false;
                    break;
                }
            }else{
                break;
            }
        }
        
        return result;
    }

    public void backupRevCodes(Map<String, Object> params){
        params.cadaYearCodes.each{String year ->
            _getMaxRevCodes(year).each{GroovyRowResult _acode -> 
                Acode acode = Acode.findById(_acode.PreAcode);
                if(acode == null){
                    new Acode(id: _acode.PreAcode, revCode: _acode.RevCode).save();
                }else{
                    acode.revCode = _acode.RevCode;
                    acode.save();
                }
            }
        }
    }

    protected List<GroovyRowResult> _getMaxRevCodes(String year = null){
        return SqlUtil.executeFile(_getSql(), 'sql/DataPromotion/getMaxRevCodes.sql', [year: year]);
    }
    
    protected static Map<String, String> _getRevData(NodeChild row){
        return [
            RevCode : row.TD[0].text(),
            RevLocked : row.TD[3].text(),
            RevNote : row.TD[5].text(),
            isDelayed : row.TD[7].text().size() > 1 ? 'Y' : 'N'
        ];
    }
    
    public Map<String, List<GroovyRowResult>> checkDb(Map<String, Object> params){
        return [
            "revCode" : SqlUtil.executeFile(_getSql(), 'sql/DataPromotion/revCodeInconsistencies.sql'),
            "ei02" : SqlUtil.executeFile(_getSql(), 'sql/DataPromotion/checkAcodeEI02.sql'),
            "vt86" : SqlUtil.executeFile(_getSql(), 'sql/DataPromotion/checkAcodeVT86.sql'),
            "incentives" : SqlUtil.executeFile(_getSql(), 'sql/DataPromotion/checkAcodeIncentives.sql', params)
        ];
    }
    
    public List<GroovyRowResult> checkHidden(Map<String, Object> params){
        return SqlUtil.executeFile(_getSql(), 'sql/DataPromotion/disabledVehicles.sql', params);
    }
    
    public Boolean validateYears(Map<String, Object> params){
        Boolean result = true;
        
        ArrayList<String> dbYears = new ArrayList<String>();
        SqlUtil.execute(_getSql(), "SELECT * FROM VT08_Year").each{row->
            dbYears.add(row.YearCode);
        }
        for(int i = 0; i < params.cadaYearCodes.size(); i++){
            if(!dbYears.contains(params.cadaYearCodes[i])){
                result = false;
                break;
            }
        }
        
        return result;
    }
    
    public Boolean validateAcodes(Map<String, Object> params){
        Boolean result = true;
        
        ArrayList<String> hiddenAcodes = new ArrayList<String>();
        checkHidden(params).each{GroovyRowResult row->
            hiddenAcodes.add(row.Acode);
        };
        for(int i = 0; i < params.enableAcodesList.size(); i++){
            if(!hiddenAcodes.contains(params.enableAcodesList[i])){
                result = false;
                break;
            }
        }
        
        return result;
    }
    
    public void enableAcodes(Map<String, List<String>> params){
        String query = """
            UPDATE VT05_Trim
            SET DISPLAY_VEHICLE='Y'
            WHERE Acode IN (:in_enableAcodesList);
        """;
        
        Map<String,String> inParams = SqlUtil.getInParams(params);
        String _query = SqlUtil.applyInVars(inParams, query);
        _getSql().execute(_query);
        log.info("Trims visibility set. Start Truncate Variant...");
        _getSql().execute("TRUNCATE VARIANT");
        log.info("Complete. Start Populate Variant...");
        _getSql().execute("CALL POPULATE_VARIANT()");
        log.info("Complete. Start Truncate Filter...");
        _getSql().execute("TRUNCATE FILTER");
        log.info("Complete. Start Populate Filter...");
        _getSql().execute("CALL POPULATE_FILTER()");
        log.info("Complete.");
    }

    public static Map<String, String> getDefaultParams(){
        HashMap<String, String> result = new HashMap<String, String> ();
        [
            "cadaYearCodes",
            "excludedMfgCodes",
            "excludedAcodes"
        ].each{String paramName->
            result.put(paramName, OtherParam.findByParamType(paramName)?.value);
        }

        return result;
    }
    
    public Map<String, List<GroovyRowResult>> checkSpecialPackages(){
        return [
            "newSpecialPackages" : SqlUtil.executeFile(_getSql(), 'sql/DataPromotion/verifySpecialPackages.sql'),
            "redundantSpecialPackages" : SqlUtil.executeFile(_getSql(), 'sql/DataPromotion/verifySpecialPackagesRed.sql'),
        ];
    }
    
    public ArrayList<Map<String, String>> prepareSpecialPackagesPairs(List<Object> spList){
        ArrayList<Map<String, String>> pairs = new ArrayList<Map<String, String>>();

        for(int i = 0; i < spList.size(); i++){
            JsonSlurper js = new JsonSlurper();
            pairs.add(js.parseText([spList[i]].id));
        }
        
        return pairs;
    }
    
    public Boolean validateSpecialPackages(ArrayList<Map<String, String>> pairs, Boolean red = false){
        Boolean result = true;

        Map<String, List<GroovyRowResult>> specialPackages = checkSpecialPackages();
        for(int i = 0; i < pairs.size(); i++){
            List<GroovyRowResult> spList = red ? specialPackages["redundantSpecialPackages"] : specialPackages["newSpecialPackages"];
            def match = spList.find{GroovyRowResult sp ->
                return sp.Acode == pairs[i].Acode && sp.OptCode == pairs[i].OptCode;
            };
            if(!match){
                result = false;
            }
        }
        
        return result;
    }

    public void enableSpecialPackages(ArrayList<Map<String, String>> pairs){
        String query = "INSERT INTO SPECIAL_PACKAGES VALUES ";
        String _query = "";
        
        pairs.each{Map<String, String> pair->
            _query += (_query.size() > 0 ? "," : "") +  "('${pair["Acode"]}','${pair["OptCode"]}')";
        };
        
        log.info(query + _query);
        
        _getSql().execute(query + _query);
    }

    public void disableSpecialPackages(ArrayList<Map<String, String>> pairs){
        String query = "DELETE FROM SPECIAL_PACKAGES WHERE";
        String _query = "";

        pairs.each{Map<String, String> pair->
            _query += (_query.size() > 0 ? " OR " : "") +  "(Acode = '${pair["Acode"]}' AND OptCode = '${pair["OptCode"]}')";
        };

        log.info(query + _query);

        _getSql().execute(query + _query);
    }
}