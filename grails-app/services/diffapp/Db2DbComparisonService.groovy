package diffapp

import com.silly_diff.Helpers.MapDiffHelper
import com.silly_diff.Infostructure.AbstractDiffHelper
import com.silly_diff.Util.SqlUtil
import grails.transaction.Transactional
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import groovy.util.logging.Slf4j

@Slf4j
@Transactional
class Db2DbComparisonService{

    public static QService queue = new QService("db2DbComparison");

    public runTest(Db2DbParam db2DbParam){
        HashMap<String, Object> res = _initTest(db2DbParam);
        AbstractDiffHelper diffHelper = (AbstractDiffHelper)res["diffHelper"];
        diffHelper.calcDiff();

        return [
            passed: diffHelper.isSimilar(),
            original1: AbstractDiffHelper.getOutputListAsString(res["original1"]),
            original2: AbstractDiffHelper.getOutputListAsString(res["original2"]),
            out1: AbstractDiffHelper.getOutputListAsString(diffHelper.outputList1),
            out2: AbstractDiffHelper.getOutputListAsString(diffHelper.outputList2),
            prep1: AbstractDiffHelper.getOutputListAsString(diffHelper.source1),
            prep2: AbstractDiffHelper.getOutputListAsString(diffHelper.source2)
        ];
    }
    
    public startTest(int id){
        QService.Q q = queue.getQ(id);
        if(q.isRunning){
            q.error = "Test is already Running!";
        }else{
            Db2DbParam testParam = Db2DbParam.getFullTree(id);
            if(testParam == null){
                q.error = "Test not found!";
            }else{
                q = new QService.Q();
                queue.setQ(id, q);
                HashMap<String, Object> res = _initTest(testParam);
                q.diffHelper = (AbstractDiffHelper)res["diffHelper"];
                q.original1 = res["original1"];
                q.original2 = res["original2"];

                Thread.start{
                    log.info("Start Db2Db for '${testParam.name}':${testParam.id}");
                    Csv2DbParam.withNewSession{session ->
                        try{
                            q.diffHelper.calcDiff();
                            log.info("Complete Db2Db for '${testParam.name}':${testParam.id}");
                        }catch(Exception e){
                            q.error = e.message;
                            log.info("Error: '${e.message}");
                        }finally{
                            q.isRunning = false;
                        }
                    }
                };
            }
        }

        return q;
    }

    protected HashMap<String, Object> _initTest(Db2DbParam db2DbParam){
        String query1 = db2DbParam.query1;
        Sql sql1 = UtilService.createSqlConnection(db2DbParam.dbSetting1);
        List<GroovyRowResult> dbRows1 = SqlUtil.execute(sql1, query1);

        String query2 = db2DbParam.query2;
        query2 = query2.size() > 0 ? query2 : query1
        Sql sql2 = UtilService.createSqlConnection(db2DbParam.dbSetting2);
        List<GroovyRowResult> dbRows2 = SqlUtil.execute(sql2, query2);

        MapDiffHelper diffHelper = new MapDiffHelper(dbRows1, dbRows2);
        HashMap<String, Object> ignoredElements = UtilService.parseIgnoredElements(db2DbParam.ignoredElements);
        diffHelper.setupFromConfig([
            "skipMissedDb": false,
            "orderlySafeMode": db2DbParam.orderlySafeMode,
            "ignoredPairs": ignoredElements["ignore"],
            "ignoredPairsWValues": ignoredElements["ignoreValues"]
        ]);

        return [
            "diffHelper": diffHelper,
            "original1": dbRows1,
            "original2": dbRows2
        ];
    }
}
