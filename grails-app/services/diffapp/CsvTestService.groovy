package diffapp

import com.silly_diff.Helpers.MapDiffHelper
import com.silly_diff.Infostructure.AbstractDiffHelper
import com.xlson.groovycsv.CsvParser
import com.silly_diff.Util.SqlUtil
import com.xlson.groovycsv.PropertyMapper
import grails.core.GrailsApplication
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import groovy.util.logging.Slf4j
import org.springframework.web.multipart.MultipartFile

import java.nio.file.Files

@Slf4j
class CsvTestService{

    GrailsApplication grailsApplication;
    
    protected final static String IGNORED_PREFIX = "_ignored_";

    protected static File _outFolder;

    public static QService queue = new QService("csv2DbComparison");

    public CsvTestService(){
    }

    protected synchronized File _getOutFolder(){
        if(_outFolder == null){
            _outFolder = new File("${grailsApplication.config.getProperty('custom.outputFolder')}/${grailsApplication.config.getProperty('custom.csv.subFolder')}");
            Files.createDirectories(_outFolder.toPath());
        }
        return _outFolder;
    }

    public Map<String, Object> runTest(Csv2DbParam csv2DbParam, File feed){
        /*Sql sql = SqlUtil.createSqlConn([
            "DbUrl" : csv2DbParam.dbSetting.dbDriver.driverUrlPrefix + csv2DbParam.dbSetting.url,
            "DbUser" : csv2DbParam.dbSetting.login,
            "DbPass" : csv2DbParam.dbSetting.password,
            "DbDriver" : csv2DbParam.dbSetting.dbDriver.driverClass
        ]);
        List<GroovyRowResult> dbRows = SqlUtil.execute(sql, csv2DbParam.query);

        ArrayList<CsvColumn> duplicates = new ArrayList<>();
        List<String> columnNames = _prepareColumns(csv2DbParam.csvSetting.csvColumn, duplicates);

        List<Map<String, Object>> feedRows = new ArrayList<>();
        new CsvParser().parse(
            feed.text,
            separator :  csv2DbParam.csvSetting.splitChar,
            quoteChar : csv2DbParam.csvSetting.quoteChar,
            readFirstLine : !csv2DbParam.csvSetting.skipFirstLine,
            columnNames : columnNames
        ).each{PropertyMapper line ->
            feedRows.add(line.toMap());
            duplicates.each{CsvColumn dup ->
                Map<String, Object> entry = line.toMap()
                entry[dup.name] = entry[IGNORED_PREFIX + dup.position];
                feedRows.add(entry);
            };
        };

        MapDiffHelper diffHelper = new MapDiffHelper(dbRows, _postRowProcessing(csv2DbParam.csvSetting.csvColumn, feedRows));
        diffHelper.setupFromConfig(_prepareConfig(csv2DbParam, columnNames));*/
        HashMap<String, Object> res = _initTest(csv2DbParam, feed);
        AbstractDiffHelper diffHelper = (AbstractDiffHelper)res["diffHelper"];
        diffHelper.calcDiff();

        return [
            passed: diffHelper.isSimilar(),
            original1: AbstractDiffHelper.getOutputListAsString(res["original1"]),
            original2: AbstractDiffHelper.getOutputListAsString(res["original2"]),
            out1: AbstractDiffHelper.getOutputListAsString(diffHelper.outputList1),
            out2: AbstractDiffHelper.getOutputListAsString(diffHelper.outputList2),
            prep1: AbstractDiffHelper.getOutputListAsString(diffHelper.source1),
            prep2: AbstractDiffHelper.getOutputListAsString(diffHelper.source2)
        ];
    }

    public QService.Q startTestCsv(Integer id, File feed){
        QService.Q q = queue.getQ(id);
        if(q.isRunning){
            q.error = "Test is already Running!";
        }else{
            Csv2DbParam csv2DbParam = Csv2DbParam.getFullTree(id);
            if(csv2DbParam == null){
                q.error = "Test not found!";
            }else{
                q = new QService.Q();
                queue.setQ(id, q);
                HashMap<String, Object> res = _initTest(csv2DbParam, feed);
                q.diffHelper = (AbstractDiffHelper)res["diffHelper"];
                q.original1 = res["original1"];
                q.original2 = res["original2"];

                Thread.start{
                    log.info("Start");
                    Csv2DbParam.withNewSession{session ->
                        try{
                            q.diffHelper.calcDiff();
                            log.info("Complete");
                        }catch(Exception e){
                            q.error = e.message;
                            log.info("Error: '${e.message}");
                        }finally{
                            q.isRunning = false;
                        }
                    }
                };
            }
        }
        
        return q;
    }

    public HashMap<String, Object> _initTest(Csv2DbParam csv2DbParam, File feed){
        Sql sql = UtilService.createSqlConnection(csv2DbParam.dbSetting);
        List<GroovyRowResult> dbRows = SqlUtil.execute(sql, csv2DbParam.query);

        ArrayList<Integer> duplicates = new ArrayList<>();
        List<String> columnNames = new ArrayList<>();
        int curPosition = 0;
        csv2DbParam.csvSetting.csvColumn.sort{CsvColumn col ->
            return col.position;
        }.each{CsvColumn col ->
            while(curPosition < col.position - 1){
                curPosition++;
                columnNames.add(IGNORED_PREFIX + curPosition);
            }
            if(columnNames.indexOf(col.name) > -1){
                duplicates.add(col.position);
                columnNames.add(IGNORED_PREFIX + col.position);
            }else{
                columnNames.add(col.name);
            }
            curPosition++;
        };
        
        List<Map<String, Object>> feedRows = new ArrayList<>();
        new CsvParser().parse(
            feed.text,
            separator :  csv2DbParam.csvSetting.splitChar,
            quoteChar : csv2DbParam.csvSetting.quoteChar,
            readFirstLine : !csv2DbParam.csvSetting.skipFirstLine,
            columnNames : columnNames
        ).each{PropertyMapper line ->
            feedRows.add(line.toMap());
        };
        
        Set<String> existingCols = feedRows[0].keySet();
        List<String> diffCols = columnNames - existingCols;
        diffCols = diffCols.findAll{String col->
            return !col.startsWith(IGNORED_PREFIX);
        };
        feedRows.each{Map<String, Object> row->
            diffCols.each{String col ->
                row.put(col, "");
            };
        };
        
        MapDiffHelper diffHelper = new MapDiffHelper(dbRows, _postRowProcessing(csv2DbParam.csvSetting.csvColumn, duplicates, feedRows));
        diffHelper.setupFromConfig(_prepareConfig(csv2DbParam, columnNames));
        
        return [
            "diffHelper": diffHelper,
            "original1": dbRows,
            "original2": feedRows
        ];
    }
    
    protected static ArrayList<String> _prepareColumns(Collection<CsvColumn> columns, ArrayList<CsvColumn> duplicates){
        ArrayList<String> result = new ArrayList<>();
        int curPosition = 0;
        
        columns.sort{CsvColumn col ->
            return col.position;
        }.each{CsvColumn col ->
            while(curPosition < col.position - 1){
                curPosition++;
                result.add(IGNORED_PREFIX + curPosition);
            }
            if(result.indexOf(col.name) > -1){
                duplicates.add(col);
                result.add(IGNORED_PREFIX + col.position);
            }else{
                result.add(col.name);
            }
            curPosition++;
        };
        
        return result;
    }
    
    protected static HashMap<String, Object> _prepareConfig(Csv2DbParam csv2DbParam, List<String> columnNames){
        HashMap<String, String> invs = new HashMap<String, String>();
        csv2DbParam.csvSetting.csvColumn.each{CsvColumn col->
            if(col.ignored.size() > 0){
                invs.put(col.name, col.ignored);
            }
        };
        
        return [
            "ignoredPairs": columnNames.findAll{String colName -> return colName.startsWith(IGNORED_PREFIX)},
            "ignoredPairsWValues": invs,
            "ignoredRows": _prepareIgnoredRows(csv2DbParam.ignoredRow.toList()),
            "orderlySafeMode": csv2DbParam.orderlySafeMode,
            "showErrors": false,
            "modifications2": _prepareModificators(csv2DbParam.csvSetting.csvColumn.toList()),
        ];
    }
    
    protected static HashMap<String, Object> _prepareModificators(List<CsvColumn> columns){
        HashMap<String, Object> result = new HashMap<>();
        
        columns.each{CsvColumn col ->
            if(col.modificator != null && col.modificator.id > 0){
                result.put(col.name, col.modificator.code);
            }
        };
        
        return result;
    }
    
    protected static HashMap<String, Object> _prepareIgnoredRows(List<IgnoredRow> ignoredRows){
        HashMap<String, Object> result = new HashMap<>();
        
        ignoredRows.findAll{IgnoredRow ir ->
            return ir.type == "REGULAR";
        }.each{IgnoredRow ir -> 
            List<String> chain = _getNamingChain(ir);
            HashMap<String, Object> tmp = result;
            for(int i = chain.size() - 1; i > 0; i--){
                tmp = (HashMap<String, Object>)tmp.get(chain[i], new HashMap<String, Object>());
            }
            tmp.put(ir.name, ir.value);
        };
        
        return result;
    }
    
    protected static List<String> _getNamingChain(IgnoredRow ignoredRow, List<String> chain = new ArrayList<>()){
        chain.add(ignoredRow.name);
        
        if(ignoredRow.parentRow != null){
            chain = _getNamingChain(ignoredRow.parentRow, chain);
        }
        
        return chain;
    }
    
    protected static List<Map<String, Object>> _postRowProcessing(Collection<CsvColumn> columns, List<Integer> duplicates, List<Map<String, Object>> _lines){
        List<Map<String, Object>> lines = _lines;
        
        // Create duplicate FeedRows from Columns with duplicated name
        duplicates.each{Integer pos ->
            CsvColumn duplicate = columns.find{CsvColumn col -> 
                return col.position == pos;
            };
            ArrayList<Map<String, Object>> tmp = new ArrayList<>();
            lines.each{Map<String, Object> line ->
                tmp.add(line);
                HashMap<String, Object> shallowCopy = new HashMap<>(line);
                shallowCopy[duplicate.name] = shallowCopy[IGNORED_PREFIX + duplicate.position];
                tmp.add(shallowCopy);
            };
            lines = tmp;
        };
        
        // Create duplicate FeedRows from Columns with SubSplitChar
        columns.findAll{CsvColumn col -> 
            return col.splitChar.size() > 0
        }.each{CsvColumn col->
            ArrayList<Map<String, Object>> tmp = new ArrayList<>();
            lines.each{Map<String, Object> line ->
                String key = col.name;
                if(line.containsKey(key)){
                    line.get(key).toString().split(col.splitChar).each{String subValue ->
                        HashMap<String, Object> shallowCopy = new HashMap<>(line);
                        shallowCopy[key] = subValue;
                        tmp.add(shallowCopy);
                    };
                }
            };
            lines = tmp;
        };
        return lines;
    }

    public File moveUploadedFile(MultipartFile file){
        File target = new File("${_getOutFolder().getAbsolutePath()}/${file.getOriginalFilename()}");
        file.transferTo(target);
        return target;
    }
}
