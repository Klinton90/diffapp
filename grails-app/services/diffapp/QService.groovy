package diffapp

import com.silly_diff.Infostructure.AbstractDiffHelper

//@Transactional
class QService{
    
    public String repoName;

    public HashMap<Integer, Q> queue = new HashMap<>();
    
    public QService(String name){
        repoName = name;
    }
    
    public void setQ(int pos, Q q){
        queue[pos] = q;
    }

    public QService.Q getQ(int id){
        QService.Q q = queue[id];
        if(q == null){
            q = new Q();
            q.error = "Test not found!";
            q.isRunning = false;
        } else if(!q.isRunning){
            queue.remove(id);
        }
        return q;
    }

    public void removeQ(int id){
        queue.remove(id);
    }

    public class Q{
        public Boolean isRunning = true;
        public String error = "";
        public AbstractDiffHelper diffHelper;
        protected List<Object> original1;
        protected List<Object> original2;

        public Q(){

        }
        
        public HashMap<String, Object> getStatus(){
            HashMap<String, String> result = [
                isRunning: isRunning,
                total: 0,
                counter: 0
            ];

            if(diffHelper != null){
                result["total"] = diffHelper.total.toString();
                result["counter"] =  diffHelper.counter.toString();
                result["passed"] = isRunning ? false.toString() : diffHelper.isSimilar().toString();
            }
            if(error.size() > 0){
                result["error"] = error;
            }
            return result;
        }

        public HashMap<String, Object> getFullResults(){
            HashMap<String, Object> result = getStatus();

            if(!result.containsKey("error") && !isRunning){
                result += [
                    original1: AbstractDiffHelper.getOutputListAsString(original1),
                    original2: AbstractDiffHelper.getOutputListAsString(original2),
                    totalOriginal1: original1.size().toString(),
                    totalOriginal2: original2.size().toString(),
                    prep1: AbstractDiffHelper.getOutputListAsString(diffHelper.source1),
                    prep2: AbstractDiffHelper.getOutputListAsString(diffHelper.source2),
                    totalPrep1: diffHelper.source1.size().toString(),
                    totalPrep2: diffHelper.source2.size().toString(),
                    out1: AbstractDiffHelper.getOutputListAsString(diffHelper.outputList1),
                    out2: AbstractDiffHelper.getOutputListAsString(diffHelper.outputList2),
                    totalOut1: diffHelper.outputList1.size().toString(),
                    totalOut2: diffHelper.outputList2.size().toString(),
                ];
            }
            
            return result;
        }
        
        public HashMap<String, Object> getDiffResult(){
            HashMap<String, Object> result = getStatus();
            
            if(!result.containsKey("error") && !isRunning){
                result += [
                    out1: AbstractDiffHelper.getOutputListAsString(diffHelper.outputList1),
                    out2: AbstractDiffHelper.getOutputListAsString(diffHelper.outputList2),
                    totalOut1: diffHelper.outputList1.size().toString(),
                    totalOut2: diffHelper.outputList2.size().toString(),
                    totalOriginal1: original1.size().toString(),
                    totalOriginal2: original2.size().toString(),
                    totalPrep1: diffHelper.source1.size().toString(),
                    totalPrep2: diffHelper.source2.size().toString(),
                ];
            }
            
            return result;
        }
    }
}
