package diffapp

import com.silly_diff.Helpers.XmlDiffHelper
import com.silly_diff.Infostructure.AbstractDiffHelper
import com.silly_diff.Util.XmlUtil
import grails.transaction.Transactional
import groovy.json.JsonSlurper
import groovy.util.slurpersupport.NodeChild

@Transactional
class SimpleComparisonService{
    
    public HashMap<String, Object> testWS(Map<String, Object> params){
        Map postParams = new HashMap<String, String>();
        String wsMethod = params.hMethod.toString();
        
        if(wsMethod == 'POST'){
            if(params.postAsString.asBoolean() && params.postParamsKey.toString().size() > 0){
                postParams = [
                    "${params.postParamsKey.toString()}": params.postParams.toString()
                ];
            }else{
                try{
                    postParams = new JsonSlurper().parseText(params.postParams.toString());
                }catch(Exception e){
                    throw new CustomException("POST parameters are malformed. Please verify your inputs. Original message: '${e.getMessage()}");
                }
            }
        }

        String wsPath1 = params.path1.toString();
        String wsPath2 = params.containsKey("path2") && params.path2.toString().size() > 0 ? params.path2.toString() : params.path1.toString();
        
        NodeChild xml1 = HttpUtil.getWsResponse(params.address1.toString(), wsPath1, "", wsMethod, params.format.toString(), postParams);
        NodeChild xml2;
        if(params.address1.toString() == params.address2.toString() && wsPath1 == wsPath2){
            xml2 = xml1;
        }else{
            xml2 = HttpUtil.getWsResponse(params.address2.toString(), wsPath2, "", wsMethod, params.format.toString(), postParams);
        }
        
        HashMap<String, Object> result = _testXml(xml1, xml2, params);
        result.put("original1", AbstractDiffHelper.getOutputElementAsString(xml1));
        result.put("original2", AbstractDiffHelper.getOutputElementAsString(xml2));
        
        return result;
    }
    
    public HashMap<String, Object> testXML(Map<String, Object> params){
        XmlSlurper xs = new XmlSlurper();
        NodeChild xml1;
        NodeChild xml2;
        
        try{
            xml1 = (NodeChild)xs.parseText(params.xml1.toString());
        }catch(Exception e){
            throw new CustomException("XML input 1 is malformed. Please verify your inputs. Original message: '${e.getMessage()}'.");
        }
        
        try{
            xml2 = (NodeChild)xs.parseText(params.xml2.toString());
        }catch(Exception e){
            throw new CustomException("XML input 2 is malformed. Please verify your inputs. Original message: '${e.getMessage()}'.");
        }
        
        return _testXml(xml1, xml2, params);
    }

    protected HashMap<String, Object> _testXml(NodeChild xml1, NodeChild xml2, Map<String, Object> params){
        String xmlPath = params.get("xmlPath", "").toString();
        List<NodeChild> tst1 = XmlUtil.walkXmlByPath(xmlPath, xml1);
        List<NodeChild> tst2 = XmlUtil.walkXmlByPath(xmlPath, xml2);

        XmlDiffHelper diffHelper = new XmlDiffHelper(tst1, tst2);
        diffHelper.setupFromConfig(_prepareXmlDiffConfig(params));
        diffHelper.calcDiff();

        return [
            passed: diffHelper.isSimilar(),
            out1: AbstractDiffHelper.getOutputListAsString(diffHelper.outputList1),
            out2: AbstractDiffHelper.getOutputListAsString(diffHelper.outputList2),
            prep1: AbstractDiffHelper.getOutputListAsString(diffHelper.source1),
            prep2: AbstractDiffHelper.getOutputListAsString(diffHelper.source2)
        ];
    }
    
    protected Map<String, Object> _prepareXmlDiffConfig(Map<String, Object> params){
        /*HashMap<String, String> invs = new HashMap<String, String>();
        ArrayList<String> ias = new ArrayList<String>();
        ArrayList<String> ins = new ArrayList<String>();
        
        String ignoredElements = params.ignoredElements.toString();
        if(ignoredElements.size() > 0){
            ignoredElements.split(",").each{String _ie->
                String ie = _ie.trim();
                if(ie.contains("=")){
                    String[] inv = ie.split("=").collect{it.trim()};
                    if(inv.size() == 2){
                        invs.put(inv[0], inv[1]);
                    }
                }else if(ie.contains("@")){
                    ias.add(ie);
                }else{
                    ins.add(ie);
                }
            };
            
        }*/
        HashMap<String, Object> ignoredElements = UtilService.parseIgnoredElements(params.ignoredElements.toString());
        
        return [
            orderlySafeMode: params.orderlySafeMode,
            needleHelper: params.needleHelper.toString().size() > 0 ? params.needleHelper.toString().split(",") : new ArrayList<String>(),
            showErrors: false,
            orderlySafeChildrenMode: params.orderlySafeChildrenMode,
            "ignoreNodesWValues": ignoredElements["ignoreValues"],
            "ignoreAttrs": ignoredElements["ignoreAttrs"],
            "ignoreNodes": ignoredElements["ignore"],
        ];
    }

    public void HelloWorld(String _xml1, String _xml2){
        XmlSlurper xs = new XmlSlurper();
        NodeChild xml1 = xs.parseText(_xml1);
        NodeChild xml2 = xs.parseText(_xml2);

        AbstractDiffHelper xdh = new XmlDiffHelper(xml1.toList(), xml2.toList());
        xdh.calcDiff();
        log.info(AbstractDiffHelper.getOutputListAsString(xdh.outputList1));
        log.info(AbstractDiffHelper.getOutputListAsString(xdh.outputList2));
    }
    
    public void HelloUniverse(){
        String _xml1 = """
            <result>
                <dealers>
                    <dealer>
                        <name>www</name>
                        <id>2</id>
                    </dealer>
                    <dealer>
                        <name>qqq</name>
                        <id>1</id>
                    </dealer>
                </dealers>
            </result>
        """;
        String _xml2 = """
            <result>
                <dealers>
                    <dealer>
                        <name>qqq</name>
                        <id>1</id>
                    </dealer>
                    <dealer>
                        <name>eee</name>
                        <id>3</id>
                    </dealer>
                </dealers>
            </result>
        """;
        String errors = "";
        
        //turn XML strings into NodeChild object
        XmlSlurper xs = new XmlSlurper();
        NodeChild xml1;
        NodeChild xml2;
        try{
            xml1 = xs.parseText(_xml1);
            xml2 = xs.parseText(_xml2);
        }catch(Exception e){
            errors += e.getMessage();
        }
        
        if(xml1 && xml2){
            // XmlDiffHelper contructor accepts List<NodeChild> object. 
            // If you want to compare entire XMLs, use 
            // new XmlDiffHelper(xml1.toList(), xml2.toList());
            // But in our case only interesting part is "delaer" node, so let's pass XmlPath to that object:
            String xmlPath = "dealers.dealer";
            List<NodeChild> xmlList1 = XmlUtil.walkXmlByPath(xmlPath, xml1);
            List<NodeChild> xmlList2 = XmlUtil.walkXmlByPath(xmlPath, xml2);
            
            AbstractDiffHelper xdh = new XmlDiffHelper(xmlList1, xmlList2);
            
            // Put any additional parameters as Map<String, Object>
            xdh.setupFromConfig([
                "orderlySafeMode": false,
                "showErrors": false,
                "orderlySafeChildrenMode": true
            ]);
            
            // Or individually
            // Skip XML object order
            xdh.orderlySafeMode = false;
            // Don't put anything into console
            xdh.showErrors = false;
            // Check order of Internal nodes
            xdh.orderlySafeChildrenMode = true;
            
            // Now run comparison
            xdh.calcDiff();
            
            // Now let's review results
            if(xdh.isSimilar()){
                log.info("Success! No diff found!");
            }else{
                String xmlResult1 = AbstractDiffHelper.getOutputListAsString(xdh.outputList1);
                String xmlResult2 = AbstractDiffHelper.getOutputListAsString(xdh.outputList2);
                
                assert xmlResult1 == """<dealer>\r\n  <name>www</name>\r\n  <id>2</id>\r\n</dealer>\r\n""";
                assert xmlResult2 == """<dealer>\r\n  <name>eee</name>\r\n  <id>3</id>\r\n</dealer>\r\n""";
            }
        }
    }

}
