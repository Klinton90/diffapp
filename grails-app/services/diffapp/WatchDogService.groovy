package diffapp

import groovy.util.logging.Slf4j

@Slf4j
class WatchDogService{
    
    public static int WATCH_DOG_FREQUENCY = 15;
    
    protected Calendar _lastTime;
    
    protected TestCaseRun _testCaseRun;
    
    protected Boolean status = false;
    
    public WatchDogService(){
        throw new Exception("Not allowed but can't compile without that sheet!");
    }

    public WatchDogService(TestCaseRun testCaseRun){
        _lastTime = Calendar.getInstance();
        _lastTime.add(Calendar.SECOND, WATCH_DOG_FREQUENCY);
        _testCaseRun = testCaseRun;
    }
    
    public Boolean isCanceled(){
        if(!status){
            Calendar newTime = Calendar.getInstance();
            if(_lastTime.before(newTime)){
                TestCaseRun.withNewTransaction{
                    _testCaseRun = TestCaseRun.get(_testCaseRun.id);
                }
                if(_testCaseRun.refresh().status == "CANCELING"){
                    status = true;
                }else{
                    newTime.add(Calendar.SECOND, WATCH_DOG_FREQUENCY);
                    _lastTime = newTime;
                }
            }
        }
        
        return status;
    }
}
