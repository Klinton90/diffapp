package diffapp

import com.silly_diff.Util.SqlUtil
import grails.transaction.Transactional
import groovy.sql.Sql

//Transactional
class UtilService{

    public static Sql createSqlConnection(DbSetting dbSetting){
        return SqlUtil.createSqlConn([
            "DbUrl" : dbSetting.dbDriver.driverUrlPrefix + dbSetting.url,
            "DbUser" : dbSetting.login,
            "DbPass" : dbSetting.password,
            "DbDriver" : dbSetting.dbDriver.driverClass
        ]);
    }
    
    public static HashMap<String, Object> parseIgnoredElements(String ignoredString){
        HashMap<String, String> invs = new HashMap<>();
        ArrayList<String> ias = new ArrayList<>();
        ArrayList<String> ins = new ArrayList<>();
        
        if(ignoredString.size() > 0){
            ignoredString.split(",").each{String _ie->
                String ie = _ie.trim();
                if(ie.contains("=")){
                    String[] inv = ie.split("=").collect{it.trim()};
                    if(inv.size() == 2){
                        invs.put(inv[0], inv[1]);
                    }
                }else if(ie.contains("@")){
                    ias.add(ie);
                }else{
                    ins.add(ie);
                }
            };
        }
        
        return [
            "ignoreValues": invs,
            "ignoreAttrs": ias,
            "ignore": ins
        ];
    }
}
