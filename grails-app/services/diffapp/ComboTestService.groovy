package diffapp

import com.silly_diff.Helpers.DbDiffHelper
import com.silly_diff.Helpers.XmlDiffHelper
import com.silly_diff.Infostructure.AbstractDiffHelper
import com.silly_diff.Util.XmlUtil
import com.silly_diff.Util.SqlUtil
import grails.core.GrailsApplication
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import groovy.util.logging.Slf4j
import groovy.util.slurpersupport.NodeChild

import java.nio.file.Files

//import java.time.LocalDateTime

@Slf4j
class ComboTestService{
    
    GrailsApplication grailsApplication
    
    protected static File _outFolder;

    public ComboTestService(){
    }
    
    protected synchronized File _getOutFolder(){
        if(_outFolder == null){
            _outFolder = new File("${grailsApplication.config.getProperty('custom.outputFolder')}");
            Files.createDirectories(_outFolder.toPath());
        }
        return _outFolder;
    }

    public TestCaseRun initTestCaseRun(int testCaseId){
        TestCaseRun testCaseRun;
        TestCaseRun.withNewSession{
            testCaseRun = new TestCaseRun(testCase: TestCase.get(testCaseId)).save();
        };
        return TestCaseRun.getFullTree(testCaseRun.id);
    }

    public void runTestCase(TestCaseRun testCaseRun){
        if(testCaseRun.testCase == null){
            throw new Exception("Test Case not found");
        }

        String messages = "";
        WatchDogService watchDog = new WatchDogService(testCaseRun);
        Sql sql = UtilService.createSqlConnection(testCaseRun.testCase.dbSetting);
        
        for(int k = 0; k < testCaseRun.testCase.test.size(); k++){
            Test test = testCaseRun.testCase.test[k];
            
            List<Map<String,String>> wsParams = new ArrayList<HashMap<String,String>>();
            String format1 = test.webTest1.format;
            String format2 = test.xml2DbParamId > 0 ? "JSON" : format1;

            if(test.arrayComboGroupId > 0){
                HashMap<String, List<String>> hash = new HashMap<String, List<String>>();
                test.arrayComboGroup.arrayComboParam.each{ArrayComboParam acp ->
                    List<String> params = acp.value.split(',').toList().collect{String val-> 
                        val.trim();
                    };
                    hash.put(acp.name, params);
                };
                wsParams = processArrayParams(hash);
            }
            if(test.sqlComboGroupId > 0){
                if(sql != null){
                    HashMap<String, String> params = new HashMap<String, String>();
                    test.sqlComboParam.each{SqlComboParam scp->
                        params.put(scp.name, scp.value);
                    };
                    try{
                        wsParams += SqlUtil.execute(sql, test.sqlComboGroup.query, params);
                    }catch(Exception e){
                        messages = _log("Warning: Something went on Test ${test.name} wrong when tried to get data from DB for Combinations. Original issue: '${e.message}'", messages);
                    }
                }else{
                    messages = _log("Warning: Test '${test.name}' requires DB connection, but parameters are not defigned", messages);
                }
            }

            int iterationsCnt = wsParams.size();
            if(iterationsCnt > 0){
                TestRun testRun;
                TestRun.withNewTransaction{
                    testRun = new TestRun(total: iterationsCnt, test: test, testCaseRun: testCaseRun).save();
                };

                File outFolder = new File("${_getOutFolder()}/${testCaseRun.id}/${testRun.id}");
                outFolder.mkdirs();

                for(int i = 0; i < iterationsCnt; i++){
                    String original1 = "";
                    String original2 = "";
                    AbstractDiffHelper diffHelper;
                    Boolean passed = false;
                    HashMap<String, String> wsParam = wsParams[i];
                    //ArrayList<HashMap<String, Object>> results = new ArrayList<HashMap<String, Object>>();

                    String _wsParam = wsParam.toString();
                    if(_wsParam.size() > 255){
                        _wsParam = _wsParam.substring(0,250)+"...";
                    }

                    if(test.xml2XmlParamId > 0){
                        NodeChild xml1 = _getWsResponse(testCaseRun.testCase.webEnvironment1.url, test.webTest1, wsParam, messages);
                        NodeChild xml2 = _getWsResponse(testCaseRun.testCase.webEnvironment2.url, (test.webTest2Id > 0 ? test.webTest2 : test.webTest1), wsParam, messages);
                        
                        if(xml1 != null && xml2 != null){
                            original1 = AbstractDiffHelper.getOutputElementAsString(xml1);
                            original2 = AbstractDiffHelper.getOutputElementAsString(xml2);
                            
                            diffHelper = new XmlDiffHelper(XmlUtil.walkXmlByPath(test.listPath, xml1), XmlUtil.walkXmlByPath(test.listPath, xml2));
                            diffHelper.setupFromConfig(_prepareXml2XmlConfig(test.xml2XmlParam));
                        }
                    } else if(test.xml2DbParamId > 0){
                        if(sql != null){
                            List<GroovyRowResult> dbRows;
                            NodeChild xml = _getWsResponse(testCaseRun.testCase.webEnvironment1.url, test.webTest1, wsParam, messages);
                            try{
                                for(int j = 0; j < wsParam.keySet().size(); j++){
                                    String key = wsParam.keySet()[j];
                                    if(wsParam[key] != null && wsParam[key] instanceof String && wsParam[key].size() > 0){
                                        if(wsParam[key].substring(0, 1) == "/"){
                                            wsParam[key] = wsParam[key].substring(1, wsParam[key].size());
                                        }
                                    }
                                }
                                dbRows = SqlUtil.execute(sql, test.xml2DbParam.query, wsParam);
                            }catch(Exception e){
                                messages = _log("Warning: Something went wrong on Test ${test.name} when tried to get data from DB for Xml2Db testing. Original issue: ${e.message}", messages);
                            }

                            if(xml != null && dbRows != null){
                                original1 = AbstractDiffHelper.getOutputElementAsString(xml);
                                original2 = AbstractDiffHelper.getOutputListAsString(dbRows);
                                
                                diffHelper = new DbDiffHelper(XmlUtil.walkXmlByPath(test.listPath, xml), dbRows);
                                diffHelper.setupFromConfig(_prepareXml2DbConfig(test.xml2DbParam));
                                diffHelper.sql = sql;
                            }
                        }else{
                            messages = _log("Warning: Test '${test.name}' requires DB connection, but connection parameters are not defigned", messages);
                        }
                    }
                    
                    if(diffHelper != null){
                        diffHelper.watchDog = watchDog.&isCanceled;
                        
                        try{
                            diffHelper.calcDiff();
                            passed = diffHelper.isSimilar();
                        }catch(Exception e){
                            messages = _log("Warning: Internal Diff error on test ${test.name}. Contact to Developer with fix request. Original issue: ${e.message} on ${e.getStackTrace()[0].getClassName()}", messages);
                        }
                    }else{
                        messages = _log("Warning: Can't create Test from assigned parameters! Check settings for '${test.name}'!", messages);
                    }

                    IterationRun iterationRun;
                    IterationRun.withNewTransaction{
                        iterationRun = new IterationRun(wsParams: _wsParam, passed: passed, testRun: testRun).save();
                    };
                    
                    if(diffHelper != null){
                        _saveFile(outFolder, iterationRun.id, format1, 1, original1.toString(), true);
                        _saveFile(outFolder, iterationRun.id, format2, 2, original2.toString(), true);
                        
                        if(diffHelper.outputList1.size() > 0 || diffHelper.outputList2.size() > 0){
                            _saveFile(outFolder, iterationRun.id, format1, 1, AbstractDiffHelper.getOutputListAsString(diffHelper.outputList1));
                            _saveFile(outFolder, iterationRun.id, format2, 2, AbstractDiffHelper.getOutputListAsString(diffHelper.outputList2));
                        }
                    }
                    
                    /*results.add([
                        "wsParam" : _wsParam,
                        "diffHelper": diffHelper,
                        "original1": original1,
                        "original2": original2
                    ]);
                    _saveResultsAsync(outFolder, results, i, iterationsCnt, format1, format2)*/

                    log.info("${test.name}@${wsParam.toString()}: ${passed}");
                    if(watchDog.isCanceled()){
                        log.info("Stopped");
                        finalizeTestCaseRun(testCaseRun, messages, "ERROR");
                        return;
                    }
                };

                System.gc();
            }else{
                messages = _log("Warning: Can't create Test iterations. Check settings for '${test.name}'", messages);
            }
        };

        finalizeTestCaseRun(testCaseRun, messages);
    }
    
    public void finalizeTestCaseRun(TestCaseRun testCaseRun, String messages, String status = "FINISHED"){
        if(messages.size() > 65335){
            messages = messages.substring(0, 65335);
        }
        TestCaseRun.withNewTransaction{
            TestCaseRun t = TestCaseRun.get(testCaseRun.id);
            t.messages = messages;
            t.status = status;
            t.save();
        }
    }

    protected void _saveResultsAsync(File outFolder, ArrayList<HashMap<String, Object>> results, int i, int iterationsCnt, String format1, String format2){
        if(results.size() == 15 || i == iterationsCnt - 1){
            for(int j = 0; j < results.size(); j++){
                HashMap<String, Object> _res = results[j];
                Boolean _passed = false;
                if(_res.diffHelper != null){
                    _passed = _res.diffHelper.isSimilar();
                }
                IterationRun iterationRun;
                IterationRun.withNewTransaction{
                    iterationRun = new IterationRun(wsParams: _res.wsParam, passed: _passed, testRun: testRun).save();
                };

                if(_res.diffHelper != null){
                    _saveFile(outFolder, iterationRun.id, format1, 1, _res.original1.toString(), true);
                    _saveFile(outFolder, iterationRun.id, format2, 2, _res.original2.toString(), true);
                    
                    if(_res.diffHelper.outputList1.size() > 0 || _res.diffHelper.outputList2.size() > 0){
                        _saveFile(outFolder, iterationRun.id, format1, 1, AbstractDiffHelper.getOutputListAsString(_res.diffHelper.outputList1));
                        _saveFile(outFolder, iterationRun.id, format2, 2, AbstractDiffHelper.getOutputListAsString(_res.diffHelper.outputList2));
                    }
                }
            }
        }
    }
    
    protected void _saveFile(File outFolder, int iterationRunId, String format, int type, String data, Boolean original = false){
        new File("${outFolder.getAbsolutePath()}/${iterationRunId}_${original ? "original": ""}${type == 1 ? "1" : "2"}.${format.toLowerCase()}").withWriter {writer ->
            writer.write(data);
        };
    }

    protected static String _log(String message, String messages){
        messages += "$message\r\n";
        log.info(message);
        return messages;
    }
    
    public static TestCaseRun getLatestTCR(int testCaseId){
        TestCaseRun tcr = (TestCaseRun)TestCaseRun.createCriteria().get{
            eq("testCase.id", testCaseId)
            maxResults(1)
            order("id", "desc")
        };

        return tcr;
    }

    public File getFileResult(String testCaseRunId, String testRunId, String iterationRunId, String type, String format, Boolean original = false){
        return new File("${grailsApplication.config.getProperty('custom.outputFolder')}/${testCaseRunId}/${testRunId}/${iterationRunId}_${original ? 'original' : ''}${type}.${format.toLowerCase()}");
    }
    
    public static String getDbMapping(String xmlSource){
        return XmlUtil.mapXml((NodeChild)new XmlSlurper().parseText(xmlSource));
    }

    protected static NodeChild _getWsResponse(String host, WebTest webTest, Map<String, String> wsParam, String messages){
        NodeChild result;
        try{
            result = HttpUtil.getWsResponse(host, webTest.pathPattern, webTest.webServiceName, webTest.hMethod, webTest.format, wsParam);
        }catch(Exception e){
            messages = _log("Warning: Can't connect to WebService '$webTest.name' under URL:'$host'. Original issue: ${e.message}", messages);
        }
        return result;
    }

    public static List<Map<String,String>> processArrayParams(Map<String, List<String>> hash, List<Map<String,String>> result = new ArrayList<Map<String,String>>(), int index = 0, Map<String,String> curRow = new HashMap<String,String>()){
        Map<String,String> _curRow = (Map<String,String>)curRow.clone();
        Set<String> keys = hash.keySet();
        String key = keys[index];
        hash[key].each{String node->
            String val = node.trim();
            _curRow.put(key, val == "_" ? "" : val);
            if(keys.size() > index + 1){
                processArrayParams(hash, result, index + 1, _curRow);
            }else{
                result.add((Map<String,String>)_curRow.clone());
            }
        };

        return result;
    }

    protected static HashMap<String, Object> _prepareXml2XmlConfig(Xml2XmlParam xml2XmlParam){
        HashMap<String, String> invs = new HashMap<String, String>();
        ArrayList<String> ias = new ArrayList<String>();
        ArrayList<String> ins = new ArrayList<String>();
        xml2XmlParam.ignoredElement.each{IgnoredElement ie->
            if(ie.value.size() > 0){
                invs.put(ie.name, ie.value);
            }else if(ie.name.contains("@")){
                ias.add(ie.name);
            }else{
                ins.add(ie.name);
            }
        };

        return [
            "showErrors" : false,
            "ignoreNodesWValues": invs,
            "ignoreAttrs": ias,
            "ignoreNodes": ins,
            "orderlySafeMode": xml2XmlParam.orderlySafeMode,
            "orderlySafeChildrenMode": xml2XmlParam.orderlySafeChildrenMode,
            "needleHelper": xml2XmlParam.needleHelper.size() > 0 ? xml2XmlParam.needleHelper.split(",") : new ArrayList<String>()
        ];
    }

    protected static HashMap<String, Object> _prepareXml2DbConfig(Xml2DbParam xml2DbParam){
        HashMap<String, String> ims = new HashMap<String, String>();
        xml2DbParam.includedMapping.each{IncludedMapping im ->
            ims.put(im.name, im.query);
        }

        return [
            "showErrors" : false,
            "subQueryFromFile" : false,
            "orderlySafeMode": xml2DbParam.orderlySafeMode,
            "orderlySafeArrayMode": xml2DbParam.orderlySafeArrayMode,
            "orderlySafeIncludedMode": xml2DbParam.orderlySafeIncludedMode,
            "ignoredValue": xml2DbParam.ignoredValue,
            "includedNodes": ims
        ];
    }

    /*protected Sql sql;
    
    protected Sql _getSql(){
        if(sql == null){
            sql = Sql.newInstance(
                url: grailsApplication.config.getProperty('custom.sqlConnections.DIFF_APP.url'),
                user: grailsApplication.config.getProperty('custom.sqlConnections.DIFF_APP.login'),
                password: grailsApplication.config.getProperty('custom.sqlConnections.DIFF_APP.pass'),
                driver: grailsApplication.config.getProperty('custom.sqlConnections.DIFF_APP.driver')
            );
        }
        return sql;
    }
    
    protected int _createTestCaseRun(int testCaseId){
        def result = _getSql().executeInsert("INSERT INTO test_case_runs (tc_id, messages, version) VALUES (${testCaseId}, '', 0)");

        return result[0][0];
    }

    protected int _createTestRun(int iCnt, Test test, TestCaseRun testCaseRun){
        def result = _getSql().executeInsert("INSERT INTO test_runs (total, test_id, tc_run_id, version) VALUES (${iCnt}, ${test.id}, ${testCaseRun.id}, 0)");

        return result[0][0];
    }

    protected int _createIterationRun(String wsParam, Boolean passed, int testRunId){
        def result = _getSql().executeInsert("INSERT INTO iteration_runs (ws_params, passed, test_run_id, version) VALUES (${wsParam}, ${passed}, ${testRunId}, 0)");

        return result[0][0];
    }*/
}
