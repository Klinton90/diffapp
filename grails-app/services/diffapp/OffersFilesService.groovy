package diffapp

import grails.core.GrailsApplication
import grails.transaction.Transactional
import org.apache.commons.net.ftp.FTPClient
import org.apache.commons.net.ftp.FTPFile
import org.springframework.web.multipart.MultipartFile

import java.nio.file.Files
import java.util.zip.ZipEntry
import java.util.zip.ZipFile

@Transactional
class OffersFilesService{

    GrailsApplication grailsApplication;

    protected static File outFolder;

    protected synchronized File _getOutFolder(){
        if(outFolder == null){
            outFolder = new File("${grailsApplication.config.getProperty('custom.outputFolder')}/${grailsApplication.config.getProperty('custom.offers.subFolder')}");
            Files.createDirectories(outFolder.toPath());
        }
        return outFolder;
    }
    
    public File findFeedFile(String type, String division = "IN"){
        String _div = division == "NI" ? "Nissan" : "Infiniti";
        File result = _getOutFolder().listFiles().findAll{File file ->
            String fileName = file.name.split("/").last();
            return fileName.startsWith(type+"Offers") && fileName.endsWith(_div+".xml");
        }.sort{File file->
            return -file.lastModified()
        }.head();
        if(result == null){
            throw new Exception("Cannot find file Offers files. Please DOWNLOAD feed files before testing!");
        }
        return result;
    }

    public List<File> downloadFiles(){
        ArrayList<File> files = new ArrayList<File>();

        FTPClient ftpClient = new FTPClient();
        try{
            ftpClient.connect(grailsApplication.config.getProperty('custom.offers.feedFtp.url'));
            ftpClient.enterLocalPassiveMode();
            ftpClient.login(grailsApplication.config.getProperty('custom.offers.feedFtp.login'), grailsApplication.config.getProperty('custom.offers.feedFtp.pass'));
            ftpClient.changeWorkingDirectory(grailsApplication.config.getProperty('custom.offers.feedFtp.path'));
        }catch(Exception e){
            throw new Exception("Cannot connect to FTP with FeedFiles! Try again later or check connection settings. Original issue: ${e.getMessage()}");
        }

        File outFolder = _getOutFolder();
        outFolder.deleteDir();
        outFolder.mkdirs();

        File nissan = _download(ftpClient, "Nissan", outFolder);
        File infiniti = _download(ftpClient, "Infiniti", outFolder);
        files.add(nissan);
        files.add(infiniti);
        files.addAll(_unzip(nissan));
        files.addAll(_unzip(infiniti));

        files.each{File f->
            log.info(f.name);
        }

        ftpClient.logout();
        ftpClient.disconnect();

        files.unique();
        return files;
    }

    public File getFeedFile(String fileName){
        return new File("${_getOutFolder().getAbsolutePath()}/$fileName");
    }

    protected static File _download(FTPClient ftpClient, String name, File outFolder){
        FTPFile ftpFile = ftpClient.listFiles().findAll{FTPFile ff ->
            return ff.name.contains(name) && ff.getSize() > 4096;
        }.max{FTPFile ff ->
            return ff.timestamp.time;
        };

        File localFile = new File("${outFolder.getAbsolutePath()}/${ftpFile.getName()}");
        localFile.withOutputStream{OutputStream os ->
            ftpClient.retrieveFile(ftpFile.name, os);
        };

        return localFile;
    }

    protected static ArrayList<File> _unzip(File file){
        ArrayList<File> files = new ArrayList<File>();
        ZipFile zipFile = new ZipFile(file);
        zipFile.entries().findAll{!it.directory}.each{ZipEntry zipEntry ->
            File f = new File("${file.getParentFile().getAbsolutePath()}/${zipEntry.name}");
            f.write(zipFile.getInputStream(zipEntry).text.toString());
            files.add(f);
        }
        zipFile.close();
        return files;
    }
    
    public File moveUploadedFile(MultipartFile file){
        File target = new File("${_getOutFolder().getAbsolutePath()}/${file.getOriginalFilename()}");
        file.transferTo(target);
        return target;
    }
}
