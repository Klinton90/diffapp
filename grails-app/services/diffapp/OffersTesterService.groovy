package diffapp

import com.silly_diff.Helpers.DbDiffHelper
import com.silly_diff.Infostructure.AbstractDiffHelper
import com.silly_diff.Util.SqlUtil
import com.silly_diff.Util.XmlUtil
import grails.core.GrailsApplication
import grails.transaction.Transactional
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import groovy.util.logging.Slf4j
import groovy.util.slurpersupport.NodeChild

@Transactional
@Slf4j
class OffersTesterService{

    GrailsApplication grailsApplication;
    
    OffersFilesService offersFilesService;
    
    protected List<Map<String, String>> combinations;
    
    protected static final String _gmo = "getmodellineoffers";
    protected static final String _gto = "gettrimoffers";
    protected static final String _gco = "getcurrentoffers";

    protected static Sql sql;
    
    protected synchronized Sql _getSql(){
        if(sql == null){
            sql = Sql.newInstance(
                url: grailsApplication.config.getProperty('custom.sqlConnections.US_QA.url'),
                user: grailsApplication.config.getProperty('custom.sqlConnections.US_QA.login'),
                password: grailsApplication.config.getProperty('custom.sqlConnections.US_QA.pass'),
                driver: grailsApplication.config.getProperty('custom.sqlConnections.US_QA.driver')
            );
        }
        return sql;
    }
    
    public String prepareInputs(Map<String, Object> params){
        combinations = null;
        String errors = "";
        int curYear = Calendar.getInstance().get(Calendar.YEAR);
        ArrayList<String> allYears = [
            (curYear - 1).toString(),
            curYear.toString(),
            (curYear + 1).toString()
        ];
        
        if(params.containsKey("dma")){
            ArrayList<String> dmas = _getParamsFromString(params["dma"].toString());
            if(dmas.size() == 0){
                errors += "Can't find any DMA values";
            }

            HashMap<String, List<String>> _testData = [
                "dma" : dmas,
                "discontinued": [params["discontinued"].toString()],
            ];

            if(params["type"] == "MODEL"){
                Boolean hasYear = params.containsKey("year");
                Boolean hasModel = params.containsKey("model");

                if(!hasYear){
                    errors += "Please select YEARs to test or type 'ALL' to test all variants\r\n";
                }
                if(!hasModel){
                    errors += "Please select MODELs to test or type 'ALL' to test all variants\r\n";
                }

                if(hasModel && hasYear){
                    ArrayList<String> years = new ArrayList<String>();
                    ArrayList<String> models = new ArrayList<String>();

                    if(params["year"].toString().toUpperCase() == "ALL"){
                        years = allYears;
                    }else{
                        _getParamsFromString(params["year"].toString()).each{String year ->
                            if(!year.isInteger() || year.toInteger() < curYear - 2 || year.toInteger() > curYear + 1){
                                errors += "Year '$year' has unexpected value. Please verify your parameters\r\n";
                            }else{
                                years.add(year);
                            }
                        }
                        if(years.size() == 0){
                            errors += "Can't find any YEAR values";
                        }
                    }

                    if(years.size() > 0){
                        if(params["model"].toString().toUpperCase() == "ALL"){
                            SqlUtil.executeFile(_getSql(), "sql/OfferTester/getmodels.sql", ["years": years]).collect{GroovyRowResult t ->
                                models.add(t.VAL.toString())
                            }
                        }else{
                            models = _getParamsFromString(params["model"].toString());
                        }
                        if(models.size() == 0){
                            errors += "Can't find any MODEL values";
                        }
                    }

                    if(errors.size() == 0){
                        _testData += [
                            "year"        : years,
                            "model"       : models,
                            "all"         : [params["all"].toString()]
                        ];
                    }
                }
            }else if(params["type"] == "TRIM"){
                if(params.containsKey("acode")){
                    ArrayList<String> acodes = new ArrayList<String>();
                    
                    if(params["acode"].toString().toUpperCase() == "ALL"){
                        SqlUtil.executeFile(_getSql(), "sql/OfferTester/gettrims.sql", ["years": allYears]).collect{GroovyRowResult t ->
                            acodes.add(t.VAL.toString())
                        }
                    }else{
                        acodes = _getParamsFromString(params["acode"].toString());
                    }
                    if(acodes.size() == 0){
                        errors += "Can't find any TRIM values";
                    }
                    
                    if(errors.size() == 0){
                        _testData += [
                            "acode": acodes,
                            "all": [params["all"].toString()]
                        ];
                    }
                }else{
                    errors += "Please select TRIMs to test or type 'ALL' to test all variants\r\n";
                }
            }
            combinations = ComboTestService.processArrayParams(_testData);
            
            if(["MODEL", "TRIM"].contains(params["type"])){
                List<GroovyRowResult> divisions = SqlUtil.executeFile(_getSql(), "sql/OfferTester/getDivisions.sql");
                combinations.collect{Map<String, String> c->
                    GroovyRowResult matchedDivision = divisions.find{GroovyRowResult d->
                        if(params["type"] == "MODEL"){
                            return d["MODELCODE"] == c["model"];
                        }else{
                            return d["ACODE"] == c["acode"];
                        }
                    };
                    c["division"] = matchedDivision["DIVISION"].toString();
                };
            }
        }else{
            errors += "Please select DMAs to test\r\n";
        }

        return errors;
    }

    protected static List<String> _getParamsFromString(String source){
        return source.split(",").collect{String t -> t.trim()}.toList();
    }
    
    public ArrayList<HashMap<String, Object>> testOffers(Map<String, Object> params, Map<String, Integer> progress){
        ArrayList<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>();
        if(combinations.size() > 0){
            progress["max"] = combinations.size();

            combinations.each{Map<String, String> paramSet ->
                HashMap<String, Object> diff;
                progress["cur"]++;
                
                if(params["type"] == "MODEL"){
                    if(params["cmpType"] == "t1"){
                        diff = _testModelOffers(paramSet, params["host"].toString());
                    }else{
                        diff = _testModelOffers2(paramSet, params["host"].toString());
                    }
                } else if(params["type"] == "TRIM"){
                    diff = _testTrimOffers(paramSet, params["host"].toString());
                }else{
                    diff = _testCustomOffers(paramSet, params["host"].toString());
                }
                if(diff.size() > 0){
                    result.add(diff);
                }
            }

            combinations = null;
        }else{
            throw new Exception("'TestData' is empty. Feed this class with input parameters before calling this function!");
        }
        return result;
    }
    
    protected HashMap<String, Object> _testModelOffers(Map<String, String> params, String host){
        HashMap<String, Object> result = new HashMap<String, Object>();
        HashMap<String, Object> feedNM = new HashMap<String, Object>();

        HashMap<String, ArrayList<String>> feedOffers;
        if(params["all"] == "/all"){
            feedOffers = _getModelOffersFromAll(params);
        }else{
            feedOffers = _getModelOffersFromFeatured(params);
        }
        
        HashMap<String, ArrayList<String>> wsOffers = new HashMap<String, ArrayList<String>>();
        NodeChild wsResponse = HttpUtil.getWsResponse(host, '/$discontinued/$model/$dma/$year/$all.xml', _gmo, "GET", "XML", params);
        XmlUtil.walkXmlByPath("data.promoOffers.promoOffer", wsResponse).each{node->
            List<String> nCodes = new ArrayList<String>();
            node.nCodes.nCode.each{NodeChild nCode->
                nCodes.add(nCode.localText()[0].toString());
            };
            wsOffers.put(node.id.toString(), nCodes);
        };
        
        feedOffers.each{String feed_id, ArrayList<String> feed_nCodes->
            Boolean match = true;
            if(wsOffers.keySet().contains(feed_id)){
                ArrayList<String> ws_nCodes = wsOffers.get(feed_id);
                for(int i = 0; i < feed_nCodes.size(); i++){
                    String feed_nCode = feed_nCodes[i];
                    String matched2 = ws_nCodes.find{String ws_nCode ->
                        return ws_nCode == feed_nCode;
                    }
                    if(matched2 != null){
                        match = true;
                        break;
                    }
                }
            }else{
                match = false;
            }
            
            if(!match){
                feedNM.put(feed_id, feed_nCodes);
            }else{
                wsOffers.remove(feed_id);
            }
        }
        
        if(feedNM.size() > 0 || wsOffers.size() > 0){
            result = [
                "paramSet": params.toString(),
                "feedList": feedNM,
                "wsList"  : wsOffers
            ];
        }

        return result;
    }

    protected HashMap<String, ArrayList<String>> _getModelOffersFromFeatured(Map<String, String> params){
        HashMap<String, ArrayList<String>> result = new HashMap<String, ArrayList<String>>();
        File feedFile = offersFilesService.findFeedFile("Featured", params["division"]);
        NodeChild feedData = (NodeChild)new XmlSlurper().parseText(feedFile.getText());

        feedData.offers.ModelLine.find{node ->
            return node.@code == params["model"];
        }.dmaAssignment.findAll{node ->
            return node.@dma.toString().contains(params["dma"]);
        }.promoLocation.offer.each{node->
            String[] mfgCodes = node.@modelcode.toString().split(",");
            
            result.put(node.@id.toString(), _getAcodesByMfgcodes(mfgCodes.toList(), params["year"].toString()));
        };

        return result;
    }

    protected HashMap<String, ArrayList<String>> _getModelOffersFromAll(Map<String, String> params){
        HashMap<String, ArrayList<String>> result = new HashMap<String, ArrayList<String>>();
        File feedFile = offersFilesService.findFeedFile("All", params["division"]);
        NodeChild feedData = (NodeChild)new XmlSlurper().parseText(feedFile.getText());

        feedData.promoOffer.each{node ->
            if(node.modelLineCode == params["model"] && node.dma.toString().contains(params["dma"]) && (params["division"] == "NI" || node.@pmnt_est.toString() != "0")){
                ArrayList<String> mfgCodes = new ArrayList<String>();
                node.modelCodes.modelCode.@code.each{mc ->
                    mfgCodes.add(mc.toString());
                };
                
                result.put(node.id.toString(), _getAcodesByMfgcodes(mfgCodes, params["year"].toString()));
            }
        }

        return result;
    }

    protected HashMap<String, Object> _testModelOffers2(Map<String, String> params, String host){
        ArrayList<HashMap<String, String>> feedOffers;
        ArrayList<String> mapping = [
            "id",
            "promoOfferPosition",
            "nCodes.nCode.>"
        ];
        if(params["all"] == "/all"){
            //feedOffers = _getModelOffersFromAll2(params);
            feedOffers = _getModelOffersFromAll3(params);
        }else{
            //mapping.add("promoOfferPosition");
            feedOffers = _getModelOffersFromFeatured2(params);
        }

        NodeChild _wsOffers = HttpUtil.getWsResponse(host, '/$discontinued/$model/$dma/$year$all.xml', _gmo, "GET", "XML", params);
        List<NodeChild> wsOffers = XmlUtil.walkXmlByPath("data.promoOffers.promoOffer", _wsOffers);

        ArrayList<HashMap<String, String>> wsOffers_original = new ArrayList<HashMap<String, String>>();
        wsOffers.each{NodeChild wsOffer->
            wsOffers_original.add(_getWsOfferResult(wsOffer, mapping));
        }
        HashMap<String, Object> result = [
            "paramSet": params.toString(),
            "feedList_original": AbstractDiffHelper.getOutputListAsString(feedOffers),
            "wsList_original": AbstractDiffHelper.getOutputListAsString(wsOffers_original)
        ];
        
        log.info(params.toString());
        DbDiffHelper diff = _getDiff(wsOffers, feedOffers);
        if(!diff.isSimilar()){
            ArrayList<HashMap<String, String>> wsList = new ArrayList<HashMap<String, String>>();
            diff.outputList1.each{NodeChild wsOffer ->
                wsList.add(_getWsOfferResult(wsOffer, mapping));
            };

            result << [
                "feedList": AbstractDiffHelper.getOutputListAsString(diff.outputList2),
                "wsList"  : AbstractDiffHelper.getOutputListAsString(wsList)
            ];
        }
        
        return result;
    }

    protected ArrayList<HashMap<String, String>> _getModelOffersFromFeatured2(Map<String, String> params){
        ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
        File feedFile = offersFilesService.findFeedFile("Featured", params["division"]);
        NodeChild feedData = (NodeChild)new XmlSlurper().parseText(feedFile.getText());

        feedData.offers.ModelLine.find{node ->
            return node.@code == params["model"];
        }.dmaAssignment.findAll{node ->
            return node.@dma.toString().contains(params["dma"]);
        }.promoLocation.each{pl ->
            pl.offer.each{node ->
                String[] mfgCodes = node.@modelcode.toString().split(",");
                List<String> aCodes = _getAcodesByMfgcodes(mfgCodes.toList(), params["year"].toString());

                if(aCodes.size() > 0){
                    HashMap<String, String> offer = result.find{HashMap<String, String> o ->
                        return o["id"] == node.@id.toString() && o["promoOfferPosition"] == pl.@loc.toString();
                    };
                    if(offer == null){
                        offer = [
                            "id": node.@id.toString(),
                            "promoOfferPosition": pl.@loc.toString()
                        ];
                        for(int i = 0; i < aCodes.size(); i++){
                            offer.put("nCodes.nCode.>${i}", aCodes[i]);
                        }
                        result.add(offer);
                    }else{
                        String[] t = offer.keySet().findAll{String k -> k.startsWith("nCodes.nCode.>")}.sort().last().split(">");
                        Integer pos = Integer.parseInt(t[1]) + 1;
                        for(int i = 0; i < aCodes.size(); i++){
                            offer.put("nCodes.nCode.>${pos+i}", aCodes[i]);
                        }
                    }
                }
            };
        };

        return result;
    }

    protected ArrayList<HashMap<String, String>> _getModelOffersFromAll2(Map<String, String> params){
        ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
        File feedFile = offersFilesService.findFeedFile("All", params["division"]);
        NodeChild feedData = (NodeChild)new XmlSlurper().parseText(feedFile.getText());
        
        feedData.promoOffer.each{node ->
            if(node.modelLineCode == params["model"] && node.dma.toString().contains(params["dma"]) && (params["division"] == "NI" || node.@pmnt_est.toString() != "0")){
                ArrayList<String> mfgCodes = new ArrayList<String>();
                node.modelCodes.modelCode.@code.each{mc ->
                    mfgCodes.add(mc.toString());
                };
                
                List<String> aCodes = _getAcodesByMfgcodes(mfgCodes, params["year"].toString());
                if(aCodes.size() > 0){
                    HashMap<String, String> offer = ["id": node.id.toString()];
                    for(int i = 0; i < aCodes.size(); i++){
                        offer.put("nCodes.nCode.>${i}", aCodes[i]);
                    }
                    result.add(offer);
                }
            }
        }

        return result;
    }

    protected ArrayList<HashMap<String, String>> _getModelOffersFromAll3(Map<String, String> params){
        ArrayList<HashMap<String, String>> allOffers = _getModelOffersFromAll2(params);
        ArrayList<HashMap<String, String>> featuredOffers = _getModelOffersFromFeatured2(params);
        ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>(featuredOffers);
        
        //Iterator<HashMap<String, String>> allIterator = allOffers.iterator();
        //while(allIterator.hasNext()){
            //HashMap<String, String> curAllOffer = allIterator.next();
        allOffers.each{HashMap<String, String> curAllOffer ->
            Boolean processed = false;
            featuredOffers.each{HashMap<String, String> curFeaturedOffer ->
                if(curFeaturedOffer["id"] == curAllOffer["id"]){
                    processed = true;
                    ArrayList<String> allAcodes = new ArrayList<>();
                    ArrayList<String> featuredAcodes = new ArrayList<>()
                    
                    curAllOffer.each{String key, String value ->
                        if(key.startsWith("nCodes.nCode.>")){
                            allAcodes.add(value);
                        }
                    };

                    curFeaturedOffer.each{String key, String value ->
                        if(key.startsWith("nCodes.nCode.>")){
                            featuredAcodes.add(value);
                        }
                    };
                    
                    ArrayList<String> delta = allAcodes - featuredAcodes;

                    if(delta.size() > 0){
                        /*log.info("Deltas!");
                        log.info(allAcodes.toString());
                        log.info(featuredAcodes.toString());
                        log.info(delta.toString());*/
                        
                        HashMap<String, String> newAllOffer = [
                            id: curAllOffer["id"],
                            //promoOfferPosition: ""
                        ];
                        delta.sort();
                        for(int i = 0; i < delta.size(); i++){
                            newAllOffer.put("nCodes.nCode.>${i}", delta[i]);
                        }
                        
                        result.add(newAllOffer);
                    }
                }
            };
            if(!processed){
                result.add(curAllOffer);
            }
        }

        /*log.info("result start");
        result.each{HashMap<String, String> curRes ->
            log.info(curRes.toString());
        };
        log.info("result end");*/

        return result;
    }
    
    protected HashMap<String, Object> _testTrimOffers(Map<String, String> params, String host){
        ArrayList<HashMap<String, String>> feedOffers;
        if(params["all"] == "/all"){
            feedOffers = _getTrimOffersFromAll(params);
        }else{
            feedOffers = _getTrimOffersFromFeatured(params);
        }

        NodeChild _wsOffers = HttpUtil.getWsResponse(host, '/$acode/$dma$all.xml', _gto, "GET", "XML", params);
        List<NodeChild> wsOffers = XmlUtil.walkXmlByPath("data.promoOffers.promoOffer", _wsOffers);

        ArrayList<HashMap<String, String>> wsOffers_original = new ArrayList<HashMap<String, String>>();
        wsOffers.each{NodeChild wsOffer->
            wsOffers_original.add(_getWsOfferResult(wsOffer, [
                "id"
            ]));
        }
        HashMap<String, Object> result = [
            "paramSet": params.toString(),
            "feedList_original": AbstractDiffHelper.getOutputListAsString(feedOffers),
            "wsList_original": AbstractDiffHelper.getOutputListAsString(wsOffers_original)
        ];
        
        log.info(params.toString());
        DbDiffHelper diff = _getDiff(wsOffers, feedOffers);
        if(!diff.isSimilar()){
            ArrayList<HashMap<String, String>> wsList = new ArrayList<HashMap<String, String>>();
            diff.outputList1.each{NodeChild wsOffer->
                wsList.add(_getWsOfferResult(wsOffer, [
                    "id"
                ]));
            };
            
            result << [
                "feedList": AbstractDiffHelper.getOutputListAsString(diff.outputList2),
                "wsList": AbstractDiffHelper.getOutputListAsString(wsList)
            ];
        }
        
        return result;
    }
    
    protected ArrayList<HashMap<String, String>> _getTrimOffersFromFeatured(Map<String, String> params){
        ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
        File feedFile = offersFilesService.findFeedFile("Featured", params["division"]);
        NodeChild feedData = (NodeChild)new XmlSlurper().parseText(feedFile.getText());

        feedData.offers.ModelLine.dmaAssignment.findAll{node ->
            return node.@dma.toString().contains(params["dma"]);
        }.promoLocation.offer.findAll{node->
            String[] mfgCodes = node.@modelcode.toString().split(",");
            return _getAcodesByMfgcodes(mfgCodes.toList()).contains(params["acode"]);
        }.each{offer->
            result.add([
                "id": offer.@id.toString(),
                //"acode": params["acode"]
            ]);
        };
        
        return result;
    }

    protected ArrayList<HashMap<String, String>> _getTrimOffersFromAll(Map<String, String> params){
        ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
        File feedFile = offersFilesService.findFeedFile("All", params["division"]);
        NodeChild feedData = (NodeChild)new XmlSlurper().parseText(feedFile.getText());

        feedData.promoOffer.each{node->
            if(node.dma.toString().contains(params["dma"]) && (params["division"] == "NI" || node.@pmnt_est.toString() != "0")){
                ArrayList<String> mfgCodes = new ArrayList<String>();
                node.modelCodes.modelCode.@code.each{mc ->
                    mfgCodes.add(mc.toString());
                };
                if(_getAcodesByMfgcodes(mfgCodes).contains(params["acode"])){
                    result.add([
                        "id": node.id.toString(),
                        //"acode": params["acode"]
                    ]);
                }
            }
        }

        return result;
    }

    protected HashMap<String, Object> _testCustomOffers(Map<String, String> params, String host){
        ArrayList<HashMap<String, String>> feedOffers = _getCustomOffersFromFeed(params);

        NodeChild _wsOffers = HttpUtil.getWsResponse(host, '/IN/$dma.xml', _gco, "GET", "XML", params);
        List<NodeChild> wsOffers = XmlUtil.walkXmlByPath("data.SubModelLine.modelYears.modelYear.columns.column.promoOffers.promoOffer", _wsOffers);

        ArrayList<HashMap<String, String>> wsOffers_original = new ArrayList<HashMap<String, String>>();
        wsOffers.each{NodeChild wsOffer->
            wsOffers_original.add(_getWsOfferResult(wsOffer, [
                "id"
            ]));
        }
        HashMap<String, Object> result = [
            "paramSet": params.toString(),
            "feedList_original": AbstractDiffHelper.getOutputListAsString(feedOffers),
            "wsList_original": AbstractDiffHelper.getOutputListAsString(wsOffers_original)
        ];
        
        log.info(params.toString());
        DbDiffHelper diff = _getDiff(wsOffers, feedOffers);
        if(!diff.isSimilar()){
            ArrayList<HashMap<String, String>> wsList = new ArrayList<HashMap<String, String>>();
            diff.outputList1.each{NodeChild wsOffer->
                wsList.add(_getWsOfferResult(wsOffer, [
                    "id"
                ]));
            };
            
            result << [
                "feedList": AbstractDiffHelper.getOutputListAsString(diff.outputList2),
                "wsList": AbstractDiffHelper.getOutputListAsString(wsList)
            ];
        }

        return result;
    }

    protected ArrayList<HashMap<String, String>> _getCustomOffersFromFeed(Map<String, String> params){
        ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();
        File feedFile = offersFilesService.findFeedFile("Current");
        NodeChild feedData = (NodeChild)new XmlSlurper().parseText(feedFile.getText());
        
        /*def columns = feedData.currentOffers.subModelLine.modelYear.dmaAssignment.findAll{node->
            return node.@dma.toString().contains(params["dma"]);
        }.column;
        
        ArrayList<String> mfgCodes = new ArrayList<String>();
        columns.offer.each{node->
            HashMap<String, String> offer = new HashMap<String, String>();
            String modelCodes = node.@modelcode.toString();
            if(modelCodes.size() > 0){
                mfgCodes += modelCodes.split(",").toList();
            }
        };
        columns.modelCodes.each{node->
            String modelCodes = node.localText();
            if(modelCodes.size() > 0 && modelCodes[0].toString().size() > 0){
                mfgCodes += modelCodes[0].toString().split(",");
            }
        };
        log.info(mfgCodes.toString());*/
        feedData.currentOffers.subModelLine.modelYear.dmaAssignment.findAll{node->
            return node.@dma.toString().contains(params["dma"]);
        }.column.offer.each{node->
            result.add(["id": node.@id.toString()]);
        };

        return result;
    }

    protected ArrayList<String> _getAcodesByMfgcodes(List<String> mfgCodes, String year = ""){
        ArrayList<String> result = new ArrayList<String>();
        List<GroovyRowResult> aCodes = SqlUtil.executeFile(_getSql(), "sql/OfferTester/getAcodesForModelOffers.sql", ["mfgCodes": mfgCodes, "year": year]);
        if(aCodes.size() > 0){
            aCodes.each{GroovyRowResult row->
                result.add(row.ACODE.toString());
            };
        }
        return result;
    }

    protected static HashMap<String, String> _getWsOfferResult(NodeChild wsOffer, List<String> transformations){
        HashMap<String, String> result = new HashMap<String, String>();
        transformations.each{String key->
            if(key.substring(key.size() - 2, key.size()) == '.>'){
                String subKey = key.substring(0, key.size() - 2);
                List<NodeChild> hash = XmlUtil.walkXmlByPath(subKey, wsOffer);
                for(int i = 0; i < hash.size(); i++){
                    result.put("${subKey}.>${i}", hash[i].toString());
                }
            }else{
                result.put(key, XmlUtil.walkXmlByPathForValue(key, wsOffer));
            }
        };

        return result;
    }
    
    protected static DbDiffHelper _getDiff(List<NodeChild> wsOffers, List<Map<String, String>> feedOffers){
        DbDiffHelper diffHelper = new DbDiffHelper(wsOffers, feedOffers);
        diffHelper.setupFromConfig([
            "orderlySafeMode": false,
            "skipMissedDb": true,
            //"skipMissedXml": true,
            "orderlySafeArrayMode": false,
            "showErrors": false
        ]);
        diffHelper.calcDiff();
        Boolean passed = diffHelper.isSimilar();
        log.info("passed_status: ${passed.toString()}");
        
        return diffHelper;
    }
}
