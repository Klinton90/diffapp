import diffapp.NameIdMarshaller
import grails.converters.JSON
import grails.util.Environment
import grails.util.Holders
import org.grails.web.converters.marshaller.json.DomainClassMarshaller

class BootStrap {
    def init = { servletContext ->
        
        println "current environment: $Environment.current"

        JSON.createNamedConfig("simple"){
            it.registerObjectMarshaller(new DomainClassMarshaller(false, Holders.getGrailsApplication()));
        }
        JSON.createNamedConfig("nameId"){
            it.registerObjectMarshaller(new NameIdMarshaller());
        }
    }

    def destroy = {
        println "Application shutting down... "
    }
}
