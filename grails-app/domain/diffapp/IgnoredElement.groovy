package diffapp

class IgnoredElement{
    int id;
    String name;
    String value = '';

    static belongsTo = [
        xml2XmlParam: Xml2XmlParam,
        //csv2DbParam: Csv2DbParam
    ];

    static mapping = {
        table 'ignored_elements'
        id  column: 'ie_id',
            generator: 'identity',
            type: 'integer'
        name column: 'name'
        value column: 'value'
        xml2XmlParam column: 'xml2xml_id',
            type: 'integer'
        /*csv2DbParam column: 'csv2db_id',
            type: 'integer'*/
    }

    static constraints = {
        name size: 1..255,
            nullable: false,
            blank: false
        value size: 0..255,
            nullable: false,
            blank: true
        /*xml2XmlParam nullable: true,
            validator: {val, obj, errors->
                if(val != null){
                    if(val.id == 0 && obj.csv2DbParamId == 0){
                        errors.rejectValue(propertyName, 'ignoredElement.required');
                    }
                    if(val.id != 0 && obj.csv2DbParamId > 0){
                        errors.rejectValue(propertyName, 'ignoredElement.onlyOne');
                    }
                }else{
                    val = {id: 0};
                }
            }
        csv2DbParam nullable: true,
            validator: {val, obj, errors->
                if(val == null){
                    val = {id: 0};
                }
            }*/
    }

    public static IgnoredElement getFullTree(int id){
        return IgnoredElement.get(id);
    }
}
