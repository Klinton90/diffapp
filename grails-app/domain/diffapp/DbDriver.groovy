package diffapp

class DbDriver{

    int id;
    String name;
    String driverClass;
    String driverUrlPrefix;

    static mapping = {
        table 'db_drivers'
        id  column: 'db_driver_id',
            generator: 'identity',
            type: 'integer'
        name column: 'name'
        driverClass column: 'driver_class'
        driverUrlPrefix column: 'driver_url_prefix'
    }

    static constraints = {
        name size: 1..50,
            blank: false,
            unique: true,
            nullable: false
        driverClass size: 1..255,
            blank: false,
            unique: true,
            nullable: false
        driverUrlPrefix size: 1..50,
            blank: false,
            nullable: false
    }

    public static DbDriver getFullTree(int id){
        return DbDriver.get(id);
    }
}
