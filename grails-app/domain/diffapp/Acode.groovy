package diffapp

class Acode{
    
    String id;
    float revCode;

    static mapping = {
        table 'vt05_backup'
        id column: 'acode',
            generator: 'assigned',
            type: 'string'
        revCode column: 'rev_code'
    }

    static constraints = {
        id nullable: false, 
            minSize: 11, 
            maxSize: 11
        revCode scale: 3, max: 9999999999999.toFloat(), min: -9999999999999.toFloat()
    }
}
