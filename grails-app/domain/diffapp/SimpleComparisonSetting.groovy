package diffapp

class SimpleComparisonSetting{
    
    int id;
    String name;
    String description;
    String address1;
    String address2;
    String path1;
    String path2 = '';
    String xmlPath = '';
    String hMethod = 'GET';
    String format = 'XML';
    Boolean orderlySafeMode = false;
    String needleHelper = '';
    Boolean orderlySafeChildrenMode = false;
    String ignoredElements = '';
    String postParams = '';
    Boolean postAsString = false;
    String postParamsKey = '';
    
    static mapping = {
        table 'sc_settings'
        id  column: 'sc_setting_id',
            generator: 'identity',
            type: 'integer'
        name column: 'name'
        description column: 'description'
        address1 column: 'address1'
        address2 column: 'address2'
        path1 column: 'path1'
        path2 column: 'path2'
        xmlPath column: 'xml_path'
        hMethod column: 'method'
        format column: 'format'
        orderlySafeMode column: 'orderly_safe'
        orderlySafeChildrenMode column: 'orderly_safe_children'
        needleHelper column: 'needle_helper'
        ignoredElements column: 'ignored_elements'
        postParams column: 'post_params'
        postAsString column: 'post_string'
        postParamsKey column: 'post_string_key'
    }

    static constraints = {
        name size: 1..50,
            blank: false,
            unique: true,
            nullable: false
        description size: 1..65535,
            blank: true,
            unique: false,
            nullable: false
        address1 size: 1..255,
            blank: false,
            unique: false,
            nullable: false
        address2 size: 1..255,
            blank: false,
            unique: false,
            nullable: false
        path1 size: 1..255,
            blank: false,
            unique: false,
            nullable: false
        path2 size: 1..255,
            blank: true,
            unique: false,
            nullable: false
        xmlPath size: 0..255,
            blank: true,
            unique: false,
            nullable: false
        format inList: ['XML', 'JSON'],
            nullable: false,
            blank: false
        hMethod inList: ['GET', 'POST'],
            nullable: false,
            blank: false
        needleHelper size: 1..255,
            nullable: false,
            blank: true
        ignoredElements size: 1..255,
            nullable: false,
            blank: true
        postParams size: 0..65535,
            blank: true
        postParamsKey size: 1..50,
            nullable: false,
            blank: true
    }

    public static SimpleComparisonSetting getFullTree(int id){
        return SimpleComparisonSetting.get(id);
    }
}
