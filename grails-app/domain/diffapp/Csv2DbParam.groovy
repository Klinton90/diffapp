package diffapp

import org.hibernate.FetchMode

class Csv2DbParam{

    int id;
    String name = "";
    String description = "";
    CsvSetting csvSetting;
    DbSetting dbSetting;
    Boolean orderlySafeMode = false;
    Boolean skipMissedFeed = true;
    //String status = "RUNNING";
    String query = "";

    static hasMany = [
        ignoredRow: IgnoredRow
    ]

    static mapping = {
        table 'csv2db_params'
        id  column: 'csv2db_id',
            generator: 'identity',
            type: 'integer'
        name column: 'name'
        description column: 'description'
        csvSetting column: 'csv_setting_id',
            type: 'integer'
        orderlySafeMode column: 'orderly_safe'
        skipMissedFeed column: 'skip_missed_feed'
        //status column: 'status'
        dbSetting column: 'db_setting_id',
            type: 'integer'
        query column: 'query',
            type: 'text'
}

    static constraints = {
        name size: 1..255,
            blank: false,
            nullable: false,
            unique: true
        description size: 0..255,
            blank: true,
            nullable: false
        csvSetting validator: {val, obj, errors->
            if(val.id == 0){
                return errors.rejectValue(propertyName, 'required');
            }
        }
        /*status nullable: false,
            blank: false,
            inList: ["RUNNING", "FINISHED", "ERROR", "CANCELING"]*/
        dbSetting validator: {val, obj, errors->
            if(val.id == 0){
                return errors.rejectValue(propertyName, 'required', [propertyName] as Object[], "This field is required");
            }
        }
        query minSize: 5,
            nullable: false,
            blank: false
    }

    public static Csv2DbParam getFullTree(int id){
        return Csv2DbParam.createCriteria().get{
            eq("id", id)
            fetchMode("csvSetting", FetchMode.JOIN)
            fetchMode("csvSetting.csvColumn", FetchMode.JOIN)
            fetchMode("csvSetting.csvColumn.modificator", FetchMode.JOIN)
            fetchMode("ignoredRow", FetchMode.JOIN)
        };
    }
}
