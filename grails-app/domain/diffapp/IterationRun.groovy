package diffapp

class IterationRun{
    
    int id;
    Boolean passed = true;
    String wsParams = "";
    
    static belongsTo = [testRun: TestRun]
    
    static mapping = {
        table "iteration_runs"
        id column: 'iteration_run_id',
            generator: 'identity',
            type: 'integer'
        passed column: "passed"
        wsParams column: "ws_params"
        testRun column: 'test_run_id',
            type: 'integer'
    }

    static constraints = {
        wsParams nullable: false,
            blank: true,
            size: 0..255
    }

    public static IterationRun getFullTree(int id){
        return IterationRun.get(id);
    }
}
