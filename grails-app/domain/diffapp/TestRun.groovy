package diffapp

import org.hibernate.FetchMode

class TestRun{

    int id;
    int total;
    Test test;

    static belongsTo = [testCaseRun: TestCaseRun]
    
    static hasMany = [iterationRun: IterationRun]

    static mapping = {
        table 'test_runs'
        id column: 'test_run_id',
            generator: 'identity',
            type: 'integer'
        total column: 'total'
        test column: 'test_id'
        testCaseRun column: 'tc_run_id'
    }

    static constraints = {
        total min: 0
    }

    public static TestRun getFullTree(int id){
        return TestRun.createCriteria().get{
            eq("id", id)
            fetchMode("iterationRun", FetchMode.JOIN)
        };
    }
}
