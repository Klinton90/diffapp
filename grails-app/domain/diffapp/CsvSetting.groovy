package diffapp

import org.hibernate.FetchMode

class CsvSetting{

    int id;
    String name = "";
    String splitChar = ",";
    String quoteChar = "";
    Boolean skipFirstLine = false;

    static hasMany = [csvColumn: CsvColumn];

    static mapping = {
        table 'csv_settings'
        id  column: 'csv_setting_id',
            generator: 'identity',
            type: 'integer'
        name column: 'name'
        splitChar column: 'split_char'
        quoteChar column: 'quote_char'
        skipFirstLine column: 'skip_first_line'
    }

    static constraints = {
        name size: 1..255,
            blank: false,
            nullable: false
        splitChar size: 1..1,
            blank: false,
            nullable: false
        quoteChar size: 0..1,
            blank: true,
            nullable: false
        csvColumn nullable: true
    }

    public static CsvSetting getFullTree(int id){
        return CsvSetting.createCriteria().get{
            eq("id", id)
            fetchMode("csvColumn", FetchMode.JOIN)
            fetchMode("csvColumn.modificator", FetchMode.JOIN)
        };
    }
}
