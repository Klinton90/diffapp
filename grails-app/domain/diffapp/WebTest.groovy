package diffapp

class WebTest{
    int id;
    String name;
    String hMethod = "GET";
    String pathPattern = '';
    String webServiceName = '';
    String format = "XML";

    static belongsTo = [Test];

    static mapping = {
        table 'web_tests'
        id  column: 'web_test_id',
            generator: 'identity',
            type: 'integer'
        name column: 'name'
        pathPattern column: 'path_pattern'
        hMethod column: 'hmethod'
        webServiceName column: 'ws_name'
        format column: "format"
    }

    static constraints = {
        name size: 1..50,
            blank: false,
            nullable: false,
            unique: true
        pathPattern size: 0..255,
            blank: true,
            nullable: false
        hMethod inList: ['GET', 'POST'],
            nullable: false,
            blank: false
        webServiceName size: 1..50,
            blank: false,
            nullable: false
        format inList: ['XML', 'JSON'],
            nullable: false,
            blank: false
    }

    public static WebTest getFullTree(int id){
        return WebTest.get(id);
    }
}
