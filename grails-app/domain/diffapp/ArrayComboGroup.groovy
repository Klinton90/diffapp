package diffapp

import org.hibernate.FetchMode

class ArrayComboGroup{
    int id = 0;
    String name;

    static belongsTo = Test;

    static hasMany = [arrayComboParam: ArrayComboParam];

    static mapping = {
        table 'ac_groups'
        id  column: 'acg_id',
            generator: 'identity',
            type: 'integer'
        name column: 'name'
    }

    static constraints = {
        name size: 1..50,
            nullable: false,
            blank: false,
            unique: true
        arrayComboParam nullable: true
    }

    public static ArrayComboGroup getFullTree(int id){
        return ArrayComboGroup.createCriteria().get{
            eq("id", id)
            fetchMode("arrayComboParam", FetchMode.JOIN)
        }
    }
}
