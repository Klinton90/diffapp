package diffapp

import org.hibernate.FetchMode

class TestCaseRun{

    int id;
    String messages = "";
    TestCase testCase;
    Date dateCreated;
    String status = "RUNNING";

    static hasMany = [testRun: TestRun]

    static mapping = {
        table 'test_case_runs'
        id column: 'tc_run_id',
            generator: 'identity',
            type: 'integer'
        messages column: 'messages'
        testCase column: 'tc_id'
        status column: 'status'
    }

    static constraints = {
        testRun nullable: true
        messages nullable: false,
            blank: true
        status nullable: false,
            blank: false,
            inList: ["RUNNING", "FINISHED", "ERROR", "CANCELING"]
    }

    public static TestCaseRun getFullTree(int id){
        return TestCaseRun.createCriteria().get{
            eq("id", id)
            fetchMode("testRun", FetchMode.JOIN)
            fetchMode("testRun.iterationRun", FetchMode.JOIN)
            fetchMode("testCase", FetchMode.JOIN)
            fetchMode("testCase.test", FetchMode.JOIN)
            fetchMode("testCase.test.xml2DbParam", FetchMode.JOIN)
            fetchMode("testCase.test.xml2DbParam.includedMapping", FetchMode.JOIN)
            fetchMode("testCase.test.xml2XmlParam", FetchMode.JOIN)
            fetchMode("testCase.test.xml2XmlParam.ignoredElement", FetchMode.JOIN)
            fetchMode("testCase.test.webTest1", FetchMode.JOIN)
            fetchMode("testCase.test.webTest2", FetchMode.JOIN)
            fetchMode("testCase.test.sqlComboGroup", FetchMode.JOIN)
            fetchMode("testCase.test.sqlComboParam", FetchMode.JOIN)
            fetchMode("testCase.test.arrayComboGroup", FetchMode.JOIN)
            fetchMode("testCase.test.arrayComboGroup.arrayComboParam", FetchMode.JOIN)
            fetchMode("testCase.dbSetting", FetchMode.JOIN)
            fetchMode("testCase.dbSetting.dbDriver", FetchMode.JOIN)
            fetchMode("testCase.webEnvironment1", FetchMode.JOIN)
            fetchMode("testCase.webEnvironment2", FetchMode.JOIN)
        };
    }
}
