package diffapp

class ArrayComboParam{
    int id;
    String name;
    String value;

    static belongsTo = [arrayComboGroup: ArrayComboGroup];

    static mapping = {
        table 'ac_params'
        id  column: 'acp_id',
            generator: 'identity',
            type: 'integer'
        name column: 'name'
        value column: 'value',
            type: 'text'
        arrayComboGroup column: 'acg_id',
            type: 'integer'
    }

    static constraints = {
        name size: 1..255,
            nullable: false,
            blank: false
        value nullable: false,
            blank: true
    }

    public static ArrayComboParam getFullTree(int id){
        return ArrayComboParam.get(id);
    }
}
