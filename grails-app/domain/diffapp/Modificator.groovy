package diffapp

class Modificator{

    int id;
    String name = "";
    String code = "";

    //static belongsTo = CsvColumn;

    static mapping = {
        table 'modificators'
        id  column: 'modificator_id',
            generator: 'identity',
            type: 'integer'
        name column: 'name'
        code column: 'code',
            type: 'text'
    }

    static constraints = {
        name size: 1..50,
            blank: false,
            nullable: false,
            unique: true
        code minSize: 5,
            nullable: false,
            blank: false
    }

    public static Modificator getFullTree(int id){
        return Modificator.get(id);
    }
}
