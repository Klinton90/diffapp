package diffapp

import org.hibernate.FetchMode

class TestCase{

    int id;
    String name;
    String description;
    DbSetting dbSetting;
    WebEnvironment webEnvironment1;
    WebEnvironment webEnvironment2;

    static belongsTo = TestCaseRun

    static hasMany = [test: Test]

    static mapping = {
        table 'test_cases'
        id  column: 'tc_id',
            generator: 'identity',
            type: 'integer'
        name column: 'name'
        description column: "description"
        dbSetting column: 'db_setting_id'
        webEnvironment1 column: 'web_environment1_id'
        webEnvironment2 column: 'web_environment2_id'
    }

    static constraints = {
        name nullable: false,
            blank: false,
            unique: true,
            size: 1..255
        description nullable: false,
            blank: true,
            size: 1..255
        test nullable: true
        dbSetting validator: {val, obj, errors->
            if(val.id == 0){
                return errors.rejectValue(propertyName, 'required', [propertyName] as Object[], "This field is required");
            }
        }
        webEnvironment1 validator: {val, obj, errors->
            if(val.id == 0){
                return errors.rejectValue(propertyName, 'required', [propertyName] as Object[], "This field is required");
            }
        }
        webEnvironment2 validator: {val, obj, errors->
            if(val.id == 0){
                return errors.rejectValue(propertyName, 'required', [propertyName] as Object[], "This field is required");
            }
            if(val.id == obj.webEnvironment1.id){
                return errors.rejectValue(propertyName, 'webEnvironment.diff');
            }
        }
    }

    public static TestCase getFullTree(int id){
        return TestCase.createCriteria().get{
            eq("id", id)
            fetchMode("test", FetchMode.JOIN)
            fetchMode("test.xml2DbParam", FetchMode.JOIN)
            fetchMode("test.xml2DbParam.includedMapping", FetchMode.JOIN)
            fetchMode("test.xml2DbParam.webEnvironment", FetchMode.JOIN)
            fetchMode("test.xml2XmlParam", FetchMode.JOIN)
            fetchMode("test.xml2XmlParam.ignoredElement", FetchMode.JOIN)
            fetchMode("test.xml2XmlParam.webEnvironment1", FetchMode.JOIN)
            fetchMode("test.xml2XmlParam.webEnvironment2", FetchMode.JOIN)
            fetchMode("test.webTest1", FetchMode.JOIN)
            fetchMode("test.webTest2", FetchMode.JOIN)
            fetchMode("test.sqlComboGroup", FetchMode.JOIN)
            fetchMode("test.sqlComboParam", FetchMode.JOIN)
            fetchMode("test.arrayComboGroup", FetchMode.JOIN)
            fetchMode("test.arrayComboGroup.arrayComboParam", FetchMode.JOIN)
            fetchMode("dbSetting", FetchMode.JOIN)
            fetchMode("dbSetting.dbDriver", FetchMode.JOIN)
            fetchMode("webEnvironment1", FetchMode.JOIN)
            fetchMode("webEnvironment2", FetchMode.JOIN)
        };
    }
}