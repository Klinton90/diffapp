package diffapp

import org.hibernate.FetchMode

class DbSetting{

    int id;
    String login;
    String password;
    String url;
    String name;
    DbDriver dbDriver;

    static belongsTo = [
        TestCase,
        Csv2DbParam
    ];

    static mapping = {
        table 'db_settings'
        id  column: 'db_setting_id',
            generator: 'identity',
            type: 'integer'
        login column: 'login'
        password column: 'password'
        url column: 'url'
        dbDriver column: 'db_driver_id'
        name column: 'name'
    }

    static constraints = {
        login size: 1..255,
            blank: false,
            nullable: false
        password size: 1..255,
            blank: false,
            nullable: false
        url size: 1..255,
            blank: false,
            nullable: false
        name size: 1..50,
            blank: false,
            nullable: false,
            unique: true
        dbDriver validator: {val, obj, errors->
            if(val.id == 0){
                return errors.rejectValue(propertyName, 'required');
            }
        }
    }

    public static DbSetting getFullTree(int id){
        return DbSetting.createCriteria().get{
            eq("id", id)
            fetchMode("dbDriver", FetchMode.JOIN)
        };
    }
}
