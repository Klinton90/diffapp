package diffapp

import org.hibernate.FetchMode

class Xml2DbParam{
    int id;
    Boolean orderlySafeMode = true;
    Boolean orderlySafeArrayMode = false;
    Boolean orderlySafeIncludedMode = true;
    String ignoredValue = 'ignore';
    String modifications1 = '';
    String modifications2 = '';
    String name = '';
    String query = '';

    static belongsTo = Test;

    static hasMany = [includedMapping: IncludedMapping]

    static mapping = {
        table 'xml2db_params'
        id  column: 'xml2db_id',
            generator: 'identity',
            type: 'integer'
        orderlySafeMode column: 'orderly_safe'
        orderlySafeArrayMode column: 'orderly_safe_array'
        orderlySafeIncludedMode column: 'orderly_safe_inc'
        ignoredValue column: 'ignored_value'
        modifications1 column: 'mods1',
            type: 'text'
        modifications2 column: 'mods2',
            type: 'text'
        name column: 'name'
        query column: 'query',
            type: 'text'
    }

    static constraints = {
        ignoredValue size: 0..10,
            nullable: false,
            blank: false
        modifications1 nullable: false,
            blank: true
        modifications2 nullable: false,
            blank: true
        includedMapping nullable: true
        name size: 1..255,
            nullable: false,
            blank: false,
            unique: true
        query nullable: false,
            blank: false,
            minSize: 5
    }

    public static Xml2DbParam getFullTree(int id){
        return Xml2DbParam.createCriteria().get{
            eq("id", id)
            fetchMode("includedMapping", FetchMode.JOIN)
        };
    }
}
