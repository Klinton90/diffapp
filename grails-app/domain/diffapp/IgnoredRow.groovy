package diffapp

import org.hibernate.FetchMode

class IgnoredRow{
    int id;
    String name;
    String value = '';
    String type = 'REGULAR';

    static hasMany = IgnoredRow
    
    static belongsTo = [
        parentRow: IgnoredRow,
        csv2DbParam: Csv2DbParam
    ];

    static mapping = {
        table 'ignored_rows'
        id  column: 'ir_id',
            generator: 'identity',
            type: 'integer'
        name column: 'name'
        value column: 'value'
        type column: 'type'
        parentRow column: 'parent_row_id',
            type: 'integer'
        csv2DbParam column: 'csv2db_id',
            type: 'integer'
    }

    static constraints = {
        name size: 1..255,
            nullable: false,
            blank: false
        type inList: ["REGULAR", "AND", "OR"],
            nullable: false,
            blank: false
        value size: 0..255,
            nullable: false,
            blank: true,
            validator: {val, obj, errors->
                if(obj.type != "REGULAR"){
                    if(val != ""){
                        errors.rejectValue(propertyName, 'IgnoredRow.type');
                    }
                }else if(val == ""){
                    errors.rejectValue(propertyName, 'IgnoredRow.min');
                }
            }
        parentRow nullable: true
        csv2DbParam nullable: false/*,
            validator: {val, obj, errors->
                if(val.id == 0 && obj.parentRowId == 0){
                    errors.rejectValue(propertyName, 'IgnoredRow.required');
                }
                if(val.id != 0 && obj.parentRowId > 0){
                    errors.rejectValue(propertyName, 'IgnoredRow.onlyOne');
                }
            }*/
    }

    public static IgnoredRow getFullTree(int id){
        return IgnoredRow.createCriteria().get{
            eq("id", id)
            fetchMode("parentRow", FetchMode.JOIN)
            fetchMode("csv2DbParam", FetchMode.JOIN)
            //fetchMode("ignoredRow", FetchMode.JOIN)
        };
    }
}
