package diffapp

import groovy.util.logging.Slf4j
import org.hibernate.FetchMode

@Slf4j
class Xml2XmlParam{
    int id;
    Boolean orderlySafeMode = false;
    Boolean orderlySafeChildrenMode = true;
    String ignoreAttrsCommand = '';
    String ignoreNodesCommand = '';
    String modifications1 = '';
    String modifications2 = '';
    String name = '';
    String needleHelper = '';

    static belongsTo = Test;

    static hasMany = [ignoredElement: IgnoredElement]

    static mapping = {
        table 'xml2xml_params'
        id  column: 'xml2xml_id',
            generator: 'identity',
            type: 'integer'
        orderlySafeMode column: 'orderly_safe'
        orderlySafeChildrenMode column: 'orderly_safe_children'
        ignoreAttrsCommand column: 'attr_command',
            type: 'text'
        ignoreNodesCommand column: 'node_command',
            type: 'text'
        modifications1 column: 'mods1',
            type: 'text'
        modifications2 column: 'mods2',
            type: 'text'
        name column: 'name'
        needleHelper column: 'needle_helper'
    }

    static constraints = {
        ignoreAttrsCommand nullable: false,
            blank: true
        ignoreNodesCommand nullable: false,
            blank: true
        modifications1 nullable: false,
            blank: true
        modifications2 nullable: false,
            blank: true
        ignoredElement nullable: true
        name size: 1..255,
            nullable: false,
            blank: false,
            unique: true
        needleHelper size: 1..255,
            nullable: false,
            blank: true
    }

    public static Xml2XmlParam getFullTree(int id){
        return Xml2XmlParam.createCriteria().get{
            eq("id", id)
            fetchMode("ignoredElement", FetchMode.JOIN)
        };
    }
}
