package diffapp

class OtherParam{
    
    int id = 0;
    String paramType;
    String value;

    static mapping = {
        table 'other_params'
        id  column: 'op_id',
            generator: 'identity',
            type: 'integer'
        paramType column: 'param_type'
        value column: 'value'
    }

    static constraints = {
        paramType size: 1..50,
            nullable: false,
            blank: false,
            unique: true
        value size: 1..255,
        nullable: false,
        blank: false
    }

    public static OtherParam getFullTree(int id){
        return OtherParam.get(id);
    }
    
    public static void createOrUpdate(Object value, String paramName){
        OtherParam op = OtherParam.findOrCreateByParamType(paramName);
        def tmp = value;
        op.value = tmp instanceof List<String> ? ((ArrayList<String>)tmp).join(",") : tmp.toString();
        op.save();
    }
}
