package diffapp

import org.hibernate.FetchMode

class CsvColumn{

    int id;
    int position;
    String name = "";
    String ignored = "";
    String splitChar = "";
    Modificator modificator;

    static belongsTo = [csvSetting: CsvSetting];

    static mapping = {
        table 'csv_columns'
        id  column: 'csv_column_id',
            generator: 'identity',
            type: 'integer'
        name column: 'name'
        ignored column: 'ignored'
        position column: 'position'
        splitChar column: 'split_char'
        modificator column: 'modificator_id'
        csvSetting column: 'csv_setting_id',
            type: 'integer'
    }

    static constraints = {
        name size: 1..50,
            blank: false,
            nullable: false
        ignored size: 0..50,
            blank: true,
            nullable: false
        splitChar size: 0..3,
            blank: true,
            nullable: false
        modificator nullable: true
        csvSetting nullable: false
        position min: 1
    }

    public static CsvColumn getFullTree(int id){
        return CsvColumn.createCriteria().get{
            eq("id", id)
            fetchMode("modificator", FetchMode.JOIN)
        };
    }
}
