package diffapp

class SqlComboParam{
    int id;
    String name;
    String value;

    static belongsTo = [test: Test];

    static mapping = {
        table 'sql_combo_params'
        id  column: 'scp_id',
            generator: 'identity',
            type: 'integer'
        name column: 'name'
        value column: 'value'
        test column: 'test_id',
            type: 'integer'
    }

    static constraints = {
        name size: 1..255,
            nullable: false,
            blank: false
        value size: 0..255,
            nullable: false,
            blank: true
    }

    public static SqlComboParam getFullTree(int id){
        return SqlComboParam.get(id);
    }
}
