package diffapp

import org.hibernate.FetchMode

class Db2DbParam{

    int id;
    String name;
    String description = "";
    DbSetting dbSetting1;
    DbSetting dbSetting2;
    Boolean orderlySafeMode = false;
    String query1;
    String query2 = "";
    String ignoredElements = "";

    static mapping = {
        table 'db2db_params'
        id  column: 'db2db_id',
            generator: 'identity',
            type: 'integer'
        name column: 'name'
        description column: 'description'
        dbSetting1 column: 'db_setting1_id'
        dbSetting2 column: 'db_setting2_id'
        orderlySafeMode column: 'orderly_safe'
        query1 column: 'query1'
        query2 column: 'query2'
        ignoredElements column: 'ignored_elements'
    }

    static constraints = {
        name size: 1..50,
            blank: false,
            nullable: false,
            unique: true
        query1 nullable: false,
            blank: false,
            minSize: 5,
            notEqual: "SELECT * FROM"
        query2 nullable: false,
            blank: true
        description size: 0..255,
            blank: true,
            nullable: false
        ignoredElements size: 0..255,
            blank: true,
            nullable: false
        dbSetting1 validator: {val, obj, errors->
            if(val.id == 0){
                return errors.rejectValue(propertyName, 'required', [propertyName] as Object[], "This field is required");
            }
        }
        dbSetting2 validator: {val, obj, errors->
            if(val.id == 0){
                return errors.rejectValue(propertyName, 'required', [propertyName] as Object[], "This field is required");
            }
        }
    }

    public static Db2DbParam getFullTree(int id){
        return Db2DbParam.createCriteria().get{
            eq("id", id)
            fetchMode("dbSetting1", FetchMode.JOIN)
            fetchMode("dbSetting1.dbDriver", FetchMode.JOIN)
            fetchMode("dbSetting2", FetchMode.JOIN)
            fetchMode("dbSetting2.dbDriver", FetchMode.JOIN)
        };
    }
}
