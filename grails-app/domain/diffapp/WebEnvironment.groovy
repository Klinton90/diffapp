package diffapp

class WebEnvironment{
    int id;
    String url;
    String name;

    static belongsTo = [TestCase];

    static mapping = {
        table 'web_environments'
        id  column: 'web_environment_id',
            generator: 'identity',
            type: 'integer'
        url column: 'url'
        name column: 'name'
    }

    static constraints = {
        name size: 1..50,
            blank: false,
            nullable: false,
            unique: true
        url size: 5..255,
            blank: false,
            nullable: false
    }

    public static WebEnvironment getFullTree(int id){
        return WebEnvironment.get(id);
    }
}
