package diffapp

import org.hibernate.FetchMode

class Test{
    int id;
    String name;
    WebTest webTest1;
    WebTest webTest2;
    String listPath = '';
    ArrayComboGroup arrayComboGroup;
    SqlComboGroup sqlComboGroup;
    String description = '';
    int spiraTestId = 0;
    int spiraPrjId = 0;
    Xml2XmlParam xml2XmlParam;
    Xml2DbParam xml2DbParam;

    static belongsTo = TestCase;

    static hasMany = [sqlComboParam: SqlComboParam/*, testCase: TestCase*/];
    //static hasMany = [testCase: TestCase];

    static mapping = {
        table 'tests'
        id  column: 'test_id',
            generator: 'identity',
            type: 'integer'
        name column: 'name'
        webTest1 column: 'web_test1_id'
        webTest2 column: 'web_test2_id'
        listPath column: 'list_path'
        arrayComboGroup column: 'acg_id'
        sqlComboGroup column: 'scg_id'
        description column: 'description'
        spiraTestId column: 'spira_test_id'
        spiraPrjId column: 'spira_prj_id'
        xml2XmlParam column: 'xml2xml_id'
        xml2DbParam column: 'xml2db_id'
    }

    static constraints = {
        name size: 1..50,
            blank: false,
            nullable: false,
            unique: true
        listPath size: 0..255,
            blank: true,
            nullable: false
        description size: 0..255,
            blank: true,
            nullable: false
        webTest1 validator: {val, obj, errors->
            if(val.id == 0){
                return errors.rejectValue(propertyName, 'required', [propertyName] as Object[], "This field is required");
            }
        }
        webTest2 nullable: true,
            validator: {val, obj, errors->
                if(val != null && val.id != 0 && val.format != obj.webTest1.format){
                    return errors.rejectValue(propertyName, 'webTest.format.diff', [propertyName] as Object[], "Formats must match");
                }
            }
        arrayComboGroup nullable: true
        sqlComboGroup nullable: true,
            validator: {val, obj, errors->
                if(val.id == 0 && obj.arrayComboGroupId == 0){
                    errors.rejectValue(propertyName, 'combo.required')
                }
            }
        sqlComboParam nullable: true
        xml2XmlParam nullable: true
        xml2DbParam nullable: true,
            validator: {val, obj, errors->
                if(val.id == 0 && obj.xml2XmlParamId == 0){
                    errors.rejectValue(propertyName, 'testParam.required');
                }
                if(val.id != 0 && obj.xml2XmlParamId > 0){
                    errors.rejectValue(propertyName, 'testParam.onlyOne');
                }
            }
    }

    public static Test getFullTree(int id){
        return Test.createCriteria().get{
            eq("id", id)
            fetchMode("xml2DbParam", FetchMode.JOIN)
            fetchMode("xml2DbParam.includedMapping", FetchMode.JOIN)
            fetchMode("xml2XmlParam", FetchMode.JOIN)
            fetchMode("xml2XmlParam.ignoredElement", FetchMode.JOIN)
            fetchMode("webTest1", FetchMode.JOIN)
            fetchMode("webTest2", FetchMode.JOIN)
            fetchMode("sqlComboGroup", FetchMode.JOIN)
            fetchMode("sqlComboParam", FetchMode.JOIN)
            fetchMode("arrayComboGroup", FetchMode.JOIN)
            fetchMode("arrayComboGroup.arrayComboParam", FetchMode.JOIN)
        };
    }
}