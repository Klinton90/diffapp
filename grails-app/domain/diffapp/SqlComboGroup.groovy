package diffapp

import org.hibernate.FetchMode

class SqlComboGroup{
    int id;
    String name;
    String query;

    static belongsTo = Test;

    static mapping = {
        table 'sql_combo_groups'
        id  column: 'scg_id',
            generator: 'identity',
            type: 'integer'
        name column: 'name'
        query column: 'query',
            type: 'text'
    }

    static constraints = {
        name size: 1..50,
            blank: false,
            nullable: false,
            unique: true
        query minSize: 5,
            nullable: false,
            blank: false
    }

    public static SqlComboGroup getFullTree(int id){
        return SqlComboGroup.get(id);
    }
}
