package diffapp

class IncludedMapping{
    int id;
    String name
    String query;

    static belongsTo = [xml2DbParam: Xml2DbParam];

    static mapping = {
        table 'included_mappings'
        id  column: 'im_id',
            generator: 'identity',
            type: 'integer'
        name column: 'name'
        query column: 'query',
            type: 'text'
        xml2DbParam column: 'xml2db_id',
            type: 'integer'
    }

    static constraints = {
        name size: 1..255,
            nullable: false,
            blank: false
        query minSize: 5,
            nullable: false,
            blank: false
    }

    public static IncludedMapping getFullTree(int id){
        return IncludedMapping.get(id);
    }
}
